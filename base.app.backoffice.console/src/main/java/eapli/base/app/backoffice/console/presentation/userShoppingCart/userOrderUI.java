package eapli.base.app.backoffice.console.presentation.userShoppingCart;/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */




//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.net.InetAddress;
//import java.net.Socket;
//import java.net.UnknownHostException;

import eapli.CodingAndDecoding;
import eapli.MessageDTO;
import eapli.base.costumer.domain.Costumer;
import eapli.base.product.Internal_code;
import eapli.base.product.Product;
import eapli.base.salesmanagement.application.ShoppingCartController;
import eapli.base.salesmanagement.domain.OrderItem;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.Quantity;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings({ "squid:S106" })
public class userOrderUI extends AbstractUI {
    private final ShoppingCartController shoppingCartController = new ShoppingCartController();
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    boolean maisProdutos=true;
    static final int SERVER_PORT=10735;
    static final String KEYSTORE_PASS="forgotten";

    static InetAddress serverIP;
    static SSLSocket sock;;
    private static CodingAndDecoding cod= new CodingAndDecoding();

    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow() {
        String email = authz.session().get().authenticatedUser().email().toString();
// Trust these certificates provided by servers
        System.setProperty("javax.net.ssl.trustStore", "C:\\Users\\RPG\\Desktop\\teste\\src\\main\\java\\tcp\\client1_J.jks");
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key for client certificate when requested by the server
        System.setProperty("javax.net.ssl.keyStore","C:\\Users\\RPG\\Desktop\\teste\\src\\main\\java\\tcp\\client1_J.jks");
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();

        try { serverIP = InetAddress.getByName("vsgate-s1.dei.isep.ipp.pt"); }
        catch(UnknownHostException ex) {
            System.out.println("Invalid server specified: 127.0.0.1");
            System.exit(1); }


        try {
            sock = (SSLSocket) sf.createSocket(serverIP,SERVER_PORT);
        }
        catch(IOException ex) {
            System.out.println("Failed to connect to the server" + SERVER_PORT);
            System.out.println("Application aborted.");
            System.exit(1);
        }


        try {
            sock.startHandshake();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Product_Order> list=null;
        try {
            list = getOrdersFromCostumer(email);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }



        try {
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Product_Order> getOrdersFromCostumer(String email) throws IOException, InterruptedException, ClassNotFoundException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(email);
        oOut.flush();
        byte[] data = bos.toByteArray();
        byte[] message = new byte[data.length+4];

        message[0]=1;
        message[1]=3;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        message[2]= (byte) size1;
        message[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            message[i+4]=data[i];
        }
        out.write(message);
        
        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        ByteArrayInputStream in = new ByteArrayInputStream(messageDTO.getData_message());
        ObjectInputStream is = new ObjectInputStream(in);
        List<Product_Order> list= (List<Product_Order>) is.readObject();
        return list;
    }

    @Override
    public String headline() {
        return "Register a new Costumer";
    }
}
