package eapli.base.app.backoffice.console.presentation.WarehouseManager;

import eapli.base.app.backoffice.console.presentation.WarehouseManager.Actions.AGVPrinter;
import eapli.base.app.backoffice.console.presentation.WarehouseManager.Actions.OrderPrinter;
import eapli.base.generalmanagement.domain.AGV;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.warehousemanagement.application.OrderToAGVController;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrderToAGVUI extends AbstractUI {
    private OrderToAGVController controller=new OrderToAGVController();


    @Override
    protected boolean doShow() {
        List<Product_Order>orders=new ArrayList<>();
        orders=controller.findavailableOrder();
        if(orders.isEmpty()) {
        throw new IllegalArgumentException("No Orders available.");
        }
        final SelectWidget<Product_Order> selectorCat = new SelectWidget<>("Select a Order", orders, new OrderPrinter());
        selectorCat.show();
        Product_Order order=selectorCat.selectedElement();
        List<AGV>agvList;
        agvList=controller.findavailableAGV(order.getWeight().getWeight(),order.getVolume().getTotal());
        if(agvList.isEmpty()){
            throw new IllegalArgumentException("No AGV is available for that order");
        }
        final SelectWidget<AGV> selectorAGVCat = new SelectWidget<>("Select a AGV", agvList, new AGVPrinter());
        selectorAGVCat.show();
        AGV agv=selectorAGVCat.selectedElement();
        final String description = Console.readLine("Description of the Task:\n");
        controller.changeStatusAGV(agv);
        controller.changeStatusOrder(order);
        controller.createTaskandSave(description,order,agv);




    return true;
    }

    public boolean show(){

        return doShow();
    }
    @Override
    public String headline() {
        return null;
    }
}
