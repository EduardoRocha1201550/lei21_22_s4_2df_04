package eapli.base.app.backoffice.console.presentation.WarehouseManager;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.warehousemanagement.application.OrderUpdateController;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

import java.util.ArrayList;
import java.util.List;

public class OrderUpdateUI extends AbstractUI {
    private OrderUpdateController controller=new OrderUpdateController();

    @Override
    protected boolean doShow(){

        List<Product_Order> preparedOrders =new ArrayList<>();
        List<Product_Order> doneOrders =new ArrayList<>();
        preparedOrders = controller.findOrderDoing();
        int num=0;
        for (Product_Order cat : preparedOrders){
            System.out.println(num+". "+cat.toString());
            doneOrders.add(cat);
            num++;
        }

        Integer option1=Console.readInteger("Please enter an option");
        Product_Order order=doneOrders.get(option1);
        controller.changeStatus2(order);
        System.out.println("Order Updated!");
        return true;
    }

    public boolean show(){

        return doShow();
    }
    @Override
    public String headline() {
        return null;
    }
}

