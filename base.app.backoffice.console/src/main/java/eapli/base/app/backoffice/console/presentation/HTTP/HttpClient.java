package eapli.base.app.backoffice.console.presentation.HTTP;

import eapli.base.app.backoffice.console.presentation.HTTP.HTTPmessage;
import eapli.base.app.backoffice.console.presentation.HTTP.HttpServerOnline;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.Internal_code;
import eapli.base.salesmanagement.domain.*;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;

import java.io.*;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import java.net.Socket;
import javax.net.ssl.SSLSocket;

/**
 *
 * @author ANDRE MOREIRA (asc@isep.ipp.pt)
 */
public class HttpClient extends Thread {
	String baseFolder;
	SSLSocket sock;
	DataInputStream inS;
	DataOutputStream outS;




	public HttpClient(SSLSocket s, String f) {
		baseFolder=f; sock=s;
	}

	public void run() {
		try {
			outS = new DataOutputStream(sock.getOutputStream());
			inS = new DataInputStream(sock.getInputStream());
		}
		catch(IOException ex) { System.out.println("Thread error on data streams creation"); }
		try {
			HTTPmessage request = new HTTPmessage(inS);
			HTTPmessage response = new HTTPmessage();
			// System.out.println(request.getURI());

			if(request.getMethod().equals("GET")) {
				if(request.getURI().equals("/votes")) {
					response.setContentFromString(
							HttpServerOnline.getVotesStandingInHTML(), "text/html");
					response.setResponseStatus("200 Ok");
				}
				else {
					String fullname=baseFolder + "/";
					if(request.getURI().equals("/")) fullname=fullname+"index.html";
					else fullname=fullname+request.getURI();
					if(response.setContentFromFile(fullname)) {
						response.setResponseStatus("200 Ok");
					}
					else {
						response.setContentFromString(
								"<html><body><h1>404 File not found</h1></body></html>",
								"text/html");
						response.setResponseStatus("404 Not Found");
					}
				}
				response.send(outS);
			}
			else { // NOT GET
				if(request.getMethod().equals("PUT")
						&& request.getURI().startsWith("/votes/")) {
					response.setResponseStatus("200 Ok");
				}
				else {
					response.setContentFromString(
							"<html><body><h1>ERROR: 405 Method Not Allowed</h1></body></html>",
							"text/html");
					response.setResponseStatus("405 Method Not Allowed");
				}
				response.send(outS);
			}
		}
		catch(IOException ex) { }
		try { sock.close();}
		catch(IOException ex) { System.out.println("CLOSE IOException"); }
	}
}