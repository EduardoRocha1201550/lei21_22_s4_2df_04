package eapli.base.app.backoffice.console.presentation.WarehouseManager.Actions;

import eapli.base.warehousemanagement.application.JSONController;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;



public class ShowWarehousePlantUI extends AbstractUI {


    private static final boolean VERSION = true;

    private final JSONController theController = new JSONController();

    @Override
    protected boolean doShow() {

        final String file_name = Console.readLine("File Name");


        theController.createPlant(file_name,VERSION);


        System.out.println(theController.showWarehousePlant());

        return false;
    }

    @Override
    public String headline() {
        return "Enter JSON file";
    }
}
