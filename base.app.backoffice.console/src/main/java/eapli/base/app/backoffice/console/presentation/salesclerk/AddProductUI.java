package eapli.base.app.backoffice.console.presentation.salesclerk;

import eapli.base.generalmanagement.action.CategoryPrinter;
import eapli.base.generalmanagement.application.AddProductController;
import eapli.base.generalmanagement.application.ListCategoryController;
import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.*;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Money;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class AddProductUI extends AbstractUI {

    ListCategoryController list;

    private final AddProductController controller = new AddProductController();
    private final CategoryRepository repository = PersistenceContext.repositories().categories();


    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow(){
        final String novoIdentificador = Console.readLine("ID do produto :\n");
        final String novoShortDescription = Console.readLine("Short Description :\n");
        final String technicalDescription = Console.readLine("Technical Description: ");
        final String novoExtendedDescription = Console.readLine("Extended Description :\n");
        final String novoReference = Console.readLine("Reference :\n");
        final Money novoPriceWithoutTx = Money.euros(Console.readDouble("Price Without Tax: "));
        final Money novoPrice = Money.euros(Console.readDouble("Price : \n"));
        final String novoBrand = Console.readLine("Brand: \n");
        System.out.println("Volume sizes");
        final double novoA = Console.readDouble("Altura: ");
        final double novoC = Console.readDouble("Comprimento: ");
        final double novoL = Console.readDouble("Largura: ");
        final double peso = Console.readDouble("Weight :");
        System.out.println("Product Location");
        final int novoWarehouse = Console.readInteger("Warehouse ID: ");
        final int novoAisle = Console.readInteger("Aisle:");
        final int novoRow = Console.readInteger("Row:");
        final int novoShelf = Console.readInteger("Shelf:");
        Integer choose;
        Long ean, upc;
        final Set<String> photoPaths = new HashSet<>();
        String option;
        String photoPath;


        do {
            photoPath = Console.readLine("Photo Path:");
            File file = new File(photoPath);
            while (!file.exists()){
                photoPath = Console.readLine("Invalid Photo Path! Insert a new one:");
                file = new File(photoPath);
            }
            if (Files.exists(Paths.get(photoPath)) && !photoPaths.contains(photoPath)){
                photoPaths.add(photoPath);




            } else if (!Files.exists(Paths.get(photoPath))){
                System.out.println("Wrong Path Inserted");
            } else {
                System.out.println("This photo was already added");
            }
            option = Console.readLine("Do you want to add another phot? (yes|no)");

        }while (option.equalsIgnoreCase("yes"));



        Barcode barcodeSTR =null;

        choose = Console.readInteger("Do you want the Barcode to be with what valid coding " +
                "standard:\n1.UPC\n2.EAN-13");
        switch (choose){
            case 1:
                do{
                    upc = Console.readLong("Insert reference (12 digits):");

                }while (upc<=99999999999L|upc>=999999999999L);
                barcodeSTR = new Barcode(upc, Product.TYPE.UPC);
                break;
            case 2:
                do{
                    ean = Console.readLong("Insert reference (13 digits):");

                }while (ean.toString().length() != 13);
                barcodeSTR = new Barcode(ean, Product.TYPE.EAN13);
                break;
        }
        final boolean novoAtivo = true;

        ListCategoryUI listCategoryUI= new ListCategoryUI();
        Iterable<Category> listCat = listCategoryUI.elements();
        List<Category> categoryList=new LinkedList<>();
        int num=0;
        for (Category cat : listCat){
            System.out.println(num+". "+cat.toString());
            categoryList.add(cat);
            num++;
        }

        Integer option1=Console.readInteger("Please enter an option");
        Category category=categoryList.get(option1);

        Integer choice = Console.readInteger("Do you want to add an Production Code:\n1.Yes\n2.No");
        switch (choice){
            case 1:
                final String productCode= Console.readLine("Product Code: ");
                try{
                    this.controller.registerProduct(new Product(new Internal_code(novoIdentificador),new Product_code(productCode), new Short_description(novoShortDescription), new Extended_description(novoExtendedDescription), new Technical_description(technicalDescription), new Reference(novoReference),novoPriceWithoutTx, novoPrice, category,new Brand(novoBrand),novoAtivo,new Volume(novoA,novoL,novoC),new Weight(peso),barcodeSTR,new Product_Location(novoWarehouse,novoAisle,novoRow,novoShelf),new Photo(photoPath)));
                } catch(final IntegrityViolationException e){
                    System.out.println("That product already exists");
                }
            case 2:

                try{
                    this.controller.registerProduct(new Product(new Internal_code(novoIdentificador), new Short_description(novoShortDescription), new Extended_description(novoExtendedDescription), new Technical_description(technicalDescription), new Reference(novoReference),novoPriceWithoutTx, novoPrice, category,new Brand(novoBrand),novoAtivo,new Volume(novoA,novoL,novoC),new Weight(peso),barcodeSTR,new Product_Location(novoWarehouse,novoAisle,novoRow,novoShelf), new Photo(photoPath)));
                } catch(final IntegrityViolationException e){
                    System.out.println("That product already exists");
                }
        }
        return false;
    }

    @Override
    public String headline(){
        return "Register new Product";
    }
}
