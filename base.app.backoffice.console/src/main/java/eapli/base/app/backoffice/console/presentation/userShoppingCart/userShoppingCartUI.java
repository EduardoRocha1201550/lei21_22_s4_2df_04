package eapli.base.app.backoffice.console.presentation.userShoppingCart;/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */




//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.net.InetAddress;
//import java.net.Socket;
//import java.net.UnknownHostException;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import eapli.CodingAndDecoding;
import eapli.MessageDTO;
import eapli.base.costumer.domain.*;
import eapli.base.product.Internal_code;
import eapli.base.product.Product;
import eapli.base.salesmanagement.application.ShoppingCartController;
import eapli.base.salesmanagement.domain.OrderItem;
import eapli.base.salesmanagement.domain.Quantity;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;


@SuppressWarnings({ "squid:S106" })
public class userShoppingCartUI extends AbstractUI {
    private final ShoppingCartController shoppingCartController = new ShoppingCartController();
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    boolean maisProdutos=true;
    static InetAddress serverIP;
    static Socket sock;
    private static CodingAndDecoding cod= new CodingAndDecoding();

    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow() {
        String email=authz.session().get().authenticatedUser().email().toString();
        List<OrderItem> orderItemList=new ArrayList<>();
        try {
            serverIP = InetAddress.getByName("127.0.0.1");
        } catch (UnknownHostException ex) {
            System.out.println("Server invalido: 127.0.0.1");
            System.exit(1);
        }
        try {
            sock = new Socket(serverIP, 9996);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Costumer costumer= null;
        try {
            costumer = getCostumerByEmail(email);
        } catch (IOException | InterruptedException |ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {

            if(!shoppingCartController.shoppingCartExistance(email)) { //caso não tenha um shopping cart
                int option=Console.readInteger("Você ainda não possui um shopping cart, gostaria de criar um? 1-Sim 2-Nao");
                if(option==1){
                    while(maisProdutos){

                        System.out.println("Produtos para adicionar ao seu shopping cart");

                        System.out.println(shoppingCartController.allProducts().toString());

                        final String productInternalCode=Console.readLine("Escolha um produto");
                        final int quantity=Console.readInteger("A sua quantidade");

                        Internal_code internal_code= new Internal_code(productInternalCode);

                        OrderItem orderItem=new OrderItem(new Quantity(quantity),shoppingCartController.getProduct(internal_code));

                        orderItemList.add(orderItem);

                        int optionMoreProducts=Console.readInteger("Deseja adicionar mais produtos? 1-Sim 2-Nao");

                        if(optionMoreProducts==1){
                            maisProdutos=true;
                        }else{
                            maisProdutos=false;
                        }
                    }
                    shoppingCartController.registerShoppingCart(costumer,orderItemList);
                }

            }else{//caso já tenha um shopping cart
                while(maisProdutos) {
                    System.out.println("Produtos para adicionar ao seu shopping cart");

                    System.out.println(shoppingCartController.allProducts().toString());

                    final String productInternalCode=Console.readLine("Escolha um produto");

                    final int quantity=Console.readInteger("A sua quantidade");

                    Internal_code internal_code= new Internal_code(productInternalCode);

                    OrderItem orderItem=new OrderItem(new Quantity(quantity),shoppingCartController.getProduct(internal_code));

                    orderItemList.add(orderItem);

                    int optionMoreProducts=Console.readInteger("Deseja adicionar mais produtos? 1-Sim 2-Nao");
                    if(optionMoreProducts==1){
                        maisProdutos=true;
                    }else{
                        maisProdutos=false;
                    }
                }
                shoppingCartController.adicionarProdutos(costumer,orderItemList);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        try {
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;


    }
    public Costumer getCostumerByEmail(String email) throws IOException, InterruptedException, ClassNotFoundException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(email);
        oOut.flush();
        byte[] data = bos.toByteArray();
        byte[] message = new byte[data.length+4];

        message[0]=1;
        message[1]=5;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        message[2]= (byte) size1;
        message[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            message[i+4]=data[i];
        }
        out.write(message);

        Thread.sleep(500);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        ByteArrayInputStream in = new ByteArrayInputStream(messageDTO.getData_message());
        ObjectInputStream is = new ObjectInputStream(in);
        Costumer costumer= (Costumer) is.readObject();
        System.out.println("Costumer: "+costumer.getId());
        return costumer;
    }
    public boolean checkShoppingCart(String email) throws IOException, InterruptedException, ClassNotFoundException{
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(email);
        oOut.flush();
        byte[] data = bos.toByteArray();
        byte[] message = new byte[data.length+4];

        message[0]=1;
        message[1]=5;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        message[2]= (byte) size1;
        message[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            message[i+4]=data[i];
        }
        out.write(message);

        Thread.sleep(500);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        if (messageDTO.getData_message()[0]==1){
            System.out.println("Has Shopping Cart");
            return true;
        }else{
            System.out.println("No Shopping Cart");
        }
        return false;
    }
    public Product getProductByCode(Internal_code code) throws IOException, InterruptedException, ClassNotFoundException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(code);
        oOut.flush();
        byte[] data = bos.toByteArray();
        byte[] message = new byte[data.length+4];

        message[0]=1;
        message[1]=5;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        message[2]= (byte) size1;
        message[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            message[i+4]=data[i];
        }
        out.write(message);

        Thread.sleep(500);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        ByteArrayInputStream in = new ByteArrayInputStream(messageDTO.getData_message());
        ObjectInputStream is = new ObjectInputStream(in);
        Product product= (Product) is.readObject();
        System.out.println("Product: "+product.getInternal_code());
        return product;
    }



    @Override
    public String headline() {
        return "Register a new Costumer";
    }
}
