package eapli.base.app.backoffice.console.presentation.salesclerk;


import eapli.framework.actions.Action;

public class ListCategoryAction implements Action {

    @Override
    public boolean execute(){
        return new ListCategoryUI().show();
    }
}
