package eapli.base.app.backoffice.console.presentation.WarehouseManager;

import eapli.framework.actions.menu.Menu;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

public class JSONUI extends AbstractUI {
    private static final int INSERTFILE=1;
    private static final int PREDEFINEDFILE=2;



    private Menu buildMainMenu() {
        final Menu menu = new Menu("Do you want to insert a file or you want a predefined file?");
        //menu.addItem(INSERTFILE,"Insert File",new JSONOpenFileAction(INSERTFILE));
        //menu.addItem(PREDEFINEDFILE,"Predefined File", new JSONOpenFileAction(PREDEFINEDFILE));
        return menu;
    }
    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow() {
        final Menu menu=buildMainMenu();
        final MenuRenderer renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        return renderer.render();
    }

    @Override
    public String headline() {
        return null;
    }
}
