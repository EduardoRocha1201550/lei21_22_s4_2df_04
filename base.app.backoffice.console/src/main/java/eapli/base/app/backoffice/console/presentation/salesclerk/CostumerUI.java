package eapli.base.app.backoffice.console.presentation.salesclerk;/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */



import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import eapli.base.LPROG.Rules;
import eapli.base.costumer.application.CostumerController;
import eapli.base.costumer.domain.*;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.domain.model.Role;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;


/**
 * Work in Progress - not fully implemented
 *
 * @author losa
 */
@SuppressWarnings({ "squid:S106" })
public class CostumerUI extends AbstractUI {
    private final CostumerController controller = new CostumerController();
    boolean dadosInvalidos=true;

    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow() {
            try {
                do{
                    final String name = Console.readLine("Nome");
                    final String vateId= Console.readLine("VatId");
                    final String phoneNumber= Console.readLine("Numero de telemovel");
                    final String email= Console.readLine("Email");
                    final int option=Console.readInteger("Quer introduzir data de nascimento, morada e genero? 1-Sim 0-Não");

                    if(option==0){
                        final String username=Console.readLine("Introduza o seu nome de utilizador");
                        final String password= Console.readLine("Introduza a sua password");
                        final ArrayList<Rules> rules= new ArrayList<Rules>();
                        controller.registerCostumer(new Name(name), new VateId(vateId), new PhoneNumber(phoneNumber), new Email(email), username, password,rules,
                         (Set<Role>) BaseRoles.CLIENT_USER);
                        dadosInvalidos=false;
                    }else if(option==1){
                        final String birthDate = Console.readLine("BirthDate dd-MM-yyyy");
                        final String address= Console.readLine("Morada");
                        final String gender=Console.readLine("Genero");

                        final String username=Console.readLine("Introduza o seu nome de utilizador");
                        final String password= Console.readLine("Introduza a sua password");
                        final ArrayList<Rules> rules= new ArrayList<Rules>();

                        controller.registerCostumerOptionals(new Name(name), new VateId(vateId),new PhoneNumber(phoneNumber),new Email(email),new BirthDate(birthDate),new Address(address), new Gender(gender), username, password,rules,(Set<Role>) BaseRoles.CLIENT_USER);
                        dadosInvalidos=false;
                    }
                }while(dadosInvalidos);
                System.out.println("Costumer created");
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            return false;


    }



    @Override
    public String headline() {
        return "Register a new Costumer";
    }
}
