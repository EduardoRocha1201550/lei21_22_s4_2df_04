//package eapli.base.app.backoffice.console.presentation.HTTP;
//
//
//import eapli.CodingAndDecoding;
//import eapli.MessageDTO;
//import eapli.base.generalmanagement.DTO.AGVDTO;
//import eapli.base.generalmanagement.domain.AGV;
//import eapli.base.infrastructure.persistence.PersistenceContext;
//import eapli.base.warehousemanagement.repositories.AGVRepository;
//
//import javax.net.ssl.SSLServerSocket;
//import javax.net.ssl.SSLServerSocketFactory;
//import javax.net.ssl.SSLSocket;
//import java.io.*;
//import java.net.InetAddress;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.net.UnknownHostException;
//import java.sql.SQLException;
//import java.util.List;
//
///**
// *
// * @author ANDRE MOREIRA (asc@isep.ipp.pt)
// */
//public class HttpServer {
//    static private final String BASE_FOLDER="/var/www/Test/www";
//    static private ServerSocket sock;
//    private static AGVRepository repository= PersistenceContext.repositories().agv();
//    static InetAddress serverIP;
//    private static CodingAndDecoding cod= new CodingAndDecoding();
//
//    public static void main(String args[]) throws Exception {
//	SSLSocket cliSock;
//
//	if(args.length!=1) {
//            System.out.println("Local port number required at the command line.");
//            System.exit(1);
//            }
//
//        refreshcounter =0;
//        System.setProperty("javax.net.ssl.keyStore", "server.jks");
//        System.setProperty("javax.net.ssl.keyStorePassword", "forgotten");
//
//        try {
//            SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
//            sock = (SSLServerSocket) sslF.createServerSocket(Integer.parseInt(args[0]));
//        }
//	catch(IOException ex) {
//            System.out.println("Server failed to open local port " + args[0]);
//            System.exit(1);
//            }
//	while (true){
//            cliSock=(SSLSocket) sock.accept();
//            HttpClient req=new HttpClient(cliSock, BASE_FOLDER);
//            req.start();
//            incAccessesCounter();
//            }
//        }
//
//
//    // DATA ACCESSED BY THREADS - LOCKING REQUIRED
//
//
//    private static int refreshcounter;
//
//    private static synchronized void incAccessesCounter() { refreshcounter++; }
//
//    public static synchronized String getWarehouseLocations() throws IOException, InterruptedException, ClassNotFoundException, SQLException {
//        /*
//        try {
//            serverIP = InetAddress.getByName("127.0.0.1");
//        } catch (UnknownHostException ex) {
//            System.out.println("Server invalido: " + "127.0.0.1");
//            System.exit(1);
//        }
//        sockAVG= new Socket(serverIP,9998);
//
//        DataOutputStream out =new DataOutputStream(sockAVG.getOutputStream());
//        byte[] message = new byte[5];
//
//        message[0]=1;
//        message[1]=8;
//        message[2]=0;
//        message[3]=0;
//        out.write(message);
//
//        Thread.sleep(2000);
//
//        DataInputStream sIn= new DataInputStream(sockAVG.getInputStream());
//        MessageDTO response= cod.Decoding(sIn);
//        ByteArrayInputStream in = new ByteArrayInputStream(response.getData_message());
//        ObjectInputStream is = new ObjectInputStream(in);
//        List<AGV> agvList= (List<AGV>) is.readObject();
//
//         */
//
//        List<AGVDTO> agvList= repository.findAll1();
//        String textHtml ="";
//        for(int i=0; i<agvList.size(); i++) {
//            String agvString="AGV ID:"+agvList.get(i).getId()+"\nAGV position:x="+agvList.get(i).getPositionx()+"y="+agvList.get(i).getPositiony();
//            textHtml = textHtml + "<tr><h3> " +
//                   " - "+agvString + " \n</h3></tr>";
//        }
//        textHtml = textHtml + "</ul><p>HTTP refresh counter: " + refreshcounter + "</p><hr>";
//
//        return textHtml;
//        }
//
//
//
//
//
//}
