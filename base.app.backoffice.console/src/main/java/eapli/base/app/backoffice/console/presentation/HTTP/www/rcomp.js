
// IMPORTANT: notice the next request is scheduled only after the
//            previous request is fully processed either successfully
//	      or not.

function refreshVotes() {
    var request = new XMLHttpRequest();
    var vBoard=document.getElementById("votes");

    request.onload = function() {
        vBoard.innerHTML = this.responseText;
        vBoard.style.color="black";
        setTimeout(refreshVotes, 10);

    };
    request.open("GET", "/votes", true);
    request.timeout = 6000;
    request.send();
}

	

