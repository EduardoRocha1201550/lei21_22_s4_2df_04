package eapli.base.app.backoffice.console.presentation.user;

import antlr.ANTLRException;
import eapli.base.LPROG.QuestionnaireControllerII;

import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.ParseCancellationException;


import java.io.IOException;




@SuppressWarnings({ "squid:S106" })
public class questionnaireUI extends AbstractUI {

    private final QuestionnaireControllerII controllerII=new QuestionnaireControllerII();

    @Override
    public boolean show() {
        return doShow();
    }
    protected boolean doShow() {
        String path = Console.readNonEmptyLine("Insert file path.", "Path required.");

        try {
            System.out.println("Path: " + path);
            controllerII.questionnaireValidation(path);
        } catch (ParseCancellationException | IOException ex ) {
            System.out.println("Invalid Grammar");
        }

        return true;
    }

    @Override
    public String headline() {
        return "Create Questionnaire";
    }

}
