package eapli.base.app.backoffice.console.presentation.salesclerk;

import eapli.base.costumer.domain.Costumer;

public class CostumerAction {
    public boolean execute() {
        return new CostumerUI().show();
    }
}
