/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.app.backoffice.console.presentation;

import eapli.SurveyUi;
import eapli.base.app.backoffice.console.presentation.WarehouseManager.Actions.ShowWarehousePlantUI;
import eapli.base.app.backoffice.console.presentation.WarehouseManager.JSONUI;
import eapli.base.app.backoffice.console.presentation.WarehouseManager.OrderToAGVUI;
import eapli.base.app.backoffice.console.presentation.WarehouseManager.OrderUpdateUI;
import eapli.base.app.backoffice.console.presentation.WarehouseManager.RegisterAGVUI;
import eapli.base.app.backoffice.console.presentation.salesclerk.*;
import eapli.base.app.backoffice.console.presentation.salesmanager.Actions.ReportUI;
import eapli.base.app.backoffice.console.presentation.user.askForQuestionsUI;
import eapli.base.app.backoffice.console.presentation.user.questionnaireUI;
import eapli.base.app.backoffice.console.presentation.userShoppingCart.userOrderUI;
import eapli.base.app.backoffice.console.presentation.userShoppingCart.userShoppingCartUI;
import eapli.base.app.common.console.presentation.authz.MyUserMenu;
import eapli.base.Application;
import eapli.base.app.backoffice.console.presentation.authz.AddUserUI;
import eapli.base.app.backoffice.console.presentation.authz.DeactivateUserAction;
import eapli.base.app.backoffice.console.presentation.authz.ListUsersAction;
import eapli.base.app.backoffice.console.presentation.clientuser.AcceptRefuseSignupRequestAction;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final String RETURN_LABEL = "Return ";

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // SETTINGS
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 1;


    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int SETTINGS_OPTION = 4;
    private static final int DISH_OPTION = 5;
    private static final int TRACEABILITY_OPTION = 6;
    private static final int MEALS_OPTION = 7;
    private static final int REPORTING_DISHES_OPTION = 8;

    private static final String SEPARATOR_LABEL = "--------------";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
        return renderer.render();
    }

    @Override
    public String headline() {

        return authz.session().map(s -> "Base [ @" + s.authenticatedUser().identity() + " ]")
                .orElse("Base [ ==Anonymous== ]");
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.ADMIN)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.addSubMenu(USERS_OPTION, usersMenu);
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.addSubMenu(SETTINGS_OPTION, settingsMenu);
        }
        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.WAREHOUSEMANAGER)) {
           final Menu menu= WarehouseMenu();
           mainMenu.addSubMenu(2,menu);
        }
        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.SALESCLERK)) {
            final Menu menu= SalesClerkMenu();
            mainMenu.addSubMenu(2,menu);
        }
        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.CLIENT_USER)) {
            final Menu menu= ClientMenu();
            mainMenu.addSubMenu(2,menu);
        }
        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.SALESMANAGER)) {
            final Menu menu= SalesManagerMenu();
            mainMenu.addSubMenu(2,menu);
        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }

    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.addItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ShowMessageAction("Not implemented yet"));
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }
    private Menu WarehouseMenu() {
        final Menu mainMenu = new Menu("WarehouseManager Menu:");
        mainMenu.addItem(1,"Import JSON file",new JSONUI()::show);
        mainMenu.addItem(2,"Assign Order to AGV",new OrderToAGVUI()::show);
        mainMenu.addItem(3,"Change Status", new OrderUpdateUI()::show);
        mainMenu.addItem(4,"ss",new ShowWarehousePlantUI()::show);


        return mainMenu;
    }
    private Menu SalesClerkMenu() {
        final Menu mainMenu = new Menu("SalesClerk Menu");
        mainMenu.addItem(1,"Add product",new AddProductUI()::show);
        mainMenu.addItem(2,"Register Category",new RegisterCategoryUI()::show);
        mainMenu.addItem(3,"Register Costumer",new CostumerUI()::show);
        mainMenu.addItem(4,"Configure AGV",new RegisterAGVUI()::show);
        mainMenu.addItem(5,"Register Order",new OrderUI()::show);
        mainMenu.addItem(6,"Delivering order",new costumerDeliveryUI()::show);




        return mainMenu;
    }
    private Menu ClientMenu() {
        final Menu mainMenu = new Menu("Client Menu");

        mainMenu.addItem(1,"My shopping cart",new userShoppingCartUI()::show);
        mainMenu.addItem(2,"See my orders",new userOrderUI()::show);
        mainMenu.addItem(3,"See my surveys",new SurveyUi()::show);
        return mainMenu;
    }

    private Menu SalesManagerMenu() {
        final Menu mainMenu = new Menu("Sales Manager Menu");
        mainMenu.addItem(1,"Questionnaire",new askForQuestionsUI()::show);
        mainMenu.addItem(2,"Questionnaire via path",new questionnaireUI()::show);
        mainMenu.addItem(3,"Statistical Report",new ReportUI()::show);

        return mainMenu;
    }


    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.addItem(ADD_USER_OPTION, "Add User", new AddUserUI()::show);
        menu.addItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction());
        menu.addItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction());
        menu.addItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }





}
