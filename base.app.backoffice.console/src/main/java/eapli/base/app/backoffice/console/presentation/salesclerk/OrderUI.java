package eapli.base.app.backoffice.console.presentation.salesclerk;
import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.LPROG.TypeRule;
import eapli.base.costumer.application.CostumerController;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.application.AddProductController;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.Brand;
import eapli.base.product.Product;
import eapli.base.salesmanagement.application.Product_OrderController;
import eapli.base.salesmanagement.domain.*;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;


import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class OrderUI extends AbstractUI {
    CostumerRepository costumerRepository= PersistenceContext.repositories().costumer();

    Product_OrderController controller=new Product_OrderController();
    AddProductController productController=new AddProductController();
    CostumerController costumerController=new CostumerController();


    @Override
    protected boolean doShow() {




        Iterable<Rules> rulesClient = null;
        int option;
        Costumer costumer=null;
        LinkedList<OrderItem> orderItems=new LinkedList<>();
        do{
            System.out.println("Product List:");
            Iterable<Product> listPro = productController.all();
            List<Product> productList=new LinkedList<>();
            int num=1;
            for (Product prod : listPro){
                System.out.println(num+". "+"Description: " + prod.getShort_description().getShort_description()+" Price: "+prod.getPrice());
                productList.add(prod);

                num++;
            }
            if (!orderItems.isEmpty()){
                System.out.println("0. Finish");
            }
            option= Console.readInteger("Please enter an option");
            if (option!=0){
                Product product=productList.get(option-1);
                Brand brand= product.getBrand();
                String forContent=brand.getBrand();
                Content content= new Content(forContent);

                 rulesClient= controller.findByContent(content);
                int quant=Console.readInteger("Quantity");
                OrderItem temp= controller.containsProduct(orderItems,product);
                if (temp==null){
                    orderItems.add(new OrderItem(new Quantity(quant),product));
                }else {
                    temp.setQuantity(new Quantity(temp.getQuantity().getQuantity()+quant));
                }
                System.out.println("Product added successfully");
            }
        }while (option!=0);
        int costumerID= Console.readInteger("Costumer ID:");
        Iterable<Costumer> costumers=costumerController.all();
        for (Costumer cost : costumers){
            if (cost.getId()==costumerID){
                costumer=cost;

                List<Rules> rulesBefore=  costumer.getRules();
                List<Rules> rulesList= (List<Rules>) rulesClient;
Iterator i=rulesList.listIterator();
Iterator i2=rulesBefore.listIterator();
for(int j=0;j<rulesList.size();j++ ){
    if(i2!=i){
        costumer.addRule(rulesList.get(j),costumer.getRules());

    }
}


            }
        }
        System.out.println(costumer.getRules());
        String address=Console.readLine("Delivering Address:");
        String postalCode=Console.readLine("Postal Code:");
        String city=Console.readLine("City");
        DeliveringAddress deliveringAddress=new DeliveringAddress(address,city,postalCode);

        System.out.println("Payment Method:");
        System.out.println("1. MB");
        System.out.println("2. Paypal");
        Integer optionPay=Console.readInteger("Please enter an option");
        Payment_Method paymentMethod=null;
        if (optionPay==1){
            paymentMethod=new Payment_Method(new OrderName("MB"),null);
        }else if (optionPay==2){
            paymentMethod=new Payment_Method(new OrderName("Paypal"),null);
        }

        System.out.println("Shipping Method:");
        System.out.println("1. CTT");
        System.out.println("2. UPS");
        Integer optionShip=Console.readInteger("Please enter an option");
        Shipping_Method shippingMethod=null;
        if (optionShip==1){
            shippingMethod=new Shipping_Method(new ShippingName("CTT"),new ShipPrice(3.5));
        }else if (optionShip==2){
            shippingMethod=new Shipping_Method(new ShippingName("CTT"),new ShipPrice(5));
        }

        Weight weight= controller.getWeightFromOrder(orderItems);
        Volume volume= controller.getTotalVolume(orderItems);
        OrderPrice price=controller.getPriceFromList(orderItems);
        price.setPrice(price.getPrice()+shippingMethod.getShipingPrice().getPrice());
        Product_Order order= controller.registerOrder(new Date(),deliveringAddress,volume,weight,costumer,shippingMethod,paymentMethod,new State("To Do"),orderItems,price);
        if (order!=null){
            System.out.println("Order info:");
            System.out.println(order);
            System.out.println("Order complete");
        }else System.out.println("Failed on registering your order");
        return false;
    }

    @Override
    public String headline() {
        return "Register Order";
    }
}
