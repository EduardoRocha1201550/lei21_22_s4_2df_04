package eapli.base.app.backoffice.console.presentation.salesclerk;
import eapli.base.generalmanagement.action.CategoryPrinter;
import eapli.base.generalmanagement.application.ListCategoryController;
import eapli.base.generalmanagement.domain.Category;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;


public class ListCategoryUI extends AbstractListUI<Category> {


    private final ListCategoryController controller = new ListCategoryController();


    @Override
    protected Iterable<Category> elements(){
        return this.controller.all();
    }

    @Override
    protected Visitor<Category> elementPrinter(){
        return new CategoryPrinter();
    }

    @Override
    protected String elementName(){
        return "Category:";
    }

    @Override
    protected String listHeader(){
        return "Categories";
    }

    @Override
    protected String emptyMessage(){
        return "No data.";
    }

    @Override
    public String headline(){
        return "List of Categories";
    }











}
