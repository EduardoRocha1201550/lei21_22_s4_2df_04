package eapli.base.app.backoffice.console.presentation.salesmanager.Actions;

import eapli.base.questionnaire.domain.Questionnaire;
import eapli.framework.visitor.Visitor;

public class QuestionnairePrinter implements Visitor<Questionnaire> {


    @Override
    public void visit(final Questionnaire visitee) {
        System.out.println(visitee.getId()+"\n"+visitee.getTitle()+"\n");
    }
}
