package eapli.base.app.backoffice.console.presentation.WarehouseManager.Actions;

import eapli.base.salesmanagement.domain.Product_Order;
import eapli.framework.visitor.Visitor;

public class OrderPrinter implements Visitor<Product_Order> {
    @Override
    public void visit(Product_Order visitee) {
        System.out.println(visitee.toString());
    }
}
