package eapli.base.app.backoffice.console.presentation.HTTP;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.warehousemanagement.application.CreateMatrixPlant;
import eapli.base.warehousemanagement.application.Warehouse;
import eapli.base.warehousemanagement.application.WarehousePlant;
import eapli.base.warehousemanagement.domain.PositionAGV;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.base.warehousemanagement.repositories.WarehousePlantRepository;

public class WarehouseControllerHttp {
    public AGVRepository repository = PersistenceContext.repositories().agv();
    public WarehousePlantRepository repo = PersistenceContext.repositories().warehousePlantRepository();


    public synchronized Iterable<AGV> getAGVStatus() {
        Iterable<AGV> agvList = repository.findAll();

        return agvList;
    }

    public synchronized WarehousePlant getPlant() {
        WarehousePlant plant = repo.findAll().iterator().next();
        return plant;
    }

}