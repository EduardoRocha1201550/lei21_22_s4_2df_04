package eapli.base.app.backoffice.console.presentation.salesmanager.Actions;

import eapli.base.generalmanagement.application.QuestionAnswer;
import eapli.base.generalmanagement.application.StatisticalReportController;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.questionnaire.domain.Questionnaire;
import eapli.base.questionnaire.repositories.QuestionnaireRepository;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.util.HashMap;
import java.util.List;

public class ReportUI extends AbstractUI {
    QuestionnaireRepository repository= PersistenceContext.repositories().questionnaireRepository();
    StatisticalReportController questionnaireController=new StatisticalReportController();
    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow() {
        SelectWidget<Questionnaire>selectWidget=new SelectWidget<>("Select the questionnaire",repository.findAll(),new QuestionnairePrinter());
        selectWidget.show();
        Questionnaire questionnaire=selectWidget.selectedElement();
        List<QuestionAnswer> list=null;
        try {
            list= questionnaireController.getStats(questionnaire);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<Long,List<QuestionAnswer>>map=questionnaireController.getQuestionsOrdered(list);
        System.out.println(questionnaireController.setReport(map,questionnaire));
        return true;
    }

    @Override
    public String headline() {
        return "null";
    }
}
