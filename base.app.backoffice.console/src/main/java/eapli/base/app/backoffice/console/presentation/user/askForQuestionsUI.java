package eapli.base.app.backoffice.console.presentation.user;/*
 * Copyright (c) 2013-2022 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */




//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.net.InetAddress;
//import java.net.Socket;
//import java.net.UnknownHostException;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import java.util.List;


import eapli.base.questionnaire.application.QuestionnaireController;
import eapli.base.questionnaire.domain.Answer;
import eapli.base.questionnaire.domain.Question;
import eapli.base.questionnaire.domain.Section;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;



@SuppressWarnings({ "squid:S106" })
public class askForQuestionsUI extends AbstractUI {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final QuestionnaireController controller= new QuestionnaireController();


    @Override
    public boolean show() {
        return doShow();
    }
    protected boolean doShow() {


        List<Question> questions= new ArrayList<>();
        List<String> answersList=new ArrayList<>();
        List<Section> sectionsList=new ArrayList<>();
        Section section= new Section();

        Question questionObject= new Question();
        boolean sectionBoolean=false;
        boolean questionBoolean=true;
        boolean multipleChoice=true;
        String id = Console.readLine("Questionnaire Id:");

        String title = Console.readLine("Questionnaire Title:");

        //Start Welcome Message
        String welcome_message = "";

        String option = Console.readLine("Would you like to have a Welcome Message in your questionnaire? (Y/N)");

        if(option.equalsIgnoreCase("Y"))
            welcome_message = Console.readLine("Questionnaire Welcome Message:");
        //End Welcome Message


        do{
            long sectionId = Console.readLong("Section Id:");

            String sectionTitle = Console.readLine("Section Title:");

            String sectionDescription = Console.readLine("Section description:");

            int sectionObligatorines = Console.readInteger("Section Obligatorines:\n{mandatory - 1}\n{optional - 2\n{dependent - 2");
            String obligatorines ;
            switch (sectionObligatorines){
                case 1:
                    obligatorines="{mandatory}";
                    break;
                case 2:
                    obligatorines="{optional}";
                    break;
                case 3:
                    obligatorines="{dependent}";
                    break;
                default:
                    continue;
            }

            int sectionRepeat = Console.readInteger("Section Repeat:");



            while(questionBoolean) {
                long questionId = Console.readInteger("Question Id:");

                String question = Console.readLine("Question:");

                int questionObligatorines = Console.readInteger("Question Obligatorines:\n{mandatory - 1}\n{optional - 2\n{dependent - 3");
                String obligatorinesQuestion;

                switch (questionObligatorines) {
                    case 1:
                        obligatorinesQuestion = "{mandatory}";
                        break;
                    case 2:
                        obligatorinesQuestion = "{optional}";
                        break;
                    case 3:
                        obligatorinesQuestion = "{dependent}";
                        break;
                    default:
                        continue;

                }
                int type = Console.readInteger("What type of question do you want: Open answer-1 Multiple choice-2 Single-Choice-3");

                if (type == 1) {
                    //Opcao de resposta aberta
                }
                if (type == 2) {
                    boolean options = true;
                    int counterOptions = 0;

                    while (options) {
                        System.out.println("Please insert an option");
                        String option1 = Console.readLine("Option:");
                        counterOptions++;
                        answersList.add(option1);
                        int optionverif = Console.readInteger("Do you want to introduce another option answer?: Yes-1 No-2");
                        if (optionverif == 2 && counterOptions < 2) {
                            System.out.println("Please insert more options for a Multiple choice question.");
                        } else if (optionverif == 1) {

                        } else {
                            options = false;
                        }
                    }
                }
                if (type == 3) {
                    boolean options = true;
                    while (options) {
                        System.out.println("Please insert an option");
                        String option1 = Console.readLine("Option:");
                        answersList.add(option1);
                        int optionverif = -1;
                        while (optionverif != 2) {
                            optionverif = Console.readInteger("Do you want to introduce another option answer?: Yes-1 No-2");
                            if (optionverif == 2) {
                                options = false;
                            } else if (optionverif == 1) {
                            } else {
                                System.out.println("Insert a valid number:\n");
                            }
                        }
                    }
                }

                int questionVerif = Console.readInteger("Want to create a new question?: Yes-1 No-2");

                questionObject = new Question(questionId, question, obligatorinesQuestion, type, answersList);
                questions.add(questionObject);

                if (questionVerif == 2) {
                    questionBoolean = false;
                }
            }

            int sectionVerif = Console.readInteger("Want to create a new section?: Yes-1 No-2");

            section = new Section(sectionId, sectionTitle, sectionDescription, obligatorines, sectionRepeat, questions);
            sectionsList.add(section);
            if (sectionVerif == 2) {
                sectionBoolean = false;
            }


        } while (sectionBoolean);

        String final_message = Console.readLine("Questionnare final message:");

        try {
            this.controller.ExportQuestionnaireToFile(id,title,welcome_message,final_message,sectionsList);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            this.controller.createNewQuestionnaire(id,title,welcome_message,final_message,sectionsList);
        } catch (Exception e) {
            System.out.println("------------------------------------------------------------------");
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public String headline() {
        return "Create Questionnaire";
    }

}
