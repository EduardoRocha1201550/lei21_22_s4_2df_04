package eapli.base.app.backoffice.console.presentation.WarehouseManager;

import eapli.base.generalmanagement.action.AGVDockPrinter;
import eapli.base.generalmanagement.domain.*;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.repositories.AGVDocksRepository;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;

import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

public class RegisterAGVUI extends AbstractUI {
    private final AGVDocksRepository repository = PersistenceContext.repositories().agvDocks();
    private final AGVRepository AGVrepository = PersistenceContext.repositories().agv();
    @Override
    public boolean show() {
        return doShow();
    }
    protected boolean doShow(){
    try {
        final String idString = Console.readLine("ID do AGV :\n");
        final double maxvolumeDouble = Console.readDouble("Max volume :\n");
        final double maxweight = Console.readDouble("Max weight :\n");
        final int RouteInt = Console.readInteger("Route :\n");
        final String Model1 = Console.readLine("Model :\n");
        final String shortDescriptionString = Console.readLine("Short Description :\n");
        final String autonomyString = Console.readLine("Autonomy : \n");
        final Iterable<AGVDocks> listAGVDock = repository.findAllAGVnull();
        final SelectWidget<AGVDocks> selectorCat = new SelectWidget<>("Select a AGVDock", listAGVDock, new AGVDockPrinter());
        selectorCat.show();
        AGVDocks agvDocks = selectorCat.selectedElement();
        String id = idString;
        MaxWeight maxWeight1 = new MaxWeight(maxweight);
        Model model1 = new Model(Model1);
        MaxVolume maxVolume = new MaxVolume(maxvolumeDouble);
        Route route1 = new Route(RouteInt);
        ShortDescription shortDescription1 = new ShortDescription(shortDescriptionString);
        Autonomy autonomy1 = new Autonomy(autonomyString);
        AGV agv = new AGV(id, maxVolume, maxWeight1, model1, route1, shortDescription1, AGV.Status.FREE, autonomy1, agvDocks,null);
        AGVrepository.save(agv);
        repository.save(agvDocks);
        System.out.println("Operation Completed");
    }catch (Exception e){
        System.out.println(e.getMessage());
    }
        return false;
    }

    @Override
    public String headline(){
        return "Register AGV";
    }
}
