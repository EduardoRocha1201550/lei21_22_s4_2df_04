package eapli.base.app.backoffice.console.presentation.WarehouseManager.Actions;

import eapli.base.generalmanagement.domain.AGV;
import eapli.framework.visitor.Visitor;

public class AGVPrinter implements Visitor<AGV> {
    @Override
    public void visit(AGV visitee) {
        System.out.println(visitee.toString());
    }
}
