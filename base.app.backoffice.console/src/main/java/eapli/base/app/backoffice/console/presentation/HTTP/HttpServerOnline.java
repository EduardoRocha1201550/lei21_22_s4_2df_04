package eapli.base.app.backoffice.console.presentation.HTTP;


import ch.qos.logback.core.net.ssl.SSL;
import eapli.CodingAndDecoding;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.Internal_code;
import eapli.base.salesmanagement.domain.*;
import eapli.base.warehousemanagement.application.CreateMatrixPlant;
import eapli.base.warehousemanagement.application.WarehousePlant;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.PositionAGV;
import eapli.base.warehousemanagement.repositories.AGVDocksRepository;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.base.warehousemanagement.repositories.WarehousePlantRepository;
import eapli.framework.io.util.Console;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import javax.net.ssl.SSLSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.IllegalFormatCodePointException;
import java.util.List;

/**
 * @author ANDRE MOREIRA (asc@isep.ipp.pt)
 */
public class HttpServerOnline {
    static private final String BASE_FOLDER = "/var/www/Test/www";
    static private final CreateMatrixPlant crea = new CreateMatrixPlant();
    static private final AGVRepository agvConfigurationRepository = PersistenceContext.repositories().agv();
    static private SSLServerSocket sock;
    private static AGVRepository repository = PersistenceContext.repositories().agv();
    static InetAddress serverIP;
    static SSLSocket sockAVG;
    static private final WarehousePlantRepository warehousePlantRepository = PersistenceContext.repositories().warehousePlantRepository();
    private static final CostumerRepository costumerRepository= PersistenceContext.repositories().costumer();
    private static final ProductRepository productRepository= PersistenceContext.repositories().products();

    static private final PositionAGV position = new PositionAGV(1, 2);
    static private final PositionAGV position1 = new PositionAGV(17, 3);
    static private final PositionAGV position2 = new PositionAGV(1, 12);


    static private final PositionAGV destino2 = new PositionAGV(1, 6);
    static private final PositionAGV destino1 = new PositionAGV(6, 4);
    static private final PositionAGV destino3 = new PositionAGV(4, 11);



    static private final WarehouseControllerHttp cntrl = new WarehouseControllerHttp();


    public static void main(String args[]) throws Exception {
        OrderItem orderItem=new OrderItem(new Quantity(1),productRepository.findbyID(new Internal_code("www")));
        OrderItem orderItem1=new OrderItem(new Quantity(1),productRepository.findbyID(new Internal_code("wwf")));
        OrderItem orderItem2=new OrderItem(new Quantity(1),productRepository.findbyID(new Internal_code("ww1")));


        List<Product_Order> list= new ArrayList<>();
        List<OrderItem> list1=new ArrayList<>();
        Shipping_Method shippingMethod=new Shipping_Method(new ShippingName("CTT"),new ShipPrice(3.5));
        DeliveringAddress deliveringAddress=new DeliveringAddress("1","Aveiro","4590-357");
        eapli.base.salesmanagement.domain.Volume volume=new eapli.base.salesmanagement.domain.Volume(2);
        eapli.base.salesmanagement.domain.Weight weight=new eapli.base.salesmanagement.domain.Weight(2);
        OrderName orderName=new OrderName("nome");
        Payment_Confirmation method1=new Payment_Confirmation("123131231231");
        eapli.base.salesmanagement.domain.State state=new eapli.base.salesmanagement.domain.State(eapli.base.salesmanagement.domain.State.STATETODO);
        OrderPrice orderPrice=new OrderPrice(2);
        Payment_Method method=new Payment_Method(orderName,method1);
        Costumer costumer1=costumerRepository.findbyID(7);

        list1.add(orderItem);
        list1.add(orderItem1);
        list1.add(orderItem2);


        Product_Order order=new Product_Order(new Date(),deliveringAddress,volume,weight,costumer1,shippingMethod,method,state,orderPrice,list1);
        order.setId(17l);
        System.out.println("Orders: \n1. "+ order.getId());

            System.out.println("Order products:");
            int i=0;
            for (OrderItem orderItem10 : list1){
                i++;
                System.out.println(i+". Quantity: "+ list1.get(i-1).getQuantity().getQuantity()+ " Product: "+ list1.get(i-1).getProduct().getInternal_code());
            }
            System.out.println("\nPositions");
            System.out.println("Position Product 1 x= 1 y= 5");
            System.out.println("Position Product 2 x= 1 y= 4");
            System.out.println("Position Product 3 x= 4 y= 11");
            System.out.println("\nServer will soon start");


        SSLSocket cliSock;

        if (args.length != 1) {
            System.out.println("Local port number required at the command line.");
            System.exit(1);
        }

        System.setProperty("javax.net.ssl.keyStore", "server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "forgotten");

        refreshcounter = 0;


        try {
            SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            sock = (SSLServerSocket) sslF.createServerSocket(Integer.parseInt(args[0]));


        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + args[0]);
            System.exit(1);
        }

        while (true) {
            cliSock = (SSLSocket) sock.accept();
            cliSock.setEnabledProtocols(new String[]{"TLSv1.2"});
            HttpClient req = new HttpClient(cliSock, BASE_FOLDER);
            req.start();
            incAccessesCounter();
        }
    }


    // DATA ACCESSED BY THREADS - LOCKING REQUIRED


    private static int refreshcounter;

    private static synchronized void incAccessesCounter() {
        refreshcounter++;
    }


    /*public static synchronized String getWarehouseLocations() throws IOException, InterruptedException, ClassNotFoundException {
     *//*
        try {
            serverIP = InetAddress.getByName("127.0.0.1");
        } catch (UnknownHostException ex) {
            System.out.println("Server invalido: " + "127.0.0.1");
            System.exit(1);
        }
        sockAVG= new Socket(serverIP,9998);

        DataOutputStream out =new DataOutputStream(sockAVG.getOutputStream());
        byte[] message = new byte[5];

        message[0]=1;
        message[1]=8;
        message[2]=0;
        message[3]=0;
        out.write(message);

        Thread.sleep(2000);

        DataInputStream sIn= new DataInputStream(sockAVG.getInputStream());
        MessageDTO response= cod.Decoding(sIn);
        ByteArrayInputStream in = new ByteArrayInputStream(response.getData_message());
        ObjectInputStream is = new ObjectInputStream(in);
        List<AGV> agvList= (List<AGV>) is.readObject();

         *//*
    }*/
    public static synchronized String getVotesStandingInHTML() {
        Iterable<AGV> agvs = cntrl.getAGVStatus();
        WarehousePlant warehousePlant = cntrl.getPlant();

        String[][] plant = crea.createMatrix(warehousePlant);
        StringBuilder textHtml = new StringBuilder("<table>");


        //parte de scomp
        //  plant[6][1] = "0";//obstaculo demostracao
        //    plant[position1.getyPos()][position1.getxPos()]="A";
        plant[position.getPositiony()][position.getPositionx()] = "A1";
        plant[position1.getPositiony()][position1.getPositionx()] = "A2";
        plant[position2.getPositiony()][position2.getPositionx()] = "A3";


        if (destino1.getPositionx() == position1.getPositionx() && destino1.getPositiony() == position1.getPositiony()) {
            destino1.setPositionx(position1.getPositionx());
            destino1.setPositionx(position1.getPositiony());
        }

////////////////////////////////////////////
        if (position1.getPositiony() < destino2.getPositiony() && plant[position1.getPositiony() + 1][position1.getPositionx()] == null) {
            position1.setPositiony(position1.getPositiony() + 1);

        } else if (position1.getPositiony() > destino2.getPositiony() && plant[position1.getPositiony() - 1][position1.getPositionx()] == null) {

            position1.setPositiony(position1.getPositiony() - 1);
        } else if (position1.getPositionx() < destino2.getPositionx() && plant[position1.getPositiony()][position1.getPositionx() + 1] == null) {
            position1.setPositionx(position.getPositionx() + 1);

        } else if (position1.getPositionx() > destino2.getPositionx() && plant[position1.getPositiony()][position1.getPositionx() - 1] == null) {
            position1.setPositionx(position1.getPositionx() - 1);
        }

   ////////////////////////////////////////
        if (destino2.getPositionx() == position.getPositionx() && destino2.getPositiony() == position.getPositiony()) {
            destino2.setPositionx(position.getPositionx());
            destino2.setPositionx(position.getPositiony());
        }

        if (position.getPositiony() < destino2.getPositiony() && plant[position.getPositiony() + 1][position.getPositionx()] == null) {
            position.setPositiony(position.getPositiony() + 1);

        } else if (position.getPositiony() > destino2.getPositiony() && plant[position.getPositiony() - 1][position.getPositionx()] == null) {

            position.setPositiony(position.getPositiony() - 1);
        } else if (position.getPositionx() < destino2.getPositionx() && plant[position.getPositiony()][position.getPositionx() + 1] == null) {
            position.setPositionx(position.getPositionx() + 1);

        } else if (position.getPositionx() > destino2.getPositionx() && plant[position.getPositiony()][position.getPositionx() - 1] == null) {
            position.setPositionx(position.getPositionx() - 1);
        }

        ///////////////////////////////////////////////////////

        if (destino3.getPositionx() == position2.getPositionx() && destino3.getPositiony() == position2.getPositiony()) {
            destino3.setPositionx(position2.getPositionx());
            destino3.setPositionx(position2.getPositiony());

        }
        if (position2.getPositiony() < destino3.getPositiony() && plant[position2.getPositiony() + 1][position2.getPositionx()] == null) {
            position2.setPositiony(position2.getPositiony() + 1);

        } else if (position2.getPositiony() > destino3.getPositiony() && plant[position2.getPositiony() - 1][position2.getPositionx()] == null) {

            position2.setPositiony(position2.getPositiony() - 1);
        } else if (position2.getPositionx() < destino3.getPositionx() && plant[position2.getPositiony()][position2.getPositionx() + 1] == null) {
            position2.setPositionx(position2.getPositionx() + 1);

        } else if (position2.getPositionx() > destino3.getPositionx() && plant[position2.getPositiony()][position2.getPositionx() - 1] == null) {
            position2.setPositionx(position2.getPositionx() - 1);
        }

        //////////////////////////////////////////////
/*
        if (destino4.getPositionx() == position3.getPositionx() && destino4.getPositiony() == position3.getPositiony()) {
            destino4.setPositionx(position3.getPositionx());
            destino4.setPositionx(position3.getPositiony());

        }
        if (position3.getPositiony() < destino4.getPositiony() && plant[position3.getPositiony() + 1][position3.getPositionx()] == null) {
            position3.setPositiony(position3.getPositiony() + 1);

        } else if (position3.getPositiony() > destino4.getPositiony() && plant[position3.getPositiony() - 1][position3.getPositionx()] == null) {

            position3.setPositiony(position3.getPositiony() - 1);
        } else if (position3.getPositionx() < destino4.getPositionx() && plant[position3.getPositiony()][position3.getPositionx() + 1] == null) {
            position3.setPositionx(position3.getPositionx() + 1);

        } else if (position3.getPositionx() > destino4.getPositionx() && plant[position3.getPositiony()][position3.getPositionx() - 1] == null) {
            position3.setPositionx(position3.getPositionx() - 1);
        }*/
//parte de scomp


        for (String[] strings : plant) {
            textHtml.append("<tr>");
            for (int j = 0; j < plant[0].length; j++) {
                if (strings[j] == null) {
                    textHtml.append("<td width=30px height=30px>").append(" ").append("</td>");
                } else {

                    if (strings[j].charAt(0) == 'D') {
                        textHtml.append("<th style=\"background-color:#08FF00\";>").append(strings[j]).append("</th>");
                    } else if (strings[j].charAt(0) == 'A') {
                        textHtml.append("<th style=\"background-color:#5A8AF1\";>").append(strings[j]).append("</th>");
                    } else {
                        textHtml.append("<th style=\"background-color:#34856734\";>").append(strings[j]).append("</th>");
                    }

                }
            }
            textHtml.append("</tr>");
        }
        textHtml.append("</table>");

        for (AGVDocks agvD : warehousePlant.getAGVDocks()) {
            int i = 0;
            for (AGV agv : agvs) {
                if (agv.getAgvDocks().getId().equals(agvD.getId())) {
                    textHtml.append("</br>").append(agvD.getId().getId()).append("--->").append(agv.getId()).append(": ").append(agv.getStatus());
                    i++;
                }
            }

            if (i != 1) {
                textHtml.append("</br>").append(agvD.getId().getId()).append("--->").append("no AGV Detected!");

            }

        }


        textHtml.append("</br>HTTP server accesses counter: ").append(refreshcounter);
        return textHtml.toString();
    }
}