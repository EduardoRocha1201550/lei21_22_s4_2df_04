package eapli.base.app.backoffice.console.presentation.salesclerk;

import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.warehousemanagement.application.OrderDoneController;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

import java.util.ArrayList;
import java.util.List;

public class costumerDeliveryUI extends AbstractUI {
    private final OrderDoneController controller=new OrderDoneController();

    @Override
    protected boolean doShow() {

        List<Product_Order> shippingOrders =new ArrayList<>();
        List<Product_Order> doneOrders =new ArrayList<>();
        doneOrders = controller.findOrderDone();
        int num=0;
        for (Product_Order cat : doneOrders){
            System.out.println(num+". "+cat.toString());
            shippingOrders.add(cat);
            num++;
        }

        Integer option1= Console.readInteger("Please enter an option");
        Product_Order order=shippingOrders.get(option1);
        controller.changeDoneToBeingShipped(order.getId());
        System.out.println("Order Updated!");
        return true;
    }

    public boolean show(){

        return doShow();
    }
    @Override
    public String headline() {
        return null;
    }
}
