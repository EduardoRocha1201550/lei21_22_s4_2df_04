package eapli.base.app.backoffice.console.presentation.salesclerk;

import eapli.base.generalmanagement.application.RegisterCategoryController;
import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.generalmanagement.domain.Description;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

public class RegisterCategoryUI extends AbstractUI {

    private final RegisterCategoryController controller = new RegisterCategoryController();


    @Override
    protected boolean doShow(){
        Code novoCode=null;
        Description novoDescription=null;
        System.out.println("1. Show all categories");
        System.out.println("2. Create a category");
        System.out.println("3. Exit");
        Integer option = Console.readInteger("");
        if (option==1){
            ListCategoryUI listCategoryUI=new ListCategoryUI();
            Iterable<Category> categories= listCategoryUI.elements();
            for (Category cat : categories){
                System.out.println(cat.toString());
            }
        }else if (option==2){
            boolean verif=false;
            while (!verif){
                try{
                    verif=true;
                    novoCode=new Code(Console.readLine("Code:"));
                    novoDescription = new Description(Console.readLine("Description:"));
                }catch (Exception e){
                    System.out.println(e.getMessage() + "\nTry again\n");
                    verif=false;
                }
            }
            boolean verif1;
            try {
                verif1=true;
                this.controller.registerCategory(novoCode, novoDescription);
            } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
                verif1=false;
                System.out.println("That code is already in use.");
            }
            if (verif1){
                System.out.println("Category successfully created");
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Category";
    }
}