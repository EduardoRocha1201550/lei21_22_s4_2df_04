package eapli;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.framework.io.util.Console;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;


public class TcpCliente {


    private DataOutputStream sOut;
    private DataInputStream sIn;
    static final int SERVER_PORT=10653; // Port AGVManager
    static final String KEYSTORE_PASS="forgotten";

    static InetAddress serverIP;
    static SSLSocket sock;;
    private static CodingAndDecoding cod= new CodingAndDecoding();

    public static void main(String[] args) throws Exception {

        System.setProperty("javax.net.ssl.trustStore", "base.AGVManager/src/main/java/client1_J.jks");
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key for client certificate when requested by the server
        System.setProperty("javax.net.ssl.keyStore","base.AGVManager/src/main/java/client1_J.jks");
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();

        try { serverIP = InetAddress.getByName("vsgate-s1.dei.isep.ipp.pt"); }
        catch(UnknownHostException ex) {
            System.out.println("Invalid server specified: vsgate-s1.dei.isep.ipp.pt");
            System.exit(1); }


        try {
            sock = (SSLSocket) sf.createSocket(serverIP,SERVER_PORT);
        }
        catch(IOException ex) {
            System.out.println("Failed to connect to the server" + SERVER_PORT);
            System.out.println("Application aborted.");
            System.exit(1);
        }

        System.out.println("Connected to the server" + SERVER_PORT);


        try {
            sock.startHandshake();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int option= Integer.parseInt(Console.readLine("Insert the option \n1.AGV Change Status\n2.FIFO"));

        if (option==1){
            changeStatus("A2");
        }

        if(option==2){
            FIFO();
        }

        sock.close();

    }
    public static boolean changeStatus(String agvID) throws IOException, InterruptedException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(agvID);
        oOut.flush();
        byte[] data = bos.toByteArray();
        byte[] message = new byte[data.length+4];

        message[0]=1;
        message[1]=4;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        message[2]= (byte) size1;
        message[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            message[i+4]=data[i];
        }
        out.write(message);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        if (messageDTO.getData_message()[0]==1){
            System.out.println("Success");
            return true;
        }else{
            System.out.println("Not able to change AGV status");
        }
        return false;

    }

    public static List<Product_Order> productOrdersState() throws IOException, InterruptedException, ClassNotFoundException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());


        byte[] message = new byte[4];

        message[0]= 1;
        message[1]= 6;
        message[2]= 0;
        message[3]= 0;

        out.write(message);

        Thread.sleep(500);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        ByteArrayInputStream in = new ByteArrayInputStream(messageDTO.getData_message());
        ObjectInputStream is = new ObjectInputStream(in);
        List<Product_Order> orderList= (List<Product_Order>) is.readObject();
        for (Product_Order order : orderList){
            System.out.println(order);
        }

        return orderList;
    }
    public static boolean FIFO() throws IOException, InterruptedException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        byte[] message = new byte[5];
        message[0]=1;
        message[1]=7;
        message[2]=0;
        message[3]=0;
        out.write(message);
        Thread.sleep(20000);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        if (messageDTO.getData_message()[0]==1){
            System.out.println("Success");
            return true;
        }else{
            System.out.println("Not able to assign all the product orders that are ready to be assigned to an AGV with the FIFO algorithm");
        }
        return false;
    }
    public void AVAILABLEAGVS () throws IOException, InterruptedException, ClassNotFoundException {
        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        byte[] message = new byte[5];
        message[0]=1;
        message[1]=5;
        message[2]=0;
        message[3]=0;
        out.write(message);
        Thread.sleep(200);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());

        ByteArrayInputStream in = new ByteArrayInputStream(messageDTO.getData_message());
        ObjectInputStream is = new ObjectInputStream(in);
        List<AGV> agvList= (List<AGV>) is.readObject();
        for (AGV agv : agvList){
            System.out.println(agv);
        }
    }
}