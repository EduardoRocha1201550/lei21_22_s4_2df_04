package eapli;

import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.repositories.RulesRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

import java.util.List;

public class SurveyUi extends AbstractUI {
    RulesRepository rulesRepository=PersistenceContext.repositories().rulesRepository();
   CostumerRepository costumerRepository= PersistenceContext.repositories().costumer();
SurveyController surveyController=new SurveyController();
    @Override
    public boolean show() {
        return doShow();
    }
    @Override
    protected boolean doShow() {

        int id=Console.readInteger("Input your costumer id");
        Costumer costumer=costumerRepository.findbyID(id);
        List<Rules> rules=costumer.getRules();
        System.out.println("You have this surveys\n"+costumer.getRules() +"\n"+"Do you want to answer any of them? Choose the survey or enter 0 to leave");
        String option= Console.readLine("Enter the option");
        Content content= new Content(option);
        Iterable<Rules> rule= rulesRepository.findByContent(content);
        try {
            surveyController.ShowMySurveys(rule,id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    return true;
    }

    @Override
    public String headline() {
        return null;
    }
}
