package eapli;

import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.app.bootstrap.GetAnswer;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.repositories.RulesRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.framework.io.util.Console;

import java.util.List;

public class SurveyController {
    private CostumerRepository costumerRepository= PersistenceContext.repositories().costumer();
    private RulesRepository rulesRepository= PersistenceContext.repositories().rulesRepository();

    public void ShowMySurveys(Iterable<Rules> rules,int id) throws Exception {

        if(rules!=null){
            GetAnswer.VerifyRules(rules,id);
        }


    }
}
