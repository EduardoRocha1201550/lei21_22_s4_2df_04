package eapli;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class CodingAndDecoding {


    public MessageDTO Decoding(DataInputStream r){

        try {
            int version =r.read();
            int code =r.read();
            int size1= r.read();
            int size2=  r.read();

            int size=size1+256*size2;
            byte[] data_message=new byte[size];
            r.read(data_message,0,size);

            MessageDTO message= new MessageDTO(version,code,size,data_message);
            return message;
        } catch (IOException e) {
            System.out.println(e.getMessage());;
        }
        return null;
    }


    public byte[] Coding_String(int version,int code,String message){
        int data_byte_lenght = message.getBytes(StandardCharsets.UTF_8).length;;
        int lenght= 4+  data_byte_lenght;
        byte[] coded_message= new byte[lenght];
        coded_message[0]= (byte) version;
        coded_message[1]=(byte) code;
        coded_message[2] = (byte) (data_byte_lenght%256);
        coded_message[3] = (byte) (data_byte_lenght/256);
        int data_lenght=coded_message[2]+ coded_message[3];
        for (int i = 0 ; i< data_lenght; i++){
            coded_message[4+i] = Byte.parseByte(message);
        }
        return coded_message;
    }

    public byte[] Coding_int(int version,int code, int message){
        int data_byte_lenght = 4;
        int lenght= 4+  data_byte_lenght;
        byte[] coded_message= new byte[lenght];
        coded_message[0]= (byte) version;
        coded_message[1]=(byte) code;
        coded_message[2] = (byte) (data_byte_lenght%256);
        coded_message[3] = (byte) (data_byte_lenght/256);
        coded_message[4]= (byte) message;
        return coded_message;
    }
    public byte[] Coding_Object(int version,int code, Object object) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(object);
        oOut.flush();
        byte[] data = bos.toByteArray();

        byte[] ResponseMessage = new byte[data.length+4];
        ResponseMessage[0]=1;
        ResponseMessage[1]=(byte) code;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        ResponseMessage[2]= (byte) size1;
        ResponseMessage[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            ResponseMessage[i+4]=data[i];
        }
        return ResponseMessage;
    }
}
