package eapli;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.salesmanagement.domain.Product_Order;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

public class TcpClienteOrder  {
    private DataOutputStream sOut;
    private DataInputStream sIn;
    static InetAddress serverIP;
    static Socket sock;

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Server IPv4/IPv6 address ou DNS é requerido");
            System.exit(1);
        }
        try {
            serverIP = InetAddress.getByName(args[0]);
        } catch (UnknownHostException ex) {
            System.out.println("Server invalido: " + args[0]);
            System.exit(1);
        }

        try {
            sock = new Socket(serverIP, 9995
            );
            DataOutputStream out =new DataOutputStream(sock.getOutputStream());
            byte[] message = new byte[5];
            message[0]=1;
            message[1]=3;
            message[2]=1;
            message[3]=4;
            message[4]=7;


            out.write(message);

            Thread.sleep(100);

            ObjectInputStream inputStream=new ObjectInputStream(sock.getInputStream());
            List<Product_Order> orders= (List<Product_Order>) inputStream.readObject();
            System.out.println(orders.get(0).getId());
            System.out.println("Size: " + orders.size());
            sock.close();

        } catch (IOException ex) {
            System.out.println("Erro ao establecer a ligação TCP");
            System.exit(1);
        }
    }


}