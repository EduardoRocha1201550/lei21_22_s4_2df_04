package eapli.base.persistence.impl.jpa;

import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;

import javax.persistence.TypedQuery;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JpaOrderRepository extends BaseJpaRepositoryBase<Product_Order,Long,Long> implements Product_OrderRepository {
    Connection conn;

    {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:h2:tcp://vsgate-s2.dei.isep.ipp.pt:10734/OrdersServer;MV_STORE=FALSE;AUTO_SERVER=true","sa", "eapli");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public JpaOrderRepository() {
        super("id");
    }

    @Override
    public List<Product_Order> findAvailable() {
            final TypedQuery<Product_Order> q=createQuery("Select e FROM Product_Order e WHERE e.state=:state",Product_Order.class);
            q.setParameter("state",new State(State.STATETODO) );
            return q.getResultList();
    }

    @Override
    public List<Product_Order> findAvailableDoing() {
        final TypedQuery<Product_Order> q=createQuery("Select e FROM Product_Order e WHERE e.state=:state",Product_Order.class);
        q.setParameter("state",new State(State.STATEDOING) );
        return q.getResultList();
    }

    @Override
    public List<Product_Order> findAvailableDone() {
        final TypedQuery<Product_Order> q=createQuery("Select e FROM Product_Order e WHERE e.state=:state",Product_Order.class);
        q.setParameter("state",new State(State.STATEDONE) );
        return q.getResultList();
    }

    @Override
    public Product_Order findOrderById(Long id){
        final TypedQuery<Product_Order> q=createQuery("SELECT e FROM Product_Order e WHERE e.id =:id ",Product_Order.class);
        q.setParameter("id",id);
        return q.getSingleResult();
    }

    @Override
    public List<Product_Order> getOrdersFromCostumer(int costumer_id) throws SQLException {
        Statement stmt;
        ResultSet rset = null;
        try {
            stmt = conn.createStatement();
            String query="select * from Product_Order where costumer_id="+costumer_id;
            rset = stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Product_Order> list = new ArrayList<>();
        while(rset.next()){
            long id=rset.getLong(1);
            TypedQuery<Product_Order> q=createQuery("SELECT e FROM Product_Order e WHERE e.id =:id ",Product_Order.class);
            q.setParameter("id",id);
            list.add(q.getSingleResult());
        }

        return list;
    }


}
