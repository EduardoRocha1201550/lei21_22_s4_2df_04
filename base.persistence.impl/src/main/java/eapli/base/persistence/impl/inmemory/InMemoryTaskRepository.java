package eapli.base.persistence.impl.inmemory;


import eapli.base.generalmanagement.domain.Task;
import eapli.base.generalmanagement.repositories.TaskRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryTaskRepository extends InMemoryDomainRepository<Task,Integer> implements TaskRepository {
    static {
        InMemoryInitializer.init();
    }

    @Override
    public Task findByOrder(Long id) {
        return null;
    }
}
