package eapli.base.persistence.impl.jpa;

import eapli.base.warehousemanagement.domain.Aisles;
import eapli.base.warehousemanagement.repositories.AislesRepository;

public class JPAAisleRepository extends BaseJpaRepositoryBase<Aisles,Integer,Integer> implements AislesRepository {


    JPAAisleRepository() {
        super("Id1");
    }
}
