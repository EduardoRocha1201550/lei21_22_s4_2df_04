package eapli.base.persistence.impl.jpa;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.product.Internal_code;
import eapli.base.salesmanagement.domain.Payment_Method;

import javax.persistence.TypedQuery;

public class JpaCategoryRepository extends BaseJpaRepositoryBase<Category, Code,Code> implements CategoryRepository {

    public JpaCategoryRepository() {
        super("id");
    }

    public Category findCategoryById(int id) {
        final TypedQuery<Category> q = createQuery("SELECT e FROM Category e WHERE e.id = :id", Category.class);
        q.setParameter("id", id);
        return (Category) q.getResultList();
    }

    @Override
    public Category findCategoryById(Code id) {
        final TypedQuery<Category> q = createQuery("SELECT e FROM Category e WHERE  e.id = :idSearch",
                Category.class);
        q.setParameter("idSearch", id);
        return q.getSingleResult();
    }
}
