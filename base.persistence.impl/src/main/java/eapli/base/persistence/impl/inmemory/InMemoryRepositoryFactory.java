/*
 * Copyright (c) 2013-2021 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.generalmanagement.repositories.*;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.infrastructure.bootstrapers.BaseBootstrapper;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.questionnaire.repositories.QuestionRepository;
import eapli.base.questionnaire.repositories.QuestionnaireRepository;
import eapli.base.questionnaire.repositories.SectionRepository;
import eapli.base.salesmanagement.repositories.OrderItemRepository;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.salesmanagement.repositories.ShoppingCartRepository;
import eapli.base.warehousemanagement.repositories.*;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.InMemoryUserRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new BaseBootstrapper().execute();
    }

    @Override
    public UserRepository users(final TransactionalContext tx) {
        return new InMemoryUserRepository();
    }

    @Override
    public UserRepository users() {
        return users(null);
    }

    @Override
    public ClientUserRepository clientUsers(final TransactionalContext tx) {

        return new InMemoryClientUserRepository();
    }

    @Override
    public WarehousePlantRepository warehousePlantRepository(){
        return new InMemoryWarehousePlantRepository();
    }
    @Override
    public ClientUserRepository clientUsers() {
        return clientUsers(null);
    }

    @Override
    public WarehouseRepository warehouses() {
        return new InMemoryWarehouseRepository();
    }

    @Override
    public ProductRepository products() {
        return new InMemoryProductRepository();
    }

    @Override
    public CategoryRepository categories() {
        return new InMemoryCategoryRepository();
    }

    @Override
    public AGVDocksRepository agvDocks() {
        return new InMemoryAGVDocksRepository();
    }

    @Override
    public AGVRepository agv() {
        return new InMemoryAGVRepository();
    }

    @Override
    public AislesRepository aisles() {
        return new InMemoryAisleRepository();
    }

    @Override
    public RowAislesRepository rowAisles() {
        return new InMemoryRowAisleRepository();
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return signupRequests(null);
    }

    @Override
    public OrderItemRepository orderItem() {
        return new InMemoryOrderItemRepository();
    }

    @Override
    public CostumerRepository costumer() {
        return new InMemoryCostumerRepository();
    }
    @Override
    public ShoppingCartRepository shoppingCart() {
        return new InMemoryShoopingCartRepository();
    }


    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext tx) {
        return new InMemorySignupRequestRepository();
    }

    @Override
    public TransactionalContext newTransactionalContext() {
        // in memory does not support transactions...
        return null;
    }

    @Override
    public Product_OrderRepository orders() {
        return new InMemoryProductOrderRepository();
    }

    @Override
    public TaskRepository tasks() {
        return new InMemoryTaskRepository();
    }

    @Override
    public QuestionnaireRepository questionnaireRepository() {
        return new InMemoryQuestionnaireRepository();
    }

    @Override
    public SectionRepository sectionRepository() {
        return new InMemorySectionRepository();
    }

    @Override
    public QuestionRepository questionRepository() {
        return new InMemoryQuestionRepository();
    }

    @Override
    public PositionAGVRepository positionAGV() {
        return null;
    }

    @Override
    public RulesRepository rulesRepository() {
        return null;
    }
}
