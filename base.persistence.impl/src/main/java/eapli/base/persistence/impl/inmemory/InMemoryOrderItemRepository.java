package eapli.base.persistence.impl.inmemory;

import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.salesmanagement.domain.OrderItem;
import eapli.base.salesmanagement.repositories.OrderItemRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryOrderItemRepository extends InMemoryDomainRepository<OrderItem, Integer> implements OrderItemRepository {

    static {
        InMemoryInitializer.init();
    }
}