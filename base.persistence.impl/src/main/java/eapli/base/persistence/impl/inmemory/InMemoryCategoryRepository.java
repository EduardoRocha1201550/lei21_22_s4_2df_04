package eapli.base.persistence.impl.inmemory;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.product.Internal_code;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryCategoryRepository extends InMemoryDomainRepository<Category, Code> implements CategoryRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Category findCategoryById(Code id) {
        return (Category) match(e -> e.getCode().equals(id));
    }
}
