package eapli.base.persistence.impl.jpa;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.warehousemanagement.domain.PositionAGV;
import eapli.base.warehousemanagement.repositories.PositionAGVRepository;

public class JpaPositionAGVRepository  extends BaseJpaRepositoryBase<PositionAGV, Integer,Integer> implements PositionAGVRepository {


    JpaPositionAGVRepository() {
        super("id");
    }
}
