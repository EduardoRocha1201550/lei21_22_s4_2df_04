package eapli.base.persistence.impl.jpa;

import eapli.base.questionnaire.domain.Question;
import eapli.base.questionnaire.repositories.QuestionRepository;

public class JpaQuestionRepository extends BaseJpaRepositoryBase<Question, String, String> implements QuestionRepository {

    public JpaQuestionRepository(String persistenceUnitName) {
        super(persistenceUnitName, "code");
    }
}
