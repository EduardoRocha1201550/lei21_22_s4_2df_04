package eapli.base.persistence.impl.jpa;

import eapli.base.costumer.domain.Costumer;
import eapli.base.salesmanagement.domain.ShoppingCart;
import eapli.base.salesmanagement.repositories.ShoppingCartRepository;
import javax.persistence.TypedQuery;

public class JpaShoppingCartRepository extends BaseJpaRepositoryBase<ShoppingCart, Integer, Integer> implements ShoppingCartRepository  {



    public JpaShoppingCartRepository() {
        super("id");
    }


    @Override
    public ShoppingCart findShoppingCart(Costumer costumer) {
        final TypedQuery<ShoppingCart> q=super.entityManager().createQuery("Select e FROM ShoppingCart e WHERE e.costumer=:costumer",ShoppingCart.class);
        q.setParameter("costumer",costumer );
        return q.getSingleResult();
    }


}
