package eapli.base.persistence.impl.jpa;

import eapli.base.generalmanagement.domain.Category;

import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.product.*;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class JpaProductRepository extends BaseJpaRepositoryBase<Product, Internal_code,Internal_code> implements ProductRepository  {

    public JpaProductRepository() {
        super("internal_code");}


    @Override


    public Iterable<Product> findAll() {
        final TypedQuery<Product> q=createQuery("SELECT e FROM Product e WHERE e.active = true ",Product.class);

        return q.getResultList();
    }
    @Override
    public Iterable<Product> findByBrand(final Brand brand) {
        final TypedQuery<Product> q=createQuery("SELECT e FROM Product e WHERE e.brand = :brand ",Product.class);
        q.setParameter("brand",brand);
        return q.getResultList();
    }


    @Override
    public Iterable<Product> findByExtended_Description(Extended_description extended_description){
        final TypedQuery<Product> q=createQuery("SELECT e FROM Product e WHERE e.extended_description = :extended_description",Product.class);
        q.setParameter("extended_description",extended_description);
        return q.getResultList();
    }

    @Override
    public Iterable<Product> findByShort_Description(Short_description short_description){
        final TypedQuery<Product> q=createQuery("SELECT e FROM Product e WHERE e.short_description = :short_description",Product.class);
        q.setParameter("short_description",short_description);
        return q.getResultList();
    }


    @Override
    public Iterable<Product> findByCategory(Category category){
        final TypedQuery<Product> q=createQuery("SELECT e FROM Product e WHERE e.category = :category",Product.class);
        q.setParameter("category",category);
        return q.getResultList();
    }
    @Override
    public Product findbyID(Internal_code id) {
        final TypedQuery<Product> q=createQuery("SELECT e FROM Product e WHERE e.internal_code =:id ",Product.class);
        q.setParameter("id",id);
        return q.getSingleResult();
    }

    @Override
    public Product findProductByCode(Internal_code internal_code) {
        try {
            Query query = super.entityManager().createQuery
                    ("SELECT p FROM Product p WHERE p.internal_code=:internal_code");
            query.setParameter("internal_code", internal_code);

            Product product = (Product) query.getSingleResult();
            return product;
        } catch (NoResultException e) {
            return null;
        }
    }
}
