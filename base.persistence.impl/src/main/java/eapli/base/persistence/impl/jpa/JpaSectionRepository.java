package eapli.base.persistence.impl.jpa;

import eapli.base.questionnaire.domain.Section;
import eapli.base.questionnaire.repositories.SectionRepository;

public class JpaSectionRepository extends BaseJpaRepositoryBase<Section, String, String> implements SectionRepository {

    public JpaSectionRepository(String persistenceUnitName) {
        super(persistenceUnitName, "code");
    }
}
