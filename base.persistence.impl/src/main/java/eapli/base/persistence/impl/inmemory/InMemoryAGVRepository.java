package eapli.base.persistence.impl.inmemory;


import eapli.base.generalmanagement.DTO.AGVDTO;
import eapli.base.generalmanagement.domain.AGV;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;


import java.sql.SQLException;
import java.util.List;


public class InMemoryAGVRepository extends InMemoryDomainRepository<AGV, String> implements AGVRepository {
    static {
        InMemoryInitializer.init();
    }
   /* @Override
    public AGV findByiid(String id) {
        return  matchOne(e-> e.getId().equals(id));
    }*/

    @Override
    public AGV findByiid(String id) {
        return null;
    }

    @Override
    public List<AGV> findAvailable(double volume,double weight) {
        return (List<AGV>) match(e->e.getStatus().equals(AGV.Status.FREE));
    }

    @Override
    public List<AGV> findAvailable1() {
        return null;
    }

    @Override
    public List<AGVDTO> findAll1() throws SQLException {
        return null;
    }


}
