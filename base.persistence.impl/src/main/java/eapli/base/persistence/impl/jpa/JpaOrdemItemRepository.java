package eapli.base.persistence.impl.jpa;


import eapli.base.salesmanagement.domain.OrderItem;
import eapli.base.salesmanagement.repositories.OrderItemRepository;


public class JpaOrdemItemRepository extends BaseJpaRepositoryBase<OrderItem,Integer, Integer> implements OrderItemRepository {

    public JpaOrdemItemRepository(){
        super("id");

    }
}
