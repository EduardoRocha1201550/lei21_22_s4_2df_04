package eapli.base.persistence.impl.jpa;
import eapli.base.warehousemanagement.domain.Rows;
import eapli.base.warehousemanagement.repositories.RowAislesRepository;

public class JPARowAisleRepository extends BaseJpaRepositoryBase<Rows,Integer,Integer> implements RowAislesRepository {

    JPARowAisleRepository() {
        super("Id");
    }
}
