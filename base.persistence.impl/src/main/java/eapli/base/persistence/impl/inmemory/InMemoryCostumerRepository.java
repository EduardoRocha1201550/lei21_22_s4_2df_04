package eapli.base.persistence.impl.inmemory;

import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.domain.Email;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryCostumerRepository extends InMemoryDomainRepository<Costumer, Integer> implements CostumerRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Costumer findbyID(int id) {
        return (Costumer) match(e->e.getId()==id);
    }

    @Override
    public Costumer findByEmail(Email email) {
        return (Costumer) match(e->e.getEmail()==email);
    }
}
