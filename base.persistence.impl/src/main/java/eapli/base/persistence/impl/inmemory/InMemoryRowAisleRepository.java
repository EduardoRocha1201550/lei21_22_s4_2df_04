package eapli.base.persistence.impl.inmemory;



import eapli.base.warehousemanagement.domain.Rows;
import eapli.base.warehousemanagement.repositories.RowAislesRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryRowAisleRepository extends InMemoryDomainRepository<Rows, Integer> implements RowAislesRepository {
    static {
        InMemoryInitializer.init();
    }
}
