package eapli.base.persistence.impl.jpa;

import eapli.base.generalmanagement.domain.Task;
import eapli.base.generalmanagement.repositories.TaskRepository;
import eapli.base.product.Product;
import eapli.base.product.Short_description;
import eapli.base.salesmanagement.domain.Product_Order;

import javax.persistence.TypedQuery;


public class JpaTaskRepository extends BaseJpaRepositoryBase<Task,Integer,Integer> implements TaskRepository {
    JpaTaskRepository() {
        super("task_ID");
    }

   public Task findByOrder(Long id){
        final TypedQuery<Task> q=createQuery("SELECT e FROM Task e WHERE e.order_id = :order_id",Task.class);
        q.setParameter("order_id",id);
        return q.getSingleResult();
    }


}
