package eapli.base.persistence.impl.jpa;

import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.generalmanagement.repositories.RulesRepository;
import eapli.base.product.Product;

import javax.persistence.TypedQuery;
import java.util.Optional;

public class JpaRuleRepository extends BaseJpaRepositoryBase<Rules,Long,Long> implements RulesRepository {

    public JpaRuleRepository(String persistenceUnitName) {
        super("id");}




    @Override
    public Optional<Rules> ofIdentity(Long id) {
        return Optional.empty();
    }

    @Override
    public void delete(Rules entity) {

    }

    @Override
    public void deleteOfIdentity(Long entityId) {

    }

@Override
    public Iterable<Rules> findByContent(final Content content) {
        final TypedQuery<Rules> q=createQuery("SELECT e FROM Rules e WHERE e.content = :content ",Rules.class);
        q.setParameter("content",content);
        return q.getResultList();
    }
@Override
    public Iterable<Rules> findAll() {
        final TypedQuery<Rules> q=createQuery("SELECT e FROM Rules e WHERE e.active = true ",Rules.class);

        return q.getResultList();
    }

}
