package eapli.base.persistence.impl.inmemory;

import eapli.base.costumer.domain.Costumer;
import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.product.*;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

import java.util.Optional;

public class InMemoryProductRepository extends InMemoryDomainRepository<Product, Internal_code> implements ProductRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public <S extends Product> S save(S entity) {
        return null;
    }

    @Override
    public Iterable<Product> findByBrand(Brand brand) {
        return null;
    }

    @Override
    public Iterable<Product> findByExtended_Description(Extended_description extended_description) {
        return null;
    }

    @Override
    public Iterable<Product> findAll() {
        return null;
    }

    @Override
    public Iterable<Product> findByShort_Description(Short_description short_description) {
        return null;
    }

    @Override
    public Iterable<Product> findByCategory(Category category) {
        return null;
    }

    @Override
    public Product findbyID(Internal_code code) {
        return null;
    }

    @Override
    public Product findProductByCode(Internal_code internal_code) {
        return null;
    }

    @Override
    public Optional<Product> ofIdentity(Internal_code id) {
        return Optional.empty();
    }

    @Override
    public void delete(Product entity) {

    }

    @Override
    public void deleteOfIdentity(Internal_code entityId) {

    }

    @Override
    public long count() {
        return 0;
    }
}
