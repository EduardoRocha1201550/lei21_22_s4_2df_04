package eapli.base.persistence.impl.inmemory;


import eapli.base.warehousemanagement.repositories.AGVDocksRepository;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryAGVDocksRepository extends InMemoryDomainRepository<AGVDocks, String> implements AGVDocksRepository {
    static {
        InMemoryInitializer.init();
    }
    @Override
    public Iterable<AGVDocks> findAllAGVnull() {
        return match(e-> e.getId() == null);
    }

}
