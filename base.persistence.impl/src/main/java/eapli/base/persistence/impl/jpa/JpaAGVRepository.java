package eapli.base.persistence.impl.jpa;

import eapli.base.generalmanagement.DTO.AGVDTO;
import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.MaxVolume;
import eapli.base.generalmanagement.domain.MaxWeight;
import eapli.base.generalmanagement.domain.StatusAGV;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JpaAGVRepository extends BaseJpaRepositoryBase<AGV, String,String> implements AGVRepository {
    Connection conn;

    {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:h2:tcp://vsgate-s2.dei.isep.ipp.pt:10734/OrdersServer;MV_STORE=FALSE;AUTO_SERVER=true","sa", "eapli");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    JpaAGVRepository() {
        super("id");
    }

    @Override
    public AGV findByiid(String id) {
        final TypedQuery<AGV> q = entityManager().createQuery("SELECT e FROM AGV e WHERE  e.id= :idd",
                AGV.class);
        q.setParameter("idd",id);
        return q.getSingleResult();
    }

    @Override
    public List<AGV> findAvailable(double volume,double weight) {
        MaxVolume volume1=new MaxVolume(volume);
        MaxWeight weight1=new MaxWeight(weight);
        final TypedQuery<AGV> q = entityManager().createQuery("SELECT e FROM AGV e WHERE  e.status= :status and e.maxVolume>=:volume and e.maxWeight>=:weight",
                AGV.class);
        q.setParameter("status",AGV.Status.FREE);
        q.setParameter("volume",volume1);
        q.setParameter("weight",weight1);
        return q.getResultList();
    }
    @Override
    public List<AGV> findAvailable1() {
        final TypedQuery<AGV> q = entityManager().createQuery("SELECT e FROM AGV e WHERE  e.status= :status",
                AGV.class);
        q.setParameter("status",AGV.Status.FREE);
        return q.getResultList();
    }

    @Override
    public List<AGVDTO> findAll1() throws SQLException {
        Statement stmt;
        ResultSet rset = null;
        try {
            stmt = conn.createStatement();
            String query="select * from AGV";
            rset = stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<AGVDTO> list = new ArrayList<>();
        while(rset.next()){
            String id=rset.getString(1);
            Long idposition=rset.getLong(9);
            stmt = conn.createStatement();
            ResultSet rset1 = null;
            String query="select * from POSITIONAGV where id="+idposition;
            rset1 = stmt.executeQuery(query);
            rset1.next();
            list.add(new AGVDTO(id,rset1.getInt(2),rset1.getInt(3)));

        }

        return list;
    }
    public Iterable<AGV> findAll(){
        try {
            Query query = super.entityManager().createQuery("select e From AGV e");
//            query.setHint();
//            query.setHint();
//            query.setHint();
//
            Iterable<AGV> agvs = (Iterable<AGV>) query.getResultList();
            for (AGV agv:agvs){
                entityManager().detach(agv);
            }
            return agvs;

        }catch (NoResultException e){
            return null;
        }
    }



}