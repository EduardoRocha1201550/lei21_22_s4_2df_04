package eapli.base.persistence.impl.inmemory;


import eapli.base.warehousemanagement.repositories.AislesRepository;
import eapli.base.warehousemanagement.domain.Aisles;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryAisleRepository extends InMemoryDomainRepository<Aisles, Integer> implements AislesRepository {
    static {
        InMemoryInitializer.init();
    }
}
