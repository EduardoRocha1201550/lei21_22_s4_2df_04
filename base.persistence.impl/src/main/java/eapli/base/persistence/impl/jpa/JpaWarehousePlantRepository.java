package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.warehousemanagement.application.WarehousePlant;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.AGVAll.IDagvDocks;
import eapli.base.warehousemanagement.repositories.WarehousePlantRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

public class JpaWarehousePlantRepository extends JpaAutoTxRepository<WarehousePlant, String, String> implements WarehousePlantRepository {

    public JpaWarehousePlantRepository(final TransactionalContext autoTx) {
        super(autoTx, "eapli.base");
    }

    public JpaWarehousePlantRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "eapli.base");
    }

    @Override
    public AGVDocks findByAGVDock(IDagvDocks id) {
        try {
            Query query = super.entityManager().createQuery("SELECT p FROM AGVDocks p WHERE p.id=:id");
            query.setParameter("id", id);

            AGVDocks agvDocks = (AGVDocks) query.getSingleResult();
            return agvDocks;
        } catch (NoResultException e) {
            return null;
        }
    }
}

