package eapli.base.persistence.impl.inmemory;

import eapli.base.costumer.domain.Costumer;
import eapli.base.salesmanagement.domain.ShoppingCart;
import eapli.base.salesmanagement.repositories.ShoppingCartRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryShoppingCartRepository extends InMemoryDomainRepository<ShoppingCart, Integer> implements ShoppingCartRepository {
    static {
        InMemoryInitializer.init();
    }

    @Override
    public ShoppingCart findShoppingCart(Costumer costumer) {
        return (ShoppingCart) match(e -> e.getCostumer().equals(costumer));
    }


}
