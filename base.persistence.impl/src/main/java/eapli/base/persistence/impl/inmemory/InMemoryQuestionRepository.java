package eapli.base.persistence.impl.inmemory;

import eapli.base.questionnaire.domain.Question;
import eapli.base.questionnaire.repositories.QuestionRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryQuestionRepository extends InMemoryDomainRepository<Question, String> implements QuestionRepository {

    static {
        InMemoryInitializer.init();
    }
}
