package eapli.base.persistence.impl.jpa;

import eapli.base.questionnaire.domain.Questionnaire;
import eapli.base.questionnaire.repositories.QuestionnaireRepository;

public class JpaQuestionnaireRepository extends BaseJpaRepositoryBase<Questionnaire, String, String> implements QuestionnaireRepository {

    public JpaQuestionnaireRepository(String persistenceUnitName) {
        super(persistenceUnitName, "id");
    }
}
