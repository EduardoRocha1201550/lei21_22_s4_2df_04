package eapli.base.persistence.impl.jpa;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.repositories.AGVDocksRepository;

import javax.persistence.TypedQuery;

public class JpaAGVDocksRepository extends BaseJpaRepositoryBase<AGVDocks, String,String> implements AGVDocksRepository {
    JpaAGVDocksRepository() {
        super("agv_id");
    }

    @Override
    public Iterable<AGVDocks> findAllAGVnull() {
        AGV agv=null;
        final TypedQuery<AGVDocks> q = entityManager().createQuery("SELECT e FROM AGVDocks e WHERE  e.id is null",
                AGVDocks.class);

        return q.getResultList();
    }


}
