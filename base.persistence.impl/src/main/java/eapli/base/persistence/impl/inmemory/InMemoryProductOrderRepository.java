package eapli.base.persistence.impl.inmemory;

import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.Shipping_Method;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

import java.util.List;

public class InMemoryProductOrderRepository extends InMemoryDomainRepository<Product_Order,Long> implements Product_OrderRepository {
    static {
        InMemoryInitializer.init();
    }

    @Override
    public List<Product_Order> findAvailable() {
        return (List<Product_Order>) match(e->e.getState().equals(State.STATETODO));
    }

    @Override
    public List<Product_Order> findAvailableDoing() {
        return (List<Product_Order>) match(e->e.getState().equals(State.STATEDOING));
    }

    @Override
    public List<Product_Order> findAvailableDone() {
        return (List<Product_Order>) match(e->e.getState().equals(State.STATEDONE));
    }

    @Override
    public Product_Order findOrderById(Long id) {
        return null;
    }

    @Override
    public List<Product_Order> getOrdersFromCostumer(int id) {
        return null;
    }


}
