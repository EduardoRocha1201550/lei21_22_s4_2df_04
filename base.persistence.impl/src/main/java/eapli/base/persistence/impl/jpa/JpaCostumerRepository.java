package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.domain.Email;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;

public class JpaCostumerRepository extends JpaAutoTxRepository<Costumer, Integer, Integer> implements CostumerRepository {


    public JpaCostumerRepository(String puname){
        super(puname, Application.settings().getExtendedPersistenceProperties(),"eapli.base");

    }

    @Override
    public Costumer findbyID(int id) {

        final TypedQuery<Costumer> q=createQuery("Select e FROM Costumer e WHERE e.id=:id",Costumer.class);
        q.setParameter("id",id );
        return q.getSingleResult();
    }

    @Override
    public Costumer findByEmail(Email email) {
        final TypedQuery<Costumer> q=createQuery("Select e FROM Costumer e WHERE e.email=:email",Costumer.class);
        q.setParameter("email",email);
        return q.getSingleResult();
    }

}
