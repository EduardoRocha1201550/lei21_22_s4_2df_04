package eapli.base.persistence.impl.inmemory;

import eapli.base.questionnaire.domain.Section;
import eapli.base.questionnaire.repositories.SectionRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemorySectionRepository extends InMemoryDomainRepository<Section, String> implements SectionRepository {

    static {
        InMemoryInitializer.init();
    }
}
