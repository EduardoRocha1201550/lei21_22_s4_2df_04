package eapli.base.persistence.impl.inmemory;

import eapli.base.warehousemanagement.application.WarehousePlant;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.AGVAll.IDagvDocks;
import eapli.base.warehousemanagement.repositories.WarehousePlantRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

public class InMemoryWarehousePlantRepository extends InMemoryDomainRepository<WarehousePlant,String> implements WarehousePlantRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public AGVDocks findByAGVDock(final IDagvDocks id) {
        return (AGVDocks) match(e -> e.getAGVDocks().equals(id));
    }
}
