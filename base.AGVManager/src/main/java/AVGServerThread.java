import eapli.base.generalmanagement.domain.AGV;
import eapli.base.salesmanagement.domain.Product_Order;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class AVGServerThread implements Runnable {
    public static final int ASSIGNTASK =3;

    public static final int CHANGEAGVSTATUS=4;
    public static final int ALLAVAILABLEAGV=5;
    public static final int ALLAVAILABLEORDERS=6;
    public static final int FIFO=7;
    public static final int AGVLIST=8;

    private Socket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private CodingAndDecoding2 cod= new CodingAndDecoding2();
    private final AGVManagerController controller= new AGVManagerController();

    public AVGServerThread(Socket cli_s) {
        s=cli_s;}
    public void run() {

        InetAddress clientIP;
        clientIP = s.getInetAddress();
        System.out.println("Nova ligaçao de cliente com este ip: " + clientIP.getHostAddress() +
                ", Numero de porta: " + s.getPort());

        try {
            sIn = new DataInputStream(s.getInputStream());
            DataOutputStream sOut = new DataOutputStream(this.s.getOutputStream());


                MessageDTO message= cod.Decoding(sIn);

                if(message.getCode()== ASSIGNTASK){
                    int orderId=message.getData_message()[0];
                    //chama o controler da force task
                }
                if(message.getCode()==CHANGEAGVSTATUS){

                    
                    String id= String.valueOf(message.getData_message()[0]);
                    boolean verif=controller.changeStatus(id);

                    DataOutputStream out =new DataOutputStream(s.getOutputStream());
                    byte[] ResponseMessage = new byte[5];
                    ResponseMessage[0]=1;
                    ResponseMessage[1]=5;
                    ResponseMessage[2]=1;
                    ResponseMessage[3]=0;
                    if (verif){
                        ResponseMessage[4]=1;
                    }else ResponseMessage[4]=0;
                    out.write(ResponseMessage);
                }
                if (message.getCode() == ALLAVAILABLEAGV){
                    List<AGV> agvList = controller.getAvailableAGV();
                    for (AGV agv : agvList){
                        System.out.println(agv.getId()+ " STATE: " + agv.getStatus());
                    }
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream oOut= new ObjectOutputStream(bos);
                    oOut.writeObject(agvList);
                    oOut.flush();
                    byte[] data = bos.toByteArray();

                    DataOutputStream out =new DataOutputStream(s.getOutputStream());
                    byte[] ResponseMessage = new byte[data.length+4];
                    ResponseMessage[0]=1;
                    ResponseMessage[1]=5;
                    int size=data.length;
                    int size1=size%256;
                    int size2=size/256;
                    ResponseMessage[2]= (byte) size1;
                    ResponseMessage[3]= (byte) size2;
                    for(int i=0;i<data.length;i++){
                        ResponseMessage[i+4]=data[i];
                    }
                    out.write(ResponseMessage);
                    oOut.close();
                }
                if (message.getCode() == FIFO){
                    boolean verif=controller.FifoAGV();

                    DataOutputStream out =new DataOutputStream(s.getOutputStream());
                    byte[] ResponseMessage = new byte[5];
                    ResponseMessage[0]=1;
                    ResponseMessage[1]=7;
                    ResponseMessage[2]=1;
                    ResponseMessage[3]=0;
                    if (verif){
                        ResponseMessage[4]=1;
                    }else ResponseMessage[4]=0;
                    out.write(ResponseMessage);
                }
                if (message.getCode() == ALLAVAILABLEORDERS){
                    List<Product_Order>product_orderList=controller.findavailableOrder();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream oOut= new ObjectOutputStream(bos);
                    oOut.writeObject(product_orderList);
                    oOut.flush();
                    byte[] data = bos.toByteArray();

                    DataOutputStream out =new DataOutputStream(s.getOutputStream());
                    byte[] ResponseMessage = new byte[data.length+4];
                    ResponseMessage[0]=1;
                    ResponseMessage[1]=6;
                    int size=data.length;
                    int size1=size%256;
                    int size2=size/256;
                    ResponseMessage[2]= (byte) size1;
                    ResponseMessage[3]= (byte) size2;
                    for(int i=0;i<data.length;i++){
                        ResponseMessage[i+4]=data[i];
                    }
                    out.write(ResponseMessage);
                    oOut.close();
                }
                if (message.getCode() == AGVLIST){
                    List<AGV>agvList=controller.getAGVList();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream oOut= new ObjectOutputStream(bos);
                    oOut.writeObject(agvList);
                    oOut.flush();

                    byte[] data = bos.toByteArray();
                    DataOutputStream out =new DataOutputStream(s.getOutputStream());
                    byte[] ResponseMessage = new byte[data.length+4];

                    ResponseMessage[0]=1;
                    ResponseMessage[1]=8;
                    int size=data.length;
                    int size1=size%256;
                    int size2=size/256;
                    ResponseMessage[2]= (byte) size1;
                    ResponseMessage[3]= (byte) size2;
                    for(int i=0;i<data.length;i++){
                        ResponseMessage[i+4]=data[i];
                    }
                    out.write(ResponseMessage);
                    oOut.close();
                }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {

            try {

                System.out.println("Client " + clientIP.getHostAddress() +
                        ", port number: " + s.getPort() + " disconnected");


                s.close();
            } catch (IOException ex) {
                System.out.println("Socket nao foi fechada\n");
            }
            System.out.println("Socket fechada\n");
        }

    }
}

