import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class AGVManagerAcceptance {

    private DataOutputStream sOut;
    private DataInputStream sIn;

    static CodingAndDecoding2 cod = new CodingAndDecoding2();
    static InetAddress serverIP;
    static Socket sock;

    public static void main(String[] args) {
        Product_OrderRepository product_order = PersistenceContext.repositories().orders();

        Scanner sc = new Scanner(System.in);
        if (args.length != 1) {
            System.out.println("Server IPv4/IPv6 address ou DNS é requerido");
            System.exit(1);
        }
        try {
            serverIP = InetAddress.getByName(args[0]);
        } catch (
                UnknownHostException ex) {
            System.out.println("Server invalido: " + args[0]);
            System.exit(1);
        }
        try {
            System.out.println("Enter the option that you want to select 1-Give an order to the AGV");
            int option = sc.nextByte();
            if (option == 1) {


                sock.close();
            }
        } catch (IOException ex) {
            System.out.println("Erro ao establecer a ligação TCP");
            System.exit(1);


        }
    }
}