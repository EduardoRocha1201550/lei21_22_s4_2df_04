import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.StatusAGV;
import eapli.base.generalmanagement.domain.Task;

import eapli.base.generalmanagement.repositories.TaskRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.warehousemanagement.application.GetAgvStatusController;
import eapli.base.warehousemanagement.repositories.AGVRepository;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AGVManagerController{
    private GetAgvStatusController agvController=new GetAgvStatusController();
    private Product_OrderRepository orderRepository= PersistenceContext.repositories().orders();
    private AGVRepository agvRepository= PersistenceContext.repositories().agv();
    private TaskRepository repository= PersistenceContext.repositories().tasks();

    private static CodingAndDecoding2 cod= new CodingAndDecoding2();
    public static final String STATEOCCUPIED="occupied";
    public static final String STATEDOING="Doing";


    static final int SERVER_PORT=10837;
    static final String KEYSTORE_PASS="forgotten";
    static InetAddress serverIP;
    static SSLSocket sock;;

    public List<AGV> getAvailableAGV(){
        return agvController.getAvailableAGV();
    }

    public boolean changeStatus(String id){
        if (agvController.changeAGVStatuss(id)==null){
            return false;
        }
        return true;
    }
    public boolean FifoAGV() throws InterruptedException, IOException {
        List<Product_Order> ordersList= findavailableOrder();
        Queue q = new Queue();
        for (Product_Order product_order : ordersList){
            q.enqueue(product_order);
        }

        while(!q.isEmpty()){
            Product_Order order=q.peek();

            List<AGV> agvList=findavailableAGV(order.getWeight().getWeight(),order.getVolume().getTotal());

            if (agvList.size()>0){
                AGV agv = agvList.get(0);

                Task task=createTaskandSave(order.getId().toString(),order,agv);
                boolean verif=doTask(task);
                Thread.sleep(5010);
                if (verif){
                    q.dequeue();
                }
            }
        }
        return true;
    }
    public List<AGV> getAGVList(){
        List<AGV> agvList= (List<AGV>) agvRepository.findAll();
        return agvList;
    }

    public boolean doTask(Task task) throws InterruptedException, IOException {
        // Trust these certificates provided by servers
        System.setProperty("javax.net.ssl.trustStore", "/var/www/Server/client1_J.jks");
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key for client certificate when requested by the server
        System.setProperty("javax.net.ssl.keyStore","/var/www/Server/client1_J.jks");
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();

        try { serverIP = InetAddress.getByName("vsgate-s1.dei.isep.ipp.pt"); }
        catch(UnknownHostException ex) {
            System.out.println("Invalid server specified: 127.0.0.1");
            System.exit(1); }


        try {
            sock = (SSLSocket) sf.createSocket(serverIP,SERVER_PORT);
        }
        catch(IOException ex) {
            System.out.println("Failed to connect to the server" + SERVER_PORT);
            System.out.println("Application aborted.");
            System.exit(1);
        }

        System.out.println("Connected to the server" + SERVER_PORT);


        try {
            sock.startHandshake();
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataOutputStream out =new DataOutputStream(sock.getOutputStream());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oOut= new ObjectOutputStream(bos);
        oOut.writeObject(task);
        oOut.flush();
        byte[] data = bos.toByteArray();
        byte[] message = new byte[data.length+4];

        message[0]=1;
        message[1]=6;
        int size=data.length;
        int size1=size%256;
        int size2=size/256;
        message[2]= (byte) size1;
        message[3]= (byte) size2;
        for(int i=0;i<data.length;i++){
            message[i+4]=data[i];
        }
        out.write(message);

        Thread.sleep(1500);

        DataInputStream inputStream=new DataInputStream(sock.getInputStream());
        MessageDTO messageDTO =cod.Decoding(inputStream);
        System.out.println("Version: " + messageDTO.getVersion());
        System.out.println("Code: " + messageDTO.getCode());
        System.out.println("Size: " + messageDTO.getSize());
        if (messageDTO.getData_message()[0]==1){
            System.out.println("Success");
            return true;
        }else{
            System.out.println("Not able to perform task");
        }
        return false;

    }

    public void changeStatusAGV( AGV agv){

        agv.changeStatusTo(AGV.Status.OCCUPIED_DOING_TASK);
        agvRepository.save(agv);

    }
    public Optional<AGV> getAGVbyID(String id){
        return Optional.ofNullable(agvRepository.findByiid(id));
    }
    public void changeStatusOrder(Product_Order order) {
        order.setState(new State(STATEDOING));
        orderRepository.save(order);
    }

    public List<Product_Order> findavailableOrder(){
        List<Product_Order>orders=orderRepository.findAvailable();
        Collections.sort(orders, new Comparator<Product_Order>() {
            public int compare(Product_Order o1, Product_Order o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });Collections.reverse(orders);
        return orders;
    }
    public List<AGV> findavailableAGV(double weight, double volume){
        List<AGV>agvList=agvRepository.findAvailable(weight,volume);
        Collections.sort(agvList, Comparator.comparingDouble(o -> o.getMaxVolume().getMaxVolume()));
        Collections.reverse(agvList);
        return agvList;
    }
    public Task createTaskandSave(String description,Product_Order order,AGV agv){
        Task task=new Task(description,order,agv);
        return repository.save(task);
    }
}

    class Node
    {
        Product_Order data;       // integer data
        Node next;      // pointer to the next node

        public Node(Product_Order data)
        {
            // set data in the allocated node and return it
            this.data = data;
            this.next = null;
        }
    }

    class Queue {
        private static Node rear = null, front = null;
        private static int count = 0;

        // Utility function to dequeue the front element
        public static Product_Order dequeue()     // delete at the beginning
        {
            if (front == null) {
                System.out.println("\nQueue Underflow");
                System.exit(-1);
            }

            Node temp = front;
            System.out.printf("Removing " + temp.data.getId());

            // advance front to the next node
            front = front.next;

            // if the list becomes empty
            if (front == null) {
                rear = null;
            }

            // decrease the node's count by 1
            count -= 1;

            // return the removed item
            return temp.data;
        }

        // Utility function to add an item to the queue
        public static void enqueue(Product_Order item)     // insertion at the end
        {
            // allocate a new node in a heap
            Node node = new Node(item);
            System.out.printf("Inserting " + item.getId());

            // special case: queue was empty
            if (front == null) {
                // initialize both front and rear
                front = node;
                rear = node;
            } else {
                // update rear
                rear.next = node;
                rear = node;
            }

            // increase the node's count by 1
            count += 1;
        }

        // Utility function to return the top element in a queue
        public static Product_Order peek() {
            // check for an empty queue
            if (front == null) {
                System.exit(-1);
            }

            return front.data;
        }

        // Utility function to check if the queue is empty or not
        public static boolean isEmpty() {
            return rear == null && front == null;
        }

        // Function to return the size of the queue
        private static int size() {
            return count;
        }
    }


