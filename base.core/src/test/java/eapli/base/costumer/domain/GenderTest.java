package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenderTest {

    @Test
    public void testToString() {
        Gender gender=new Gender("Masculino");
        String expected="gender='Masculino'";
        Assert.assertEquals(expected,gender.toString());

    }
}