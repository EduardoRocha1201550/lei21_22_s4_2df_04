package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;


public class EmailTest {
    Email email=new Email();

    @Test (expected = IllegalArgumentException.class)
    public void testIsValidEmail() {
        email.isValidEmail("eeeeee");
    }

    @Test
    public void testToString() {
        Email email=new Email("edurocha@isep.pt");
        String exp="email='edurocha@isep.pt'";
        Assert.assertEquals(exp,email.toString());
    }
}