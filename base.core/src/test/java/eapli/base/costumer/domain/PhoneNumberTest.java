package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PhoneNumberTest {
    PhoneNumber phoneNumber=new PhoneNumber();
    @Test (expected = IllegalStateException.class)
    public void isValidMobileNo() { // Invalid phone number
        phoneNumber.isValidMobileNo("99999");
    }

    @Test
    public void testToString() {
        PhoneNumber phoneNumber=new PhoneNumber("+351910807792");
        String expected="phoneNumber='+351910807792'";
        Assert.assertEquals(expected,phoneNumber.toString());

    }
}