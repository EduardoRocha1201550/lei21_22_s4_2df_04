package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.*;

public class BirthDateTest {
    BirthDate birthDate=new BirthDate();

    public BirthDateTest() throws ParseException {
    }

    @Test (expected = IllegalArgumentException.class)
    public void isValid() {
        birthDate.isValid("12/11/22/22");
    }

//    @Test
//    public void testToString() throws ParseException {
//        BirthDate birthDate=new BirthDate("12-10-2000");
//        String expected="birthDate=Thu Oct 12 00:00:00 WEST 2000";
//        Assert.assertEquals(expected,birthDate.toString());
//    }
}