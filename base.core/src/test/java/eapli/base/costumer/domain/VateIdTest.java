package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class VateIdTest {

    @Test
    public void isValidVateId() {
        VateId vateId=new VateId("PT999999999");
        String expected="vateId='PT999999999'";
        Assert.assertEquals(expected,vateId.toString());
    }
}