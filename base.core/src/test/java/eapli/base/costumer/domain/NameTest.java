package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class NameTest {
    Name name= new Name();

    @Test
    public void testToString() {
        Name name= new Name("Eduardo Rocha");
        String expected="name='Eduardo Rocha'";
        Assert.assertEquals(expected,name.toString());
    }
}