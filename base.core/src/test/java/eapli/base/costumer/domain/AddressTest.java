package eapli.base.costumer.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddressTest {

    @Test
    public void testToString() {
        String expected="address='Rua Vale de Cambra'";
        Address address= new Address("Rua Vale de Cambra");
        Assert.assertEquals(expected,address.toString());
    }
}