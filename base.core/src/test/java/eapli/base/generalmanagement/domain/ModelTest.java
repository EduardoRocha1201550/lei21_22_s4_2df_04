package eapli.base.generalmanagement.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class ModelTest {
    Model model= new Model();
    @Test (expected = IllegalArgumentException.class)
    public void checkModel() { // Null
        model.checkModel("");
    }

    @Test (expected = IllegalArgumentException.class)
    public void checkModel2() { // Max length
        model.checkModel("dsrerrrrrrrrrrrrerreeeerrrrrrrrrrrrrrrrreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeer");
    }
}