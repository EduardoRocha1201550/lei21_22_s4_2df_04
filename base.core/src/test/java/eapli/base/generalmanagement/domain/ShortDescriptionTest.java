package eapli.base.generalmanagement.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class ShortDescriptionTest {
    ShortDescription shortDescription=new ShortDescription();
    @Test (expected = IllegalArgumentException.class)
    public void checkShortDescription() {
        shortDescription.checkShortDescription("");
    }
    @Test (expected = IllegalArgumentException.class)
    public void checkShortDescription2() {
        shortDescription.checkShortDescription("This model is a old one.This model is a old one.This model is a old one.This model is a old one");
    }
}