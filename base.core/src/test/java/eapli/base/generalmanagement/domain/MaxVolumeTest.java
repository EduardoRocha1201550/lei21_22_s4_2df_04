package eapli.base.generalmanagement.domain;

import org.junit.Test;



public class MaxVolumeTest {
    MaxVolume max= new MaxVolume();
    @Test (expected = IllegalArgumentException.class)
    public void validateMaxVolume() {
        max.validateMaxVolume(-1);
    }
}