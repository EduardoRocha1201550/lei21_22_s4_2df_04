package eapli.base.generalmanagement.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class MaxWeightTest {
    MaxWeight maxWeight=new MaxWeight();
    @Test (expected = IllegalArgumentException.class)
    public void validateMaxWeight() {
        maxWeight.validateMaxWeight(-1);
    }
}