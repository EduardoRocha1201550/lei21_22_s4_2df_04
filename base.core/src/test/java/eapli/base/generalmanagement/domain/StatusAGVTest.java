package eapli.base.generalmanagement.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class StatusAGVTest {
    StatusAGV statusAGV=new StatusAGV();
    @Test (expected = IllegalArgumentException.class)
    public void testToString() {
        statusAGV.checkStatus("eeee");
    }
}