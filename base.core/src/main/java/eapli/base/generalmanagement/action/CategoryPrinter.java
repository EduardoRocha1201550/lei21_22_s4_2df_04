package eapli.base.generalmanagement.action;

import eapli.base.generalmanagement.domain.Category;
import eapli.framework.visitor.Visitor;

public class CategoryPrinter implements Visitor<Category> {

    @Override
    public void visit(final Category visitee){
        System.out.printf("%-10d%-30s", visitee.getCode(), visitee.getDescription());
    }
}
