package eapli.base.generalmanagement.action;

import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.framework.visitor.Visitor;

public class AGVDockPrinter implements Visitor<AGVDocks> {


    @Override
    public void visit(final AGVDocks visitee) {
        System.out.println(visitee.toString());
    }
}

