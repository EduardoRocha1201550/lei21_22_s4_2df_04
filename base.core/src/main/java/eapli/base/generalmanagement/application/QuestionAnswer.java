package eapli.base.generalmanagement.application;

public class QuestionAnswer {
    private int type;
    private Long QuestionID;
    private String question;
    private Long SectionID;
    private String answer;

    public int getType() {
        return type;
    }

    public Long getQuestionID() {
        return QuestionID;
    }

    public String getQuestion() {
        return question;
    }

    public Long getSectionID() {
        return SectionID;
    }

    public String getAnswer() {
        return answer;
    }

    public QuestionAnswer(int type, Long questionID, String question, Long sectionID, String answer) {
        this.type = type;
        QuestionID = questionID;
        SectionID = sectionID;
        this.answer = answer;
        this.question=question;
    }

}
