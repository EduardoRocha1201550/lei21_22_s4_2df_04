package eapli.base.generalmanagement.application;

import eapli.base.questionnaire.domain.Question;
import eapli.base.questionnaire.domain.Questionnaire;

import java.io.*;
import java.util.*;

public class StatisticalReportController {
    public int numberOfResponses(Questionnaire questionnaire){
        File file=new File(questionnaire.getId());
        File[]files=file.listFiles();
        return  files.length;
    }
    public List<QuestionAnswer> getStats(Questionnaire questionnaire) throws Exception {
        File file = new File(questionnaire.getId());
        File[] files = file.listFiles();
        if(files.length==0){
            throw new Exception("There are no responses for the selected Questionnaire");
        }
        List<QuestionAnswer>questionAnswersList=new ArrayList<>();
        for (int i = 0; i < files.length; i++) {

            BufferedReader br = new BufferedReader(new FileReader(files[i]));
            Long section=0l;
            while (br.readLine() != null) {
                String temp=br.readLine();
                if(temp.contains("Section ID:")){
                    section=Long.parseLong(temp.substring(11));
                    while(br.readLine()!=null){
                        String temp1= br.readLine();
                        if (temp1.contains("Question ID:")) {
                            Long id=Long.parseLong(temp1.substring(12));
                            String question=br.readLine();
                            int type=Integer.parseInt(br.readLine().substring(5));
                            br.readLine();
                            String answer=br.readLine();
                            questionAnswersList.add(new QuestionAnswer(type,id,question,section,answer));
                        }
                    }
                }
            }
            }
        return questionAnswersList;
    }

    public HashMap<Long,List<QuestionAnswer>> getQuestionsOrdered(List<QuestionAnswer>questionAnswerList){
        HashMap<Long,List<QuestionAnswer>>map=new HashMap<>();
        Collections.sort(questionAnswerList, new Comparator<QuestionAnswer>() {
            @Override
            public int compare(QuestionAnswer o1, QuestionAnswer o2) {
                return o1.getQuestionID().compareTo(o2.getQuestionID());
            }
        });
        List<QuestionAnswer>temp=questionAnswerList;
        for(int i=0;i< temp.size();i++){
            List<QuestionAnswer>questionAnswers=new ArrayList<>();
            questionAnswers.add(temp.get(i));
            for(int j=i+1;j< temp.size();j++){
                if(temp.get(i).getQuestionID().equals(temp.get(j).getQuestionID())){
                    questionAnswers.add(temp.get(j));
                    temp.remove(j);
                }
            }
            map.put(temp.get(i).getQuestionID(),questionAnswers);
        }
        return map;
    }
    public String setReport(HashMap<Long,List<QuestionAnswer>>map,Questionnaire questionnaire){
        String message="";
        message=message+"Number of Responses Obtained:"+numberOfResponses(questionnaire)+"\n";
        for(Map.Entry<Long,List<QuestionAnswer>> set:map.entrySet()){
            Long questionID=set.getKey();
            List<QuestionAnswer>list=set.getValue();
            List<String>results=calculations(list);

            message=message+"\nQuestion ID:"+questionID+"\n" +
                    "Type:"+list.get(0).getType()+"\n";
            for(int i=0;i<results.size();i++){
                message=message+results.get(i)+"\n";
            }
            message=message+"\n";


        }
        return message;
    }
    public List<String> calculations(List<QuestionAnswer>questionAnswersList){
        List<String>results=new ArrayList<>();
        HashMap<String,Integer>map=new HashMap<>();
        if(questionAnswersList.get(0).getType()==1){
            for(int i=0;i< questionAnswersList.size();i++){
                if(questionAnswersList.get(i).getAnswer().isEmpty()&&!map.containsKey("")){
                    map.put(questionAnswersList.get(i).getAnswer(),1);
                } else if(questionAnswersList.get(i).getAnswer().isEmpty()){
                    map.put(questionAnswersList.get(i).getAnswer(),map.get(questionAnswersList.get(i).getAnswer())+1);
                }
            }
            if(map.isEmpty()){
                results.add("Percentage responded:100%");
            }else{
                for (Map.Entry<String, Integer> set : map.entrySet()) {
                    double percentage = (double) set.getValue() / questionAnswersList.size();
                    percentage = percentage * 100;
                    percentage = 100 - percentage;
                    results.add("Percentage responded:" + percentage + "%");
                }
            }
        }else if(questionAnswersList.get(0).getType()==4){
            HashMap<Integer,List<String>>map1=new HashMap<>();
            int size=questionAnswersList.get(0).getAnswer().split("-").length;
            String[][]values=new String[questionAnswersList.size()][size];
            for(int i=0;i<questionAnswersList.size();i++){
                String result=questionAnswersList.get(i).getAnswer();
                String[]places=result.split("-");
            for(int j=0;j<size;j++){
                values[i][j]=places[j];
            }
            }
            for(int k=0;k< values[0].length;k++){
                List<String>strings=new ArrayList<>();
                for(int l=0;l< values.length;l++){
                    strings.add(values[l][k]);
                }
                map1.put(k,strings);
            }
            Map<String,Integer>map2=new HashMap<>();
            int k=1;
            for(Map.Entry<Integer,List<String>> set:map1.entrySet()){
                List<String> stringList=set.getValue();
                for(int i=0;i<stringList.size();i++){
                   if(map2.containsKey(stringList.get(i))){
                       map2.put(stringList.get(i),map2.get(stringList.get(i))+1);
                   }else{
                       map2.put(stringList.get(i),1);
                   }
                }

                for(Map.Entry<String,Integer> set1:map2.entrySet()){
                    double percentage=(double)set1.getValue()/ questionAnswersList.size();
                    percentage=percentage*100;
                    results.add(k+"º"+set1.getKey()+":"+percentage+"% ");

                }
                map2.clear();
                k++;
            }

        }else{
        for(int i=0;i< questionAnswersList.size();i++){
            if(!map.containsKey(questionAnswersList.get(i).getAnswer())){
                map.put(questionAnswersList.get(i).getAnswer(),1);
            }else{
                map.put(questionAnswersList.get(i).getAnswer(),map.get(questionAnswersList.get(i).getAnswer())+1);
            }

        }
        for(Map.Entry<String,Integer> set:map.entrySet()){
           double percentage=(double)set.getValue()/ questionAnswersList.size();
           percentage=percentage*100;
           results.add("Answer "+set.getKey()+":"+percentage+"%");
        }
        }
        return results;
    }

}


