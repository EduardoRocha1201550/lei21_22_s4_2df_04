package eapli.base.generalmanagement.application;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.Brand;
import eapli.base.product.Extended_description;
import eapli.base.product.Product;
import eapli.base.product.Short_description;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;

public class CatalogController   {

private final ProductRepository productRepository= PersistenceContext.repositories().products();


private final AuthorizationService authz = AuthzRegistry.authorizationService();
private CategoryRepository categoryRepository= PersistenceContext.repositories().categories(); {
};
   public Iterable<Product> allProducts(){
       authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
       return productRepository.findAll();
   }

   public Iterable<Product> findByBrand(Brand brand){
       authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
       return productRepository.findByBrand(brand);
   }

   public Iterable<Product> findByShortDescription(Short_description short_description){
       authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
       return productRepository.findByShort_Description(short_description);
   }

   public Iterable<Product> findByDescripition(Extended_description extended_description){
       authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
       return productRepository.findByExtended_Description(extended_description);
   }

   public Iterable<Product> findByCategory(Category category){
       authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
       return productRepository.findByCategory(category);
   }

    public Category findCategoryById(Code id) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
        return categoryRepository.findCategoryById(id);
    }
}



