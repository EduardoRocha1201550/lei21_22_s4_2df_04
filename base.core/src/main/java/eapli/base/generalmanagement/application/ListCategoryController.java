package eapli.base.generalmanagement.application;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

@UseCaseController
public class ListCategoryController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final CategoryRepository repository = PersistenceContext.repositories().categories();

    public Iterable<Category> all() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.SALESCLERK);
        return this.repository.findAll();
    }
}
