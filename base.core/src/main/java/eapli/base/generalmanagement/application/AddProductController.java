package eapli.base.generalmanagement.application;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.Product;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

public class AddProductController {
    private final ProductRepository repository= PersistenceContext.repositories().products();
    private final AuthorizationService authorizationService= AuthzRegistry.authorizationService();
    public void registerProduct(Product product){
        authorizationService.ensureAuthenticatedUserHasAnyOf(BaseRoles.SALESCLERK);
        repository.save(product);
    }
    public Iterable<Product> all() {
        return this.repository.findAll();
    }
}

