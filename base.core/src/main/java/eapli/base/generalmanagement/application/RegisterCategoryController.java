package eapli.base.generalmanagement.application;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.generalmanagement.domain.Description;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

public class RegisterCategoryController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final CategoryRepository repository = PersistenceContext.repositories().categories();

    public Category registerCategory(final Code novoCode, final Description novoDescription) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.SALESCLERK);

        final Category cat = new Category(novoCode,novoDescription);
        return this.repository.save(cat);
    }
}
