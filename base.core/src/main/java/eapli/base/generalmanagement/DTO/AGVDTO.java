package eapli.base.generalmanagement.DTO;

public class AGVDTO {
    private String id;
    private int positionx;
    private int positiony;

    public String getId() {
        return id;
    }

    public int getPositionx() {
        return positionx;
    }

    public int getPositiony() {
        return positiony;
    }

    public AGVDTO(String id, int positionx, int positiony) {
        this.id = id;
        this.positionx = positionx;
        this.positiony = positiony;
    }


}
