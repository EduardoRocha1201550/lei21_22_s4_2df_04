package eapli.base.generalmanagement.repositories;


import eapli.base.generalmanagement.domain.Task;
import eapli.base.product.Internal_code;
import eapli.framework.domain.repositories.DomainRepository;

public interface TaskRepository extends DomainRepository<Integer, Task> {


    public Task findByOrder(Long id);
}
