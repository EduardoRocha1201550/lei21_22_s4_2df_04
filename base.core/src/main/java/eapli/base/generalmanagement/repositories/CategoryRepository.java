package eapli.base.generalmanagement.repositories;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.product.Internal_code;
import eapli.framework.domain.repositories.DomainRepository;

public interface CategoryRepository extends DomainRepository<Code, Category> {
    Category findCategoryById(Code id);
}
