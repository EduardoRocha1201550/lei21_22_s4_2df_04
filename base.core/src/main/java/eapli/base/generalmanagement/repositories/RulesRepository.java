package eapli.base.generalmanagement.repositories;

import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.product.Brand;
import eapli.base.product.Product;
import eapli.framework.domain.repositories.DomainRepository;

public interface RulesRepository extends DomainRepository<Long, Rules> {




    Iterable<Rules> findByContent(Content content);
}
