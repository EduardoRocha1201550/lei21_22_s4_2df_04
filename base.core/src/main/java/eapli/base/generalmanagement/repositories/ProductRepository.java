package eapli.base.generalmanagement.repositories;

import eapli.base.generalmanagement.domain.Category;
import eapli.base.product.*;
import eapli.framework.domain.repositories.DomainRepository;

public interface ProductRepository extends DomainRepository<Internal_code, Product>{

    public Iterable<Product> findByBrand(final Brand brand);
    public Iterable<Product> findByExtended_Description(Extended_description extended_description);
    public Iterable<Product> findAll();

    public Iterable<Product> findByShort_Description(Short_description short_description);

    public Iterable<Product> findByCategory(Category category);
    public Product findbyID(Internal_code code);

    Product findProductByCode(Internal_code internal_code);
}