package eapli.base.generalmanagement.domain;

import eapli.base.salesmanagement.domain.Product_Order;
import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Task implements AggregateRoot<Integer> {
    @Id
    @GeneratedValue
    private int task_ID;
    private String description;
    @OneToOne
    private Product_Order order;
    @OneToOne
    private AGV agv;

    public Task(String description,Product_Order order,AGV agv) {
        this.description = description;
        this.order=order;
        this.agv=agv;
    }

    public Product_Order getOrder() {
        return order;
    }

    public void setOrder(Product_Order order) {
        this.order = order;
    }

    public AGV getAgv() {
        return agv;
    }

    public void setAgv(AGV agv) {
        this.agv = agv;
    }

    public Task() {

    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Integer identity() {
        return task_ID;
    }
}
