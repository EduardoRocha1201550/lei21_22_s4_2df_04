package eapli.base.generalmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Category implements AggregateRoot<Code> {
    @EmbeddedId
    private Code code;
    @Embedded
    private Description description;


    public Category(final Code code, final Description description) {
        this.code = code;
        this.description = description;
    }

    public Category() {

    }

    public Description description() {
        return this.description;
    }

    public void changeDescriptionTo(final Description newDescription) {
        this.description = newDescription;
    }

    public Code getCode() {
        return code;
    }

    public Description getDescription() {
        return description;
    }

    @Override
    public boolean sameAs(final Object o) {
        if (!this.equals(o)) {
            return false;
        }
        final Category other = (Category) o;
        return code==(other.code) && description.equals(other.description);
    }

    @Override
    public String toString() {
        return "Code:" + code +
                ", Description:" + description
        +"  ";
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public Code identity() {
        return code;
    }
}

