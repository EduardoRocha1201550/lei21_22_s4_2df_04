package eapli.base.generalmanagement.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employee {

    private Long id;
    private String name;
    private String address;
    private int phone_number;
    private String email;
    public Employee(Long id, String name, String address, int phone_number, String email) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone_number = phone_number;
        this.email = email;
    }

    public Employee() {

    }


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
