package eapli.base.generalmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Description implements ValueObject {
    private String description;

    public Description(String description) {
        this.description = description;
    }

    public Description() {

    }

    @Override
    public String toString() {
        return this.description;
    }

    private void validateDescription(String description) {

        if (description.length()<20 && description.length()>50){
            throw new IllegalArgumentException("Description must have between 20 characters and 50");
        }
    }
}
