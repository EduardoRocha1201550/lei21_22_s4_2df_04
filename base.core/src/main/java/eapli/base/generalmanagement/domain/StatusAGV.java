package eapli.base.generalmanagement.domain;


import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Status.
 */
@Embeddable
public class StatusAGV implements ValueObject {
    public static final String STATEFREE="free";
    public static final String STATECHARGING="charging";
    public static final String STATEOCCUPIED="occupied";

    private String status;

    /**
     * Instantiates a new Status.
     */
    public StatusAGV() {
    }

    /**
     * Instantiates a new Status.
     *
     * @param status agv status
     */
    public StatusAGV(String status) {
        checkStatus(status);
        this.status = status;

    }

    /**
     * @return agv status
     */
    @Override
    public String toString() {
        return "status: " + this.status + ';';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void checkStatus(String status) {
        if (!(status.equals(STATEFREE)|status.equals(STATECHARGING)|status.equals(STATEOCCUPIED))){
            throw new IllegalArgumentException("Status must be:free,charging or occupied");
        }
    }
}