package eapli.base.generalmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

/**
 * The type Max volume.
 */
@Embeddable
public class MaxVolume implements ValueObject {

    private double maxVolume;

    /**
     * Instantiates a new Max volume.
     */
    protected MaxVolume() {
    }

    public void setMaxVolume(double maxVolume) {
        this.maxVolume = maxVolume;
    }

    public double getMaxVolume() {
        return maxVolume;
    }

    /**
     * Instantiates a new Max volume.
     *
     * @param maxVolume agv max volume
     */
    public MaxVolume(double maxVolume) {
        validateMaxVolume(maxVolume);
        this.maxVolume = maxVolume;
    }

    public void validateMaxVolume(double volume){
        if(volume<=0){
            throw new IllegalArgumentException("Volume1 cant be null");
        }
    }

    /**
     * @return agv max volume
     */
    @Override
    public String toString() {
        return "maxVolume= " + this.maxVolume + ';';
    }
}
