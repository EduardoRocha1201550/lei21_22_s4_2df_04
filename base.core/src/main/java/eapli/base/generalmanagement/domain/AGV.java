package eapli.base.generalmanagement.domain;

import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.PositionAGV;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.regex.Pattern;


@Entity
@Table(name="agv")
    public class AGV implements Comparable<String>, AggregateRoot<String> {
    @Id
    private String id;

    @Embedded
    private MaxVolume maxVolume;

    @Embedded
    private MaxWeight maxWeight;


    @Embedded
    private Model model;

    @Embedded
    private Route route;

    @Embedded
    private ShortDescription shortDescription;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Embedded
    private Autonomy autonomy;

    @OneToOne(cascade = CascadeType.ALL)
    private PositionAGV position;

    @OneToOne(cascade = CascadeType.ALL)
    private AGVDocks agvDocks;



    public Status getStatus() {
        return status;
    }


    public String getId() {
        return id;
    }


    /**
         * Instantiates a new Agv.
         */
        public AGV() {
        }


    public AGV(String id, MaxVolume maxVolume, MaxWeight maxWeight, Model model, Route route, ShortDescription shortDescription, Status status, Autonomy autonomy,AGVDocks agvDocks,PositionAGV positionAGV) {
        validateid(id);
        this.id = id;
        this.maxVolume = maxVolume;
        this.maxWeight = maxWeight;
        this.model = model;
        this.route = route;
        this.shortDescription = shortDescription;
        this.status = status;
        this.autonomy = autonomy;
        this.agvDocks= agvDocks;
        this.position=positionAGV;
        }

    public AGVDocks getAgvDocks() {
        return agvDocks;
    }


    @Override
    public String toString() {
        return "AGV{" +
                "id='" + id + '\'' +
                ", maxVolume=" + maxVolume +
                ", maxWeight=" + maxWeight +
                ", model=" + model +
                ", route=" + route +
                ", shortDescription=" + shortDescription +
                ", status=" + status +
                ", autonomy=" + autonomy +
                ", agvDocks=" + agvDocks +
                '}';
    }

    public void validateid(String id){
            if(id==null){
                throw new IllegalArgumentException("Invalid id");
            }
            Pattern p = Pattern.compile("^[a-zA-Z0-9]*$");
            if(!p.matcher(id).find()){
                throw new IllegalArgumentException("Id must be alphanumeric");
            }
            if(id.length()>8){
                throw new IllegalArgumentException("Id has a max length of 8");
            }
    }




    @Override
    public boolean sameAs(Object other) {
            return DomainEntities.areEqual(this, other);
        }


    public void changeStatusTo(Status status) {
        this.status = status;
    }


    public MaxVolume getMaxVolume() {
        return maxVolume;
    }

    public MaxWeight getMaxWeight() {
        return maxWeight;
    }

    @Override
    public String identity() {
        return id;
    }

    public enum Status {
        /**
         * Occupied charging status.
         */
        OCCUPIED_CHARGING,
        /**
         * Occupied doing task status.
         */
        OCCUPIED_DOING_TASK,
        /**
         * Free status.
         */
        FREE
    }


}

