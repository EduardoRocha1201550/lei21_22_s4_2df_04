package eapli.base.generalmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

/**
 * The type Max weight.
 */
@Embeddable
public class MaxWeight implements ValueObject {
    private double maxWeight;

    /**
     * Instantiates a new Max weight.
     */
    protected MaxWeight() {
    }

    /**
     * Instantiates a new Max weight.
     *
     * @param maxWeight agv max weight
     */
    public MaxWeight(double maxWeight) {
        validateMaxWeight(maxWeight);
        this.maxWeight = maxWeight;
    }

    public void validateMaxWeight(double weight){
        if(weight<=0){
            throw new IllegalArgumentException("Weight cant be null");
        }
    }

    /**
     * @return agv max weight
     */
    @Override
    public String toString() {
        return "maxWeight= " + this.maxWeight + ';';
    }
}
