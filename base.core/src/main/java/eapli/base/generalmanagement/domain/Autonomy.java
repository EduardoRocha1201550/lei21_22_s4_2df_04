package eapli.base.generalmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Autonomy implements ValueObject {

    private String autonomy;

    protected Autonomy (){}

    public Autonomy(String autonomy){
        this.autonomy = autonomy;
    }

    @Override
    public String toString() {
        return "autonomy: " + this.autonomy + ';';
    }


}
