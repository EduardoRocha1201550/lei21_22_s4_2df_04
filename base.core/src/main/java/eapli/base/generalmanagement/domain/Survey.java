package eapli.base.generalmanagement.domain;

import eapli.base.questionnaire.domain.Section;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.List;
@Entity
public class Survey {
    @GeneratedValue
    @Id
    private int survey_ID;
    private String title1;
    private String welcome_message;
    private String final_message;


    //private List<Section> sectionList;

    public Survey(int survey_ID, String title, String welcome_message, String final_message, List<Section> sectionList) {
        this.survey_ID = survey_ID;
        this.title1 = title;
        this.welcome_message = welcome_message;
        this.final_message = final_message;
        //this.sectionList = sectionList;
    }

    public Survey() {

    }
}
