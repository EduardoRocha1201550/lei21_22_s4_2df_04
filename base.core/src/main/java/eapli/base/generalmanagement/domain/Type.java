package eapli.base.generalmanagement.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Type  {
    private String instruction;
    private boolean obligatoriness;
    private String extrainfo;
    @Id
    private String type;
    public Type( String instruction, boolean obligatoriness, String extrainfo, String type) {
        this.instruction = instruction;
        this.obligatoriness = obligatoriness;
        this.extrainfo = extrainfo;
        this.type = type;
    }

    public Type(int i) {

    }
}
