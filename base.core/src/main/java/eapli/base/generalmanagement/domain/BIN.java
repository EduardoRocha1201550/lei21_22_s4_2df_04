package eapli.base.generalmanagement.domain;

import eapli.base.product.Product;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class BIN {
    @OneToOne
    private Product product;
    private double length;
    private double width;
    @Id
    private Long id;

    public BIN(Product product, double length, double width, Long id) {
        this.product = product;
        this.length = length;
        this.width = width;
        this.id = id;
    }

    public BIN() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
