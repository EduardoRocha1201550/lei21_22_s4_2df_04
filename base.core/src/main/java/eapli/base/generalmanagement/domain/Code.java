package eapli.base.generalmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Code implements ValueObject, Comparable<Code> {
    private String code;

    public Code(String code) {
        this.code = code;
    }

    public Code() {

    }

    @Override
    public String toString() {
        return this.code;
    }

    public void validateCode(String code){
        if (code.length()>10 && code.length()<1){
            throw new IllegalArgumentException("Code must have a minimum of 10 characters");
        }
    }

    @Override
    public int compareTo(Code o) {
        return this.code.compareTo(o.code);
    }
}
