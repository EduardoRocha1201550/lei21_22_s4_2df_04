package eapli.base.questionnaire.repositories;

import eapli.base.questionnaire.domain.Question;
import eapli.framework.domain.repositories.DomainRepository;

public interface QuestionRepository extends DomainRepository<String, Question> {
}
