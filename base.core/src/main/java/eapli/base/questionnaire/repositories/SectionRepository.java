package eapli.base.questionnaire.repositories;

import eapli.base.questionnaire.domain.Section;
import eapli.framework.domain.repositories.DomainRepository;

public interface SectionRepository extends DomainRepository<String, Section> {
}
