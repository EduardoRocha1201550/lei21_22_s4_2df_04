package eapli.base.questionnaire.repositories;

import eapli.base.questionnaire.domain.Questionnaire;
import eapli.framework.domain.repositories.DomainRepository;

public interface QuestionnaireRepository  extends DomainRepository<String, Questionnaire> {

}

