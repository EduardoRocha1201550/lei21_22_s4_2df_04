package eapli.base.questionnaire.application;

import eapli.base.questionnaire.domain.Question;
import eapli.base.questionnaire.domain.Questionnaire;
import eapli.base.questionnaire.domain.Section;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.questionnaire.repositories.QuestionnaireRepository;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class QuestionnaireController {

    private final QuestionnaireRepository repository = PersistenceContext.repositories().questionnaireRepository();

        public void createNewQuestionnaire(String id, String title, String welcome_message, String final_message,  List<Section> sections) {
            final Questionnaire newQuestionnaire = new Questionnaire(id,title,welcome_message,sections,final_message);
            this.repository.save(newQuestionnaire);
        }


        public void ExportQuestionnaireToFile(String id, String title, String welcome_message, String final_message, List<Section> sections) throws FileNotFoundException {
            PrintWriter out = new PrintWriter("Questionnaire" + id + ".txt");

            // Questionnaire------------------------------------------------------------------------------------------------
            out.println("Questionnare id=" + id);
            out.println("Questionnaire tittle=" + title);

            // Optional
            if(!welcome_message.equals(""))
                out.println("Questionnaire welcome message=" + welcome_message);

            out.println("Questionnaire final message=" + final_message);


            // Section------------------------------------------------------------------------------------------------------
            for(Section section: sections){

                out.println("Section id=" + section.getSection_id());
                out.println("Section title=" + section.getSection_title());

                // Optional
                if(!section.getSection_description().equals(""))
                    out.println("Section description=" + section.getSection_description());

                out.println("Obligatoriness=" + section.getObligatoriness());

                out.println("Section repeatability=" + section.getRepeatability());

                //Question--------------------------------------------------------------------------------------------------
                for (Question question: section.getContent()){

                    out.println("Question id=" + question.getQuestion_id());
                    out.println("Question=" + question.getQuestion());



                    out.println("Obligatoriness=" + question.getObligatoriness());


                }
            }

            out.close();
        }


}
