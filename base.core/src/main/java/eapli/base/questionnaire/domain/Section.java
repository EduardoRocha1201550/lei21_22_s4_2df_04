package eapli.base.questionnaire.domain;

import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;
import java.util.List;

@Entity
public class Section implements AggregateRoot<String> {
    @Id
    private long section_id;

    private String section_title;

    private String section_description;

    private String obligatoriness;

    private int repeatability;

    @ManyToMany(cascade= CascadeType.ALL)
    private List<Question> content;

    public Section(long section_id, String section_title, String section_description, String obligatoriness,  int repeatability, List<Question> content) {


        this.section_id = section_id;
        this.section_title = section_title;
        this.section_description = section_description;
        this.obligatoriness = obligatoriness;
        this.repeatability = repeatability;
        this.content = content;
    }
    public Section() {
    }

    public long getSection_id() {
        return section_id;
    }

    public void setSection_id(long section_id) {
        this.section_id = section_id;
    }

    public String getSection_title() {
        return section_title;
    }

    public void setSection_title(String section_title) {
        this.section_title = section_title;
    }

    public String getSection_description() {
        return section_description;
    }

    public void setSection_description(String section_description) {
        this.section_description = section_description;
    }

    public String getObligatoriness() {
        return obligatoriness;
    }

    public void setObligatoriness(String obligatoriness) {
        this.obligatoriness = obligatoriness;
    }


    public int getRepeatability() {
        return repeatability;
    }

    public void setRepeatability(int repeatability) {
        this.repeatability = repeatability;
    }

    public List<Question> getContent() {
        return content;
    }

    public void setContent(List<Question> content) {
        this.content = content;
    }



    @Override
    public String toString() {
        return "Section " + section_id + '\n' +
                ", section_title='" + section_title + '\'' +
                ", section_description='" + section_description + '\'' +
                ", obligatoriness=" + obligatoriness +
                ", repeatability='" + repeatability + '\'' +
                ", questions: " + "\n" + content;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public String identity() {
        return null;
    }
}
