package eapli.base.questionnaire.domain;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import java.util.List;

@Embeddable
public class Answer {
    @ElementCollection
    private List<String> answer;
    private int type1;

    public Answer(List<String> answer, int type) {
        this.answer = answer;
        this.type1 = type;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }

    public int getType() {
        return type1;
    }

    public void setType(int type) {
        this.type1 = type;
    }
    public boolean isAnswered(){
        if(answer.isEmpty()){
            return false;
        }
        return true;
    }

    public Answer() {

    }
}
