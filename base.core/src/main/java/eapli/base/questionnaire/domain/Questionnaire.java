package eapli.base.questionnaire.domain;

import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;
import java.util.List;

@Entity
public class Questionnaire implements AggregateRoot<String> {

    @Id
    private String id;

    private String title1;

    private String welcome_message;

    private String final_message;


    @OneToMany//transcient guardar tudo como string na base dados, dar load ao questionario e depois separar as sections e questions
    private List<Section> sections; //  a string seria um value object



    public Questionnaire(String id, String title, String welcome_message, List<Section> sections, String final_message) {
        this.id = id;
        this.title1 = title;
        this.welcome_message = welcome_message;
        this.sections = sections;
        this.final_message = final_message;

    }

    public Questionnaire() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title1;
    }

    public void setTitle(String title) {
        this.title1 = title;
    }

    public String getWelcome_message() {
        return welcome_message;
    }

    public void setWelcome_message(String welcome_message) {
        this.welcome_message = welcome_message;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public String getFinal_message() {
        return final_message;
    }

    public void setFinal_message(String final_message) {
        this.final_message = final_message;
    }



    @Override
    public String identity() {
        return String.valueOf(this.id);
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Questionnaire)) {
            return false;
        }

        final Questionnaire that = (Questionnaire) other;

        if (this == that) {
            return true;
        }

        return identity().equals(that.identity());
    }

    @Override
    public String toString() {
        return "Questionnaire:" + '\'' +
                "id='" + id + '\'' +
                ", title='" + title1 + '\'' +
                ", welcome_message='" + welcome_message + '\'' +
                ", final_message='" + final_message + '\'' +
                ", sections:" + sections;
    }
}
