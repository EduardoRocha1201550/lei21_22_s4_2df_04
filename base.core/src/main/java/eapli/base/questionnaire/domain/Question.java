package eapli.base.questionnaire.domain;


import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Question implements AggregateRoot<String> {

    @Id
    private long question_id;

    private String question;

    private String obligatoriness;

    private int type;

    @ElementCollection
    private List<String> options;

    public Question() {
    }

    public int getType() {
        return type;
    }

    public Question(long question_id, String question, String obligatoriness, int type, List<String> options) {
        this.question_id = question_id;
        this.question = question;
        this.obligatoriness = obligatoriness;
        this.type = type;
        this.options = options;

    }




    public long getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(long question_id) {
        this.question_id = question_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }




    public String getObligatoriness() {
        return obligatoriness;
    }

    public void setObligatoriness(String obligatoriness) {
        this.obligatoriness = obligatoriness;
    }



    public List<String> getOptions() {return options;}

    public void setOptions(List<String> options) {this.options = options;}

//    @Override
//    public String identity() {
//        return String.valueOf(this.question_id);
//    }
//
//    @Override
//    public boolean sameAs(Object other) {
//        if (!(other instanceof Question)) {
//            return false;
//        }
//
//        final Question that = (Question) other;
//
//        if (this == that) {
//            return true;
//        }
//
//        return identity().equals(that.identity());
//    }

    @Override
    public String toString() {

        String string1 = question_id + ". " + question + "\n";
        List <String> listOptions = new ArrayList<>();
        if(options.size()==0){

        }else {
            for (int i = 0; i < options.size(); i++) {
                String string2 = "   " + i + ". " + options.get(i) + "\n";
                listOptions.add(string2);
            }
        }

        return string1 + listOptions;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public String identity() {
        return null;
    }
}
