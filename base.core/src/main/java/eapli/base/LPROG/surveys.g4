grammar surveys;

projeto: param TK_NEWLINE* EOF;

section: paramSection TK_NEWLINE question+;

question: paramQuestion TK_NEWLINE answer+ TK_NEWLINE;

param: 'Code: ' TK_NUMBER+ TK_NEWLINE
       'Title: ' TK_WORD+ TK_NUMBER* TK_NEWLINE
       TK_WORD+ TK_NEWLINE TK_NEWLINE//welcome message
       section+ TK_NEWLINE
       'Final Message: 'finalMessage TK_NEWLINE;

paramSection: 'Section ID: ' TK_NUMBER+ TK_NEWLINE
              'Section Title: ' (TK_WORD*TK_NUMBER*TK_WORD*TK_NUMBER*) TK_NEWLINE
              'Description: 'TK_WORD+  TK_NEWLINE //description
              obligatoriness TK_NEWLINE
              'Section Repeat: ' TK_NUMBER TK_NEWLINE ; //repetição ???


paramQuestion: 'Question ID: ' TK_NUMBER+ TK_NEWLINE
               TK_WORD+ TK_NUMBER* TK_QUESTIONMARK+ TK_NEWLINE
               type
               obligatoriness TK_NEWLINE
               ;

answer: paramAnswer|TK_NEWLINE;

paramAnswer:TK_NUMBER+ TK_SPECIALCHARS* (TK_NUMBER*TK_WORD*TK_NUMBER*) TK_NEWLINE;

extraInfo: TK_WORD+ ;

type: TK_WORD?TK_NUMBER? TK_NEWLINE?;

finalMessage: TK_WORD+;

wordNumber: TK_WORD* TK_NUMBER*
           |TK_NUMBER* TK_WORD*
           | TK_WORD*
           |TK_NUMBER*
           ;

obligatoriness: TK_MANDATORY
              | TK_OPTIONAL
              | TK_DEPENDENT
              ;


TK_NUMBER : '0'..'9';
TK_WORD : [a-zA-Z]+TK_SPECIALCHARS*[a-zA-Z]*;
TK_NEWLINE:'\n';
TK_QUESTIONMARK: '?';
TK_SPECIALCHARS:(':'|';'|','|'.'|')'|'(');
TK_MANDATORY : '{'[Mm][Aa][Nn][Dd][Aa][Tt][Oo][Rr][Yy]'}';
TK_OPTIONAL : '{'[Oo][Pp][Tt][Ii][Oo][Nn][Aa][Ll]'}';
TK_DEPENDENT : '{'[Dd][Ee][Pp][Ee][Nn][Dd][Ee][Nn][Tt]'}';
TK_WS : [\t\r ]+ -> skip ;
