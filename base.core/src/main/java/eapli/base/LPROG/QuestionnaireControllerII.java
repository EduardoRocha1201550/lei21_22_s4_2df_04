package eapli.base.LPROG;


import antlr.ANTLRException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

public class QuestionnaireControllerII {
    public boolean questionnaireValidation (String path) throws IOException, ParseCancellationException {
        surveysLexer lexer = new surveysLexer(CharStreams.fromFileName(path));
        lexer.removeErrorListeners();
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        surveysParser parser = new surveysParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new DescriptiveErrorListener().INSTANCE);
        ParseTree tree = parser.projeto(); // parse
        SurveyVisitor survey = new SurveyVisitor();
        survey.visit(tree);
        return true;
    }
}
