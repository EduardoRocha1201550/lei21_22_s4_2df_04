package eapli.base.LPROG;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;



    @Embeddable
    public class TypeRule implements ValueObject {
        private double type;

        public TypeRule(double type) {

                this.type = type;

        }

        public double getType() {
            return type;
        }



        public void setType(double type) {
            this.type = type;
        }

        public TypeRule(){

        }

        @Override
        public String toString() {
            return ""+type;
        }


    }


