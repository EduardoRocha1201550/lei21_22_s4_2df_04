package eapli.base.LPROG;


import eapli.base.costumer.domain.Costumer;
import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;

@Entity
public class Rules implements  AggregateRoot<Long> {

    @Id

    private int id;

    @Embedded
    private TypeRule type;
    @Embedded
    private Content content;


    private boolean active;

    public Rules(int id, TypeRule type, Content content, boolean active) {
        this.id=id;
        this.type=type;
        this.content=content;
        this.active=active;
    }

    public Rules() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TypeRule getType() {
        return type;
    }

    public void setType(TypeRule type) {
        this.type = type;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }




    @Override
    public String toString() {
        return ""+content;
    }


    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return null;
    }
}
