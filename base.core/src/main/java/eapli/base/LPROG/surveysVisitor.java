// Generated from C:/Users/berna/OneDrive/Documents/lei21_22_s4_2df_04/base.app.other.console/src/main/java/LPROGGRAMMAR\surveys.g4 by ANTLR 4.10.1
package eapli.base.LPROG;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link surveysParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface surveysVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link surveysParser#projeto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProjeto(surveysParser.ProjetoContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#section}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSection(surveysParser.SectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#question}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuestion(surveysParser.QuestionContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(surveysParser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#paramSection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamSection(surveysParser.ParamSectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#paramQuestion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamQuestion(surveysParser.ParamQuestionContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#answer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnswer(surveysParser.AnswerContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#paramAnswer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamAnswer(surveysParser.ParamAnswerContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#extraInfo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtraInfo(surveysParser.ExtraInfoContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#instruction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruction(surveysParser.InstructionContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#finalMessage}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFinalMessage(surveysParser.FinalMessageContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#wordNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWordNumber(surveysParser.WordNumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link surveysParser#obligatoriness}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObligatoriness(surveysParser.ObligatorinessContext ctx);
}