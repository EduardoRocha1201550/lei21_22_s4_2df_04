package eapli.base.LPROG;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

public class Survey {
        public static void main(String[] args) throws IOException {
            surveysLexer lexer = new surveysLexer(CharStreams.fromFileName("teste2.txt"));
            lexer.removeErrorListeners();
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            surveysParser parser = new surveysParser(tokens);
            ParseTree tree = parser.projeto(); // parse
            SurveyVisitor survey = new SurveyVisitor();
            survey.visit(tree);
        }
}
