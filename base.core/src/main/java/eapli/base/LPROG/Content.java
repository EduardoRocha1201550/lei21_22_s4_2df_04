package eapli.base.LPROG;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;


    @Embeddable
    public class Content implements ValueObject {
        private String content;

        public Content(String content) {

            this.content = content;

        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Content(){

        }

        @Override
        public String toString() {
            return ""+content;
        }


    }


