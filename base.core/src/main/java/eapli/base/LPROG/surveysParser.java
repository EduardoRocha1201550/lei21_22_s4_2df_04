// Generated from C:/Users/berna/OneDrive/Documents/lei21_22_s4_2df_04/base.app.other.console/src/main/java/LPROGGRAMMAR\surveys.g4 by ANTLR 4.10.1
package eapli.base.LPROG;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class surveysParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, TK_NUMBER=9, 
		TK_WORD=10, TK_NEWLINE=11, TK_QUESTIONMARK=12, TK_SPECIALCHARS=13, TK_MANDATORY=14, 
		TK_OPTIONAL=15, TK_DEPENDENT=16, TK_WS=17;
	public static final int
		RULE_projeto = 0, RULE_section = 1, RULE_question = 2, RULE_param = 3, 
		RULE_paramSection = 4, RULE_paramQuestion = 5, RULE_answer = 6, RULE_paramAnswer = 7, 
		RULE_extraInfo = 8, RULE_instruction = 9, RULE_finalMessage = 10, RULE_wordNumber = 11, 
		RULE_obligatoriness = 12;
	private static String[] makeRuleNames() {
		return new String[] {
			"projeto", "section", "question", "param", "paramSection", "paramQuestion", 
			"answer", "paramAnswer", "extraInfo", "instruction", "finalMessage", 
			"wordNumber", "obligatoriness"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'Code: '", "'Title: '", "'Final Message: '", "'Section ID: '", 
			"'Section Title: '", "'Description: '", "'Section Repeat: '", "'Question ID: '", 
			null, null, "'\\n'", "'?'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "TK_NUMBER", "TK_WORD", 
			"TK_NEWLINE", "TK_QUESTIONMARK", "TK_SPECIALCHARS", "TK_MANDATORY", "TK_OPTIONAL", 
			"TK_DEPENDENT", "TK_WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "surveys.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public surveysParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProjetoContext extends ParserRuleContext {
		public ParamContext param() {
			return getRuleContext(ParamContext.class,0);
		}
		public TerminalNode EOF() { return getToken(surveysParser.EOF, 0); }
		public List<TerminalNode> TK_NEWLINE() { return getTokens(surveysParser.TK_NEWLINE); }
		public TerminalNode TK_NEWLINE(int i) {
			return getToken(surveysParser.TK_NEWLINE, i);
		}
		public ProjetoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_projeto; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterProjeto(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitProjeto(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitProjeto(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProjetoContext projeto() throws RecognitionException {
		ProjetoContext _localctx = new ProjetoContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_projeto);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			param();
			setState(30);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_NEWLINE) {
				{
				{
				setState(27);
				match(TK_NEWLINE);
				}
				}
				setState(32);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(33);
			match(Recognizer.EOF);
			}
		}
		catch (RecognitionException re) {

			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SectionContext extends ParserRuleContext {
		public ParamSectionContext paramSection() {
			return getRuleContext(ParamSectionContext.class,0);
		}
		public TerminalNode TK_NEWLINE() { return getToken(surveysParser.TK_NEWLINE, 0); }
		public List<QuestionContext> question() {
			return getRuleContexts(QuestionContext.class);
		}
		public QuestionContext question(int i) {
			return getRuleContext(QuestionContext.class,i);
		}
		public SectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_section; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterSection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitSection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitSection(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SectionContext section() throws RecognitionException {
		SectionContext _localctx = new SectionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_section);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			paramSection();
			setState(36);
			match(TK_NEWLINE);
			setState(38); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(37);
				question();
				}
				}
				setState(40); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__7 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuestionContext extends ParserRuleContext {
		public ParamQuestionContext paramQuestion() {
			return getRuleContext(ParamQuestionContext.class,0);
		}
		public List<TerminalNode> TK_NEWLINE() { return getTokens(surveysParser.TK_NEWLINE); }
		public TerminalNode TK_NEWLINE(int i) {
			return getToken(surveysParser.TK_NEWLINE, i);
		}
		public List<AnswerContext> answer() {
			return getRuleContexts(AnswerContext.class);
		}
		public AnswerContext answer(int i) {
			return getRuleContext(AnswerContext.class,i);
		}
		public QuestionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_question; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterQuestion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitQuestion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitQuestion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QuestionContext question() throws RecognitionException {
		QuestionContext _localctx = new QuestionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_question);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42);
			paramQuestion();
			setState(43);
			match(TK_NEWLINE);
			setState(45); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(44);
				answer();
				}
				}
				setState(47); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_NUMBER );
			setState(49);
			match(TK_NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamContext extends ParserRuleContext {
		public List<TerminalNode> TK_NEWLINE() { return getTokens(surveysParser.TK_NEWLINE); }
		public TerminalNode TK_NEWLINE(int i) {
			return getToken(surveysParser.TK_NEWLINE, i);
		}
		public FinalMessageContext finalMessage() {
			return getRuleContext(FinalMessageContext.class,0);
		}
		public List<TerminalNode> TK_NUMBER() { return getTokens(surveysParser.TK_NUMBER); }
		public TerminalNode TK_NUMBER(int i) {
			return getToken(surveysParser.TK_NUMBER, i);
		}
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public List<SectionContext> section() {
			return getRuleContexts(SectionContext.class);
		}
		public SectionContext section(int i) {
			return getRuleContext(SectionContext.class,i);
		}
		public ParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitParam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitParam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamContext param() throws RecognitionException {
		ParamContext _localctx = new ParamContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_param);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			match(T__0);
			setState(53); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(52);
				match(TK_NUMBER);
				}
				}
				setState(55); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_NUMBER );
			setState(57);
			match(TK_NEWLINE);
			setState(58);
			match(T__1);
			setState(60); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(59);
				match(TK_WORD);
				}
				}
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_WORD );
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_NUMBER) {
				{
				{
				setState(64);
				match(TK_NUMBER);
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(70);
			match(TK_NEWLINE);
			setState(72); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(71);
				match(TK_WORD);
				}
				}
				setState(74); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_WORD );
			setState(76);
			match(TK_NEWLINE);
			setState(77);
			match(TK_NEWLINE);
			setState(79); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(78);
				section();
				}
				}
				setState(81); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__3 );
			setState(83);
			match(TK_NEWLINE);
			setState(84);
			match(T__2);
			setState(85);
			finalMessage();
			setState(86);
			match(TK_NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamSectionContext extends ParserRuleContext {
		public List<TerminalNode> TK_NEWLINE() { return getTokens(surveysParser.TK_NEWLINE); }
		public TerminalNode TK_NEWLINE(int i) {
			return getToken(surveysParser.TK_NEWLINE, i);
		}
		public ObligatorinessContext obligatoriness() {
			return getRuleContext(ObligatorinessContext.class,0);
		}
		public List<TerminalNode> TK_NUMBER() { return getTokens(surveysParser.TK_NUMBER); }
		public TerminalNode TK_NUMBER(int i) {
			return getToken(surveysParser.TK_NUMBER, i);
		}
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public ParamSectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramSection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterParamSection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitParamSection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitParamSection(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamSectionContext paramSection() throws RecognitionException {
		ParamSectionContext _localctx = new ParamSectionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_paramSection);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(88);
			match(T__3);
			setState(90); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(89);
				match(TK_NUMBER);
				}
				}
				setState(92); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_NUMBER );
			setState(94);
			match(TK_NEWLINE);
			setState(95);
			match(T__4);
			{
			setState(99);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(96);
					match(TK_WORD);
					}
					} 
				}
				setState(101);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			setState(105);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(102);
					match(TK_NUMBER);
					}
					} 
				}
				setState(107);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			setState(111);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_WORD) {
				{
				{
				setState(108);
				match(TK_WORD);
				}
				}
				setState(113);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(117);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_NUMBER) {
				{
				{
				setState(114);
				match(TK_NUMBER);
				}
				}
				setState(119);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			setState(120);
			match(TK_NEWLINE);
			setState(121);
			match(T__5);
			setState(123); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(122);
				match(TK_WORD);
				}
				}
				setState(125); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_WORD );
			setState(127);
			match(TK_NEWLINE);
			setState(128);
			obligatoriness();
			setState(129);
			match(TK_NEWLINE);
			setState(130);
			match(T__6);
			setState(131);
			match(TK_NUMBER);
			setState(132);
			match(TK_NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamQuestionContext extends ParserRuleContext {
		public List<TerminalNode> TK_NEWLINE() { return getTokens(surveysParser.TK_NEWLINE); }
		public TerminalNode TK_NEWLINE(int i) {
			return getToken(surveysParser.TK_NEWLINE, i);
		}
		public InstructionContext instruction() {
			return getRuleContext(InstructionContext.class,0);
		}
		public ObligatorinessContext obligatoriness() {
			return getRuleContext(ObligatorinessContext.class,0);
		}
		public List<TerminalNode> TK_NUMBER() { return getTokens(surveysParser.TK_NUMBER); }
		public TerminalNode TK_NUMBER(int i) {
			return getToken(surveysParser.TK_NUMBER, i);
		}
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public List<TerminalNode> TK_QUESTIONMARK() { return getTokens(surveysParser.TK_QUESTIONMARK); }
		public TerminalNode TK_QUESTIONMARK(int i) {
			return getToken(surveysParser.TK_QUESTIONMARK, i);
		}
		public ParamQuestionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramQuestion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterParamQuestion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitParamQuestion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitParamQuestion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamQuestionContext paramQuestion() throws RecognitionException {
		ParamQuestionContext _localctx = new ParamQuestionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_paramQuestion);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134);
			match(T__7);
			setState(136); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(135);
				match(TK_NUMBER);
				}
				}
				setState(138); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_NUMBER );
			setState(140);
			match(TK_NEWLINE);
			setState(142); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(141);
				match(TK_WORD);
				}
				}
				setState(144); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_WORD );
			setState(149);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_NUMBER) {
				{
				{
				setState(146);
				match(TK_NUMBER);
				}
				}
				setState(151);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(153); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(152);
				match(TK_QUESTIONMARK);
				}
				}
				setState(155); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_QUESTIONMARK );
			setState(157);
			match(TK_NEWLINE);
			setState(158);
			instruction();
			setState(159);
			obligatoriness();
			setState(160);
			match(TK_NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnswerContext extends ParserRuleContext {
		public ParamAnswerContext paramAnswer() {
			return getRuleContext(ParamAnswerContext.class,0);
		}
		public AnswerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_answer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterAnswer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitAnswer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitAnswer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AnswerContext answer() throws RecognitionException {
		AnswerContext _localctx = new AnswerContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_answer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(162);
			paramAnswer();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamAnswerContext extends ParserRuleContext {
		public TerminalNode TK_NEWLINE() { return getToken(surveysParser.TK_NEWLINE, 0); }
		public List<TerminalNode> TK_NUMBER() { return getTokens(surveysParser.TK_NUMBER); }
		public TerminalNode TK_NUMBER(int i) {
			return getToken(surveysParser.TK_NUMBER, i);
		}
		public List<TerminalNode> TK_SPECIALCHARS() { return getTokens(surveysParser.TK_SPECIALCHARS); }
		public TerminalNode TK_SPECIALCHARS(int i) {
			return getToken(surveysParser.TK_SPECIALCHARS, i);
		}
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public ParamAnswerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramAnswer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterParamAnswer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitParamAnswer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitParamAnswer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamAnswerContext paramAnswer() throws RecognitionException {
		ParamAnswerContext _localctx = new ParamAnswerContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_paramAnswer);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(165); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(164);
					match(TK_NUMBER);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(167); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			} while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER );
			setState(172);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_SPECIALCHARS) {
				{
				{
				setState(169);
				match(TK_SPECIALCHARS);
				}
				}
				setState(174);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			{
			setState(178);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(175);
					match(TK_NUMBER);
					}
					} 
				}
				setState(180);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			setState(184);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_WORD) {
				{
				{
				setState(181);
				match(TK_WORD);
				}
				}
				setState(186);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(190);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TK_NUMBER) {
				{
				{
				setState(187);
				match(TK_NUMBER);
				}
				}
				setState(192);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			setState(193);
			match(TK_NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtraInfoContext extends ParserRuleContext {
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public ExtraInfoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extraInfo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterExtraInfo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitExtraInfo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitExtraInfo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExtraInfoContext extraInfo() throws RecognitionException {
		ExtraInfoContext _localctx = new ExtraInfoContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_extraInfo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(195);
				match(TK_WORD);
				}
				}
				setState(198); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_WORD );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstructionContext extends ParserRuleContext {
		public TerminalNode TK_WORD() { return getToken(surveysParser.TK_WORD, 0); }
		public TerminalNode TK_NEWLINE() { return getToken(surveysParser.TK_NEWLINE, 0); }
		public InstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterInstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitInstruction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitInstruction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstructionContext instruction() throws RecognitionException {
		InstructionContext _localctx = new InstructionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_instruction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(201);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TK_WORD) {
				{
				setState(200);
				match(TK_WORD);
				}
			}

			setState(204);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TK_NEWLINE) {
				{
				setState(203);
				match(TK_NEWLINE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FinalMessageContext extends ParserRuleContext {
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public FinalMessageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finalMessage; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterFinalMessage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitFinalMessage(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitFinalMessage(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FinalMessageContext finalMessage() throws RecognitionException {
		FinalMessageContext _localctx = new FinalMessageContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_finalMessage);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(206);
				match(TK_WORD);
				}
				}
				setState(209); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TK_WORD );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WordNumberContext extends ParserRuleContext {
		public List<TerminalNode> TK_WORD() { return getTokens(surveysParser.TK_WORD); }
		public TerminalNode TK_WORD(int i) {
			return getToken(surveysParser.TK_WORD, i);
		}
		public List<TerminalNode> TK_NUMBER() { return getTokens(surveysParser.TK_NUMBER); }
		public TerminalNode TK_NUMBER(int i) {
			return getToken(surveysParser.TK_NUMBER, i);
		}
		public WordNumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wordNumber; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterWordNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitWordNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitWordNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WordNumberContext wordNumber() throws RecognitionException {
		WordNumberContext _localctx = new WordNumberContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_wordNumber);
		int _la;
		try {
			setState(247);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(214);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TK_WORD) {
					{
					{
					setState(211);
					match(TK_WORD);
					}
					}
					setState(216);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(220);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TK_NUMBER) {
					{
					{
					setState(217);
					match(TK_NUMBER);
					}
					}
					setState(222);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(226);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TK_NUMBER) {
					{
					{
					setState(223);
					match(TK_NUMBER);
					}
					}
					setState(228);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(232);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TK_WORD) {
					{
					{
					setState(229);
					match(TK_WORD);
					}
					}
					setState(234);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(238);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TK_WORD) {
					{
					{
					setState(235);
					match(TK_WORD);
					}
					}
					setState(240);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(244);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TK_NUMBER) {
					{
					{
					setState(241);
					match(TK_NUMBER);
					}
					}
					setState(246);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObligatorinessContext extends ParserRuleContext {
		public TerminalNode TK_MANDATORY() { return getToken(surveysParser.TK_MANDATORY, 0); }
		public TerminalNode TK_OPTIONAL() { return getToken(surveysParser.TK_OPTIONAL, 0); }
		public TerminalNode TK_DEPENDENT() { return getToken(surveysParser.TK_DEPENDENT, 0); }
		public ObligatorinessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_obligatoriness; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).enterObligatoriness(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof surveysListener) ((surveysListener)listener).exitObligatoriness(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof surveysVisitor) return ((surveysVisitor<? extends T>)visitor).visitObligatoriness(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObligatorinessContext obligatoriness() throws RecognitionException {
		ObligatorinessContext _localctx = new ObligatorinessContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_obligatoriness);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TK_MANDATORY) | (1L << TK_OPTIONAL) | (1L << TK_DEPENDENT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\u0004\u0001\u0011\u00fc\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001"+
		"\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004"+
		"\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007"+
		"\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b"+
		"\u0002\f\u0007\f\u0001\u0000\u0001\u0000\u0005\u0000\u001d\b\u0000\n\u0000"+
		"\f\u0000 \t\u0000\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0004\u0001\'\b\u0001\u000b\u0001\f\u0001(\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0004\u0002.\b\u0002\u000b\u0002\f\u0002/\u0001\u0002\u0001"+
		"\u0002\u0001\u0003\u0001\u0003\u0004\u00036\b\u0003\u000b\u0003\f\u0003"+
		"7\u0001\u0003\u0001\u0003\u0001\u0003\u0004\u0003=\b\u0003\u000b\u0003"+
		"\f\u0003>\u0001\u0003\u0005\u0003B\b\u0003\n\u0003\f\u0003E\t\u0003\u0001"+
		"\u0003\u0001\u0003\u0004\u0003I\b\u0003\u000b\u0003\f\u0003J\u0001\u0003"+
		"\u0001\u0003\u0001\u0003\u0004\u0003P\b\u0003\u000b\u0003\f\u0003Q\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0004\u0001"+
		"\u0004\u0004\u0004[\b\u0004\u000b\u0004\f\u0004\\\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0005\u0004b\b\u0004\n\u0004\f\u0004e\t\u0004\u0001\u0004"+
		"\u0005\u0004h\b\u0004\n\u0004\f\u0004k\t\u0004\u0001\u0004\u0005\u0004"+
		"n\b\u0004\n\u0004\f\u0004q\t\u0004\u0001\u0004\u0005\u0004t\b\u0004\n"+
		"\u0004\f\u0004w\t\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0004\u0004"+
		"|\b\u0004\u000b\u0004\f\u0004}\u0001\u0004\u0001\u0004\u0001\u0004\u0001"+
		"\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005\u0004"+
		"\u0005\u0089\b\u0005\u000b\u0005\f\u0005\u008a\u0001\u0005\u0001\u0005"+
		"\u0004\u0005\u008f\b\u0005\u000b\u0005\f\u0005\u0090\u0001\u0005\u0005"+
		"\u0005\u0094\b\u0005\n\u0005\f\u0005\u0097\t\u0005\u0001\u0005\u0004\u0005"+
		"\u009a\b\u0005\u000b\u0005\f\u0005\u009b\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0006\u0001\u0006\u0001\u0007\u0004"+
		"\u0007\u00a6\b\u0007\u000b\u0007\f\u0007\u00a7\u0001\u0007\u0005\u0007"+
		"\u00ab\b\u0007\n\u0007\f\u0007\u00ae\t\u0007\u0001\u0007\u0005\u0007\u00b1"+
		"\b\u0007\n\u0007\f\u0007\u00b4\t\u0007\u0001\u0007\u0005\u0007\u00b7\b"+
		"\u0007\n\u0007\f\u0007\u00ba\t\u0007\u0001\u0007\u0005\u0007\u00bd\b\u0007"+
		"\n\u0007\f\u0007\u00c0\t\u0007\u0001\u0007\u0001\u0007\u0001\b\u0004\b"+
		"\u00c5\b\b\u000b\b\f\b\u00c6\u0001\t\u0003\t\u00ca\b\t\u0001\t\u0003\t"+
		"\u00cd\b\t\u0001\n\u0004\n\u00d0\b\n\u000b\n\f\n\u00d1\u0001\u000b\u0005"+
		"\u000b\u00d5\b\u000b\n\u000b\f\u000b\u00d8\t\u000b\u0001\u000b\u0005\u000b"+
		"\u00db\b\u000b\n\u000b\f\u000b\u00de\t\u000b\u0001\u000b\u0005\u000b\u00e1"+
		"\b\u000b\n\u000b\f\u000b\u00e4\t\u000b\u0001\u000b\u0005\u000b\u00e7\b"+
		"\u000b\n\u000b\f\u000b\u00ea\t\u000b\u0001\u000b\u0005\u000b\u00ed\b\u000b"+
		"\n\u000b\f\u000b\u00f0\t\u000b\u0001\u000b\u0005\u000b\u00f3\b\u000b\n"+
		"\u000b\f\u000b\u00f6\t\u000b\u0003\u000b\u00f8\b\u000b\u0001\f\u0001\f"+
		"\u0001\f\u0000\u0000\r\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012"+
		"\u0014\u0016\u0018\u0000\u0001\u0001\u0000\u000e\u0010\u0112\u0000\u001a"+
		"\u0001\u0000\u0000\u0000\u0002#\u0001\u0000\u0000\u0000\u0004*\u0001\u0000"+
		"\u0000\u0000\u00063\u0001\u0000\u0000\u0000\bX\u0001\u0000\u0000\u0000"+
		"\n\u0086\u0001\u0000\u0000\u0000\f\u00a2\u0001\u0000\u0000\u0000\u000e"+
		"\u00a5\u0001\u0000\u0000\u0000\u0010\u00c4\u0001\u0000\u0000\u0000\u0012"+
		"\u00c9\u0001\u0000\u0000\u0000\u0014\u00cf\u0001\u0000\u0000\u0000\u0016"+
		"\u00f7\u0001\u0000\u0000\u0000\u0018\u00f9\u0001\u0000\u0000\u0000\u001a"+
		"\u001e\u0003\u0006\u0003\u0000\u001b\u001d\u0005\u000b\u0000\u0000\u001c"+
		"\u001b\u0001\u0000\u0000\u0000\u001d \u0001\u0000\u0000\u0000\u001e\u001c"+
		"\u0001\u0000\u0000\u0000\u001e\u001f\u0001\u0000\u0000\u0000\u001f!\u0001"+
		"\u0000\u0000\u0000 \u001e\u0001\u0000\u0000\u0000!\"\u0005\u0000\u0000"+
		"\u0001\"\u0001\u0001\u0000\u0000\u0000#$\u0003\b\u0004\u0000$&\u0005\u000b"+
		"\u0000\u0000%\'\u0003\u0004\u0002\u0000&%\u0001\u0000\u0000\u0000\'(\u0001"+
		"\u0000\u0000\u0000(&\u0001\u0000\u0000\u0000()\u0001\u0000\u0000\u0000"+
		")\u0003\u0001\u0000\u0000\u0000*+\u0003\n\u0005\u0000+-\u0005\u000b\u0000"+
		"\u0000,.\u0003\f\u0006\u0000-,\u0001\u0000\u0000\u0000./\u0001\u0000\u0000"+
		"\u0000/-\u0001\u0000\u0000\u0000/0\u0001\u0000\u0000\u000001\u0001\u0000"+
		"\u0000\u000012\u0005\u000b\u0000\u00002\u0005\u0001\u0000\u0000\u0000"+
		"35\u0005\u0001\u0000\u000046\u0005\t\u0000\u000054\u0001\u0000\u0000\u0000"+
		"67\u0001\u0000\u0000\u000075\u0001\u0000\u0000\u000078\u0001\u0000\u0000"+
		"\u000089\u0001\u0000\u0000\u00009:\u0005\u000b\u0000\u0000:<\u0005\u0002"+
		"\u0000\u0000;=\u0005\n\u0000\u0000<;\u0001\u0000\u0000\u0000=>\u0001\u0000"+
		"\u0000\u0000><\u0001\u0000\u0000\u0000>?\u0001\u0000\u0000\u0000?C\u0001"+
		"\u0000\u0000\u0000@B\u0005\t\u0000\u0000A@\u0001\u0000\u0000\u0000BE\u0001"+
		"\u0000\u0000\u0000CA\u0001\u0000\u0000\u0000CD\u0001\u0000\u0000\u0000"+
		"DF\u0001\u0000\u0000\u0000EC\u0001\u0000\u0000\u0000FH\u0005\u000b\u0000"+
		"\u0000GI\u0005\n\u0000\u0000HG\u0001\u0000\u0000\u0000IJ\u0001\u0000\u0000"+
		"\u0000JH\u0001\u0000\u0000\u0000JK\u0001\u0000\u0000\u0000KL\u0001\u0000"+
		"\u0000\u0000LM\u0005\u000b\u0000\u0000MO\u0005\u000b\u0000\u0000NP\u0003"+
		"\u0002\u0001\u0000ON\u0001\u0000\u0000\u0000PQ\u0001\u0000\u0000\u0000"+
		"QO\u0001\u0000\u0000\u0000QR\u0001\u0000\u0000\u0000RS\u0001\u0000\u0000"+
		"\u0000ST\u0005\u000b\u0000\u0000TU\u0005\u0003\u0000\u0000UV\u0003\u0014"+
		"\n\u0000VW\u0005\u000b\u0000\u0000W\u0007\u0001\u0000\u0000\u0000XZ\u0005"+
		"\u0004\u0000\u0000Y[\u0005\t\u0000\u0000ZY\u0001\u0000\u0000\u0000[\\"+
		"\u0001\u0000\u0000\u0000\\Z\u0001\u0000\u0000\u0000\\]\u0001\u0000\u0000"+
		"\u0000]^\u0001\u0000\u0000\u0000^_\u0005\u000b\u0000\u0000_c\u0005\u0005"+
		"\u0000\u0000`b\u0005\n\u0000\u0000a`\u0001\u0000\u0000\u0000be\u0001\u0000"+
		"\u0000\u0000ca\u0001\u0000\u0000\u0000cd\u0001\u0000\u0000\u0000di\u0001"+
		"\u0000\u0000\u0000ec\u0001\u0000\u0000\u0000fh\u0005\t\u0000\u0000gf\u0001"+
		"\u0000\u0000\u0000hk\u0001\u0000\u0000\u0000ig\u0001\u0000\u0000\u0000"+
		"ij\u0001\u0000\u0000\u0000jo\u0001\u0000\u0000\u0000ki\u0001\u0000\u0000"+
		"\u0000ln\u0005\n\u0000\u0000ml\u0001\u0000\u0000\u0000nq\u0001\u0000\u0000"+
		"\u0000om\u0001\u0000\u0000\u0000op\u0001\u0000\u0000\u0000pu\u0001\u0000"+
		"\u0000\u0000qo\u0001\u0000\u0000\u0000rt\u0005\t\u0000\u0000sr\u0001\u0000"+
		"\u0000\u0000tw\u0001\u0000\u0000\u0000us\u0001\u0000\u0000\u0000uv\u0001"+
		"\u0000\u0000\u0000vx\u0001\u0000\u0000\u0000wu\u0001\u0000\u0000\u0000"+
		"xy\u0005\u000b\u0000\u0000y{\u0005\u0006\u0000\u0000z|\u0005\n\u0000\u0000"+
		"{z\u0001\u0000\u0000\u0000|}\u0001\u0000\u0000\u0000}{\u0001\u0000\u0000"+
		"\u0000}~\u0001\u0000\u0000\u0000~\u007f\u0001\u0000\u0000\u0000\u007f"+
		"\u0080\u0005\u000b\u0000\u0000\u0080\u0081\u0003\u0018\f\u0000\u0081\u0082"+
		"\u0005\u000b\u0000\u0000\u0082\u0083\u0005\u0007\u0000\u0000\u0083\u0084"+
		"\u0005\t\u0000\u0000\u0084\u0085\u0005\u000b\u0000\u0000\u0085\t\u0001"+
		"\u0000\u0000\u0000\u0086\u0088\u0005\b\u0000\u0000\u0087\u0089\u0005\t"+
		"\u0000\u0000\u0088\u0087\u0001\u0000\u0000\u0000\u0089\u008a\u0001\u0000"+
		"\u0000\u0000\u008a\u0088\u0001\u0000\u0000\u0000\u008a\u008b\u0001\u0000"+
		"\u0000\u0000\u008b\u008c\u0001\u0000\u0000\u0000\u008c\u008e\u0005\u000b"+
		"\u0000\u0000\u008d\u008f\u0005\n\u0000\u0000\u008e\u008d\u0001\u0000\u0000"+
		"\u0000\u008f\u0090\u0001\u0000\u0000\u0000\u0090\u008e\u0001\u0000\u0000"+
		"\u0000\u0090\u0091\u0001\u0000\u0000\u0000\u0091\u0095\u0001\u0000\u0000"+
		"\u0000\u0092\u0094\u0005\t\u0000\u0000\u0093\u0092\u0001\u0000\u0000\u0000"+
		"\u0094\u0097\u0001\u0000\u0000\u0000\u0095\u0093\u0001\u0000\u0000\u0000"+
		"\u0095\u0096\u0001\u0000\u0000\u0000\u0096\u0099\u0001\u0000\u0000\u0000"+
		"\u0097\u0095\u0001\u0000\u0000\u0000\u0098\u009a\u0005\f\u0000\u0000\u0099"+
		"\u0098\u0001\u0000\u0000\u0000\u009a\u009b\u0001\u0000\u0000\u0000\u009b"+
		"\u0099\u0001\u0000\u0000\u0000\u009b\u009c\u0001\u0000\u0000\u0000\u009c"+
		"\u009d\u0001\u0000\u0000\u0000\u009d\u009e\u0005\u000b\u0000\u0000\u009e"+
		"\u009f\u0003\u0012\t\u0000\u009f\u00a0\u0003\u0018\f\u0000\u00a0\u00a1"+
		"\u0005\u000b\u0000\u0000\u00a1\u000b\u0001\u0000\u0000\u0000\u00a2\u00a3"+
		"\u0003\u000e\u0007\u0000\u00a3\r\u0001\u0000\u0000\u0000\u00a4\u00a6\u0005"+
		"\t\u0000\u0000\u00a5\u00a4\u0001\u0000\u0000\u0000\u00a6\u00a7\u0001\u0000"+
		"\u0000\u0000\u00a7\u00a5\u0001\u0000\u0000\u0000\u00a7\u00a8\u0001\u0000"+
		"\u0000\u0000\u00a8\u00ac\u0001\u0000\u0000\u0000\u00a9\u00ab\u0005\r\u0000"+
		"\u0000\u00aa\u00a9\u0001\u0000\u0000\u0000\u00ab\u00ae\u0001\u0000\u0000"+
		"\u0000\u00ac\u00aa\u0001\u0000\u0000\u0000\u00ac\u00ad\u0001\u0000\u0000"+
		"\u0000\u00ad\u00b2\u0001\u0000\u0000\u0000\u00ae\u00ac\u0001\u0000\u0000"+
		"\u0000\u00af\u00b1\u0005\t\u0000\u0000\u00b0\u00af\u0001\u0000\u0000\u0000"+
		"\u00b1\u00b4\u0001\u0000\u0000\u0000\u00b2\u00b0\u0001\u0000\u0000\u0000"+
		"\u00b2\u00b3\u0001\u0000\u0000\u0000\u00b3\u00b8\u0001\u0000\u0000\u0000"+
		"\u00b4\u00b2\u0001\u0000\u0000\u0000\u00b5\u00b7\u0005\n\u0000\u0000\u00b6"+
		"\u00b5\u0001\u0000\u0000\u0000\u00b7\u00ba\u0001\u0000\u0000\u0000\u00b8"+
		"\u00b6\u0001\u0000\u0000\u0000\u00b8\u00b9\u0001\u0000\u0000\u0000\u00b9"+
		"\u00be\u0001\u0000\u0000\u0000\u00ba\u00b8\u0001\u0000\u0000\u0000\u00bb"+
		"\u00bd\u0005\t\u0000\u0000\u00bc\u00bb\u0001\u0000\u0000\u0000\u00bd\u00c0"+
		"\u0001\u0000\u0000\u0000\u00be\u00bc\u0001\u0000\u0000\u0000\u00be\u00bf"+
		"\u0001\u0000\u0000\u0000\u00bf\u00c1\u0001\u0000\u0000\u0000\u00c0\u00be"+
		"\u0001\u0000\u0000\u0000\u00c1\u00c2\u0005\u000b\u0000\u0000\u00c2\u000f"+
		"\u0001\u0000\u0000\u0000\u00c3\u00c5\u0005\n\u0000\u0000\u00c4\u00c3\u0001"+
		"\u0000\u0000\u0000\u00c5\u00c6\u0001\u0000\u0000\u0000\u00c6\u00c4\u0001"+
		"\u0000\u0000\u0000\u00c6\u00c7\u0001\u0000\u0000\u0000\u00c7\u0011\u0001"+
		"\u0000\u0000\u0000\u00c8\u00ca\u0005\n\u0000\u0000\u00c9\u00c8\u0001\u0000"+
		"\u0000\u0000\u00c9\u00ca\u0001\u0000\u0000\u0000\u00ca\u00cc\u0001\u0000"+
		"\u0000\u0000\u00cb\u00cd\u0005\u000b\u0000\u0000\u00cc\u00cb\u0001\u0000"+
		"\u0000\u0000\u00cc\u00cd\u0001\u0000\u0000\u0000\u00cd\u0013\u0001\u0000"+
		"\u0000\u0000\u00ce\u00d0\u0005\n\u0000\u0000\u00cf\u00ce\u0001\u0000\u0000"+
		"\u0000\u00d0\u00d1\u0001\u0000\u0000\u0000\u00d1\u00cf\u0001\u0000\u0000"+
		"\u0000\u00d1\u00d2\u0001\u0000\u0000\u0000\u00d2\u0015\u0001\u0000\u0000"+
		"\u0000\u00d3\u00d5\u0005\n\u0000\u0000\u00d4\u00d3\u0001\u0000\u0000\u0000"+
		"\u00d5\u00d8\u0001\u0000\u0000\u0000\u00d6\u00d4\u0001\u0000\u0000\u0000"+
		"\u00d6\u00d7\u0001\u0000\u0000\u0000\u00d7\u00dc\u0001\u0000\u0000\u0000"+
		"\u00d8\u00d6\u0001\u0000\u0000\u0000\u00d9\u00db\u0005\t\u0000\u0000\u00da"+
		"\u00d9\u0001\u0000\u0000\u0000\u00db\u00de\u0001\u0000\u0000\u0000\u00dc"+
		"\u00da\u0001\u0000\u0000\u0000\u00dc\u00dd\u0001\u0000\u0000\u0000\u00dd"+
		"\u00f8\u0001\u0000\u0000\u0000\u00de\u00dc\u0001\u0000\u0000\u0000\u00df"+
		"\u00e1\u0005\t\u0000\u0000\u00e0\u00df\u0001\u0000\u0000\u0000\u00e1\u00e4"+
		"\u0001\u0000\u0000\u0000\u00e2\u00e0\u0001\u0000\u0000\u0000\u00e2\u00e3"+
		"\u0001\u0000\u0000\u0000\u00e3\u00e8\u0001\u0000\u0000\u0000\u00e4\u00e2"+
		"\u0001\u0000\u0000\u0000\u00e5\u00e7\u0005\n\u0000\u0000\u00e6\u00e5\u0001"+
		"\u0000\u0000\u0000\u00e7\u00ea\u0001\u0000\u0000\u0000\u00e8\u00e6\u0001"+
		"\u0000\u0000\u0000\u00e8\u00e9\u0001\u0000\u0000\u0000\u00e9\u00f8\u0001"+
		"\u0000\u0000\u0000\u00ea\u00e8\u0001\u0000\u0000\u0000\u00eb\u00ed\u0005"+
		"\n\u0000\u0000\u00ec\u00eb\u0001\u0000\u0000\u0000\u00ed\u00f0\u0001\u0000"+
		"\u0000\u0000\u00ee\u00ec\u0001\u0000\u0000\u0000\u00ee\u00ef\u0001\u0000"+
		"\u0000\u0000\u00ef\u00f8\u0001\u0000\u0000\u0000\u00f0\u00ee\u0001\u0000"+
		"\u0000\u0000\u00f1\u00f3\u0005\t\u0000\u0000\u00f2\u00f1\u0001\u0000\u0000"+
		"\u0000\u00f3\u00f6\u0001\u0000\u0000\u0000\u00f4\u00f2\u0001\u0000\u0000"+
		"\u0000\u00f4\u00f5\u0001\u0000\u0000\u0000\u00f5\u00f8\u0001\u0000\u0000"+
		"\u0000\u00f6\u00f4\u0001\u0000\u0000\u0000\u00f7\u00d6\u0001\u0000\u0000"+
		"\u0000\u00f7\u00e2\u0001\u0000\u0000\u0000\u00f7\u00ee\u0001\u0000\u0000"+
		"\u0000\u00f7\u00f4\u0001\u0000\u0000\u0000\u00f8\u0017\u0001\u0000\u0000"+
		"\u0000\u00f9\u00fa\u0007\u0000\u0000\u0000\u00fa\u0019\u0001\u0000\u0000"+
		"\u0000\"\u001e(/7>CJQ\\ciou}\u008a\u0090\u0095\u009b\u00a7\u00ac\u00b2"+
		"\u00b8\u00be\u00c6\u00c9\u00cc\u00d1\u00d6\u00dc\u00e2\u00e8\u00ee\u00f4"+
		"\u00f7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}