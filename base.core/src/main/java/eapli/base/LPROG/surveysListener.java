// Generated from C:/Users/berna/OneDrive/Documents/lei21_22_s4_2df_04/base.app.other.console/src/main/java/LPROGGRAMMAR\surveys.g4 by ANTLR 4.10.1
package eapli.base.LPROG;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link surveysParser}.
 */
public interface surveysListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link surveysParser#projeto}.
	 * @param ctx the parse tree
	 */
	void enterProjeto(surveysParser.ProjetoContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#projeto}.
	 * @param ctx the parse tree
	 */
	void exitProjeto(surveysParser.ProjetoContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#section}.
	 * @param ctx the parse tree
	 */
	void enterSection(surveysParser.SectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#section}.
	 * @param ctx the parse tree
	 */
	void exitSection(surveysParser.SectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#question}.
	 * @param ctx the parse tree
	 */
	void enterQuestion(surveysParser.QuestionContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#question}.
	 * @param ctx the parse tree
	 */
	void exitQuestion(surveysParser.QuestionContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#param}.
	 * @param ctx the parse tree
	 */
	void enterParam(surveysParser.ParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#param}.
	 * @param ctx the parse tree
	 */
	void exitParam(surveysParser.ParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#paramSection}.
	 * @param ctx the parse tree
	 */
	void enterParamSection(surveysParser.ParamSectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#paramSection}.
	 * @param ctx the parse tree
	 */
	void exitParamSection(surveysParser.ParamSectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#paramQuestion}.
	 * @param ctx the parse tree
	 */
	void enterParamQuestion(surveysParser.ParamQuestionContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#paramQuestion}.
	 * @param ctx the parse tree
	 */
	void exitParamQuestion(surveysParser.ParamQuestionContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#answer}.
	 * @param ctx the parse tree
	 */
	void enterAnswer(surveysParser.AnswerContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#answer}.
	 * @param ctx the parse tree
	 */
	void exitAnswer(surveysParser.AnswerContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#paramAnswer}.
	 * @param ctx the parse tree
	 */
	void enterParamAnswer(surveysParser.ParamAnswerContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#paramAnswer}.
	 * @param ctx the parse tree
	 */
	void exitParamAnswer(surveysParser.ParamAnswerContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#extraInfo}.
	 * @param ctx the parse tree
	 */
	void enterExtraInfo(surveysParser.ExtraInfoContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#extraInfo}.
	 * @param ctx the parse tree
	 */
	void exitExtraInfo(surveysParser.ExtraInfoContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#instruction}.
	 * @param ctx the parse tree
	 */
	void enterInstruction(surveysParser.InstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#instruction}.
	 * @param ctx the parse tree
	 */
	void exitInstruction(surveysParser.InstructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#finalMessage}.
	 * @param ctx the parse tree
	 */
	void enterFinalMessage(surveysParser.FinalMessageContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#finalMessage}.
	 * @param ctx the parse tree
	 */
	void exitFinalMessage(surveysParser.FinalMessageContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#wordNumber}.
	 * @param ctx the parse tree
	 */
	void enterWordNumber(surveysParser.WordNumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#wordNumber}.
	 * @param ctx the parse tree
	 */
	void exitWordNumber(surveysParser.WordNumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link surveysParser#obligatoriness}.
	 * @param ctx the parse tree
	 */
	void enterObligatoriness(surveysParser.ObligatorinessContext ctx);
	/**
	 * Exit a parse tree produced by {@link surveysParser#obligatoriness}.
	 * @param ctx the parse tree
	 */
	void exitObligatoriness(surveysParser.ObligatorinessContext ctx);
}