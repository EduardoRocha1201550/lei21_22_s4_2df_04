package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Product_code implements ValueObject{
    private String product_code;

    public Product_code(String product_code){
        if (isValidProductCode(product_code)==true){
            this.product_code=product_code;
        }
    }

    public Product_code(){}

    public boolean isValidProductCode(String product_code){
        if (product_code.length()<=0 && product_code.length()<=23){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return
                  product_code + '\''
                ;
    }
}

