package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Short_description implements ValueObject {
    private String short_description;

    public Short_description(String short_description) {
        if (isValidShortDescription(short_description) == true) {
            this.short_description = short_description;
        }
    }

    public Short_description(){

    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public boolean isValidShortDescription(String short_description){
        if(short_description.length()>0 && short_description.length()<31){
            return true;
        }return false;
    }

    @Override
    public String toString() {
        return
                  short_description + '\''
                ;
    }
}
