package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Internal_code implements ValueObject, Comparable<Internal_code> {
    private String internal;

    public Internal_code (String internal) {
        if (isValidInternalCode(internal) == true) {
            this.internal = internal;
        }
    }
    public Internal_code(){

    }

    public boolean isValidInternalCode(String internal){
        if (internal.length()<=0 && internal.length()<=23){
            return false;
        }
        return true;

    }

    @Override
    public int compareTo(Internal_code o) {
        return this.internal.compareTo(o.internal);
    }

    @Override
    public String toString() {
        return
                  internal;
    }
}
