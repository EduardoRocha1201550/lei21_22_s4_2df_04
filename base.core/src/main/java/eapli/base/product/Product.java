package eapli.base.product;


import eapli.base.generalmanagement.domain.Category;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.general.domain.model.Money;


import javax.persistence.*;

@Entity
public class Product implements AggregateRoot<Internal_code> {
    @EmbeddedId
    private Internal_code internal_code;

    @Embedded
    private Product_code product_code;
    @Embedded
    private Short_description short_description;
    @Embedded
    private Extended_description extended_description;
    @Embedded
    private Technical_description technical_description;
    @Embedded
    private Reference reference;
    @Embedded
    private Photo photoPath;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "amount", column = @Column(name = "no_taxes_amount")),
            @AttributeOverride(name = "currency", column = @Column(name = "no_taxes_currency"))
    })
    private Money priceWithoutTx;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "amount", column = @Column(name = "taxes_amount")),
            @AttributeOverride(name = "currency", column = @Column(name = "taxes_currency"))
    })
    private Money price;
    @Embedded
    private Brand brand;
    @ManyToOne
    @JoinColumn(name = "category_code")
    private Category category;
    private boolean active;

    @Embedded
    private Volume volume;
    @Embedded
    private Weight weight;

    @Embedded
    private Barcode barcode;

    @Embedded
    private Product_Location location;

    // private Barcode barcode;

    public Volume getVolume() {
        return volume;
    }

    public void setVolume(Volume volume) {
        this.volume = volume;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public void setPriceWithoutTx(Money priceWithoutTx) {
        this.priceWithoutTx = priceWithoutTx;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public void setShort_description(Short_description short_description) {
        this.short_description = short_description;
    }

    public void setExtended_description(Extended_description extended_description) {
        this.extended_description = extended_description;
    }

    public void setTechnical_description(Technical_description technical_description) {
        this.technical_description = technical_description;
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Internal_code getInternal_code() {
        return internal_code;
    }

    public void setInternal_code(Internal_code internal_code) {
        this.internal_code = internal_code;
    }


    public Product(Internal_code internal_code, Product_code product_code, Short_description short_description, Extended_description extended_description, Technical_description technical_description, Reference reference, Money priceWithoutTx, Money price, Category category, Brand brand, boolean active, Volume volume, Weight weight, Barcode barcode, Product_Location location, Photo photoPath) {
        this.product_code = product_code;
        this.internal_code = internal_code;
        this.short_description = short_description;
        this.extended_description = extended_description;
        this.technical_description= technical_description;
        this.reference = reference;
        this.priceWithoutTx=priceWithoutTx;
        this.price = price;
        this.category = category;
        this.brand=brand;
        this.active=active;
        this.volume=volume;
        this.weight=weight;
        this.barcode=barcode;
        this.location=location;
        this.photoPath=photoPath;
    }

    public Product(Internal_code internal_code, Short_description short_description, Extended_description extended_description, Technical_description technical_description, Reference reference, Money priceWithoutTx, Money price,Category category, Brand brand, boolean active, Volume volume, Weight weight, Barcode barcode, Product_Location location,Photo photoPath) {
        this.internal_code = internal_code;
        this.short_description = short_description;
        this.extended_description = extended_description;
        this.technical_description = technical_description;
        this.reference = reference;
        this.priceWithoutTx = priceWithoutTx;
        this.price = price;
        this.category = category;
        this.brand = brand;
        this.active = active;
        this.volume = volume;
        this.weight = weight;
        this.barcode = barcode;
        this.location=location;
        this.photoPath=photoPath;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public Product_Location getLocation() {
        return location;
    }

    public enum TYPE {         EAN13,         UPC;     }

    public Product() {

    }


    public Product_code getProduct_code() {
        return product_code;
    }

    public Technical_description getTechnical_description() {
        return technical_description;
    }

    public Money getPriceWithoutTx() {
        return priceWithoutTx;
    }

    public Weight getWeight() {
        return weight;
    }
    public Short_description getShort_description() {
        return short_description;
    }

    public Extended_description getExtended_description() {
        return extended_description;
    }

    public Reference getReference() {
        return reference;
    }

    public Money getPrice() {
        return price;
    }

    public Category getCategory() {
        return category;
    }

    public Brand getBrand() {
        return brand;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    @Override
    public boolean sameAs(Object other) {
        return false;

    }

    @Override
    public Internal_code identity() {
        return internal_code;
    }

    @Override
    public String toString() {
        return "Product" +
                "\n inernal_code=" + internal_code  +
                "\n product_code=" + product_code +
                "\n short_description=" + short_description +
                "\n extended_description=" + extended_description +
                "\n brand="+brand+
                "\n category="+category+
                "\n unit price="+ price+
                "\n\n\n";
    }
}

