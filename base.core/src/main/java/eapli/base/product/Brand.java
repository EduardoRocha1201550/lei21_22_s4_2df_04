package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Brand implements ValueObject {
    private String brand;

    public Brand(String brand) {
        if (isValidBrand(brand) == true) {
            this.brand = brand;
        }
    }

    public Brand(){

    }

    public boolean isValidBrand(String brand){
        if(brand.length()>0 && brand.length()<51){
            return true;
        }return false;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return
                "brand='" + brand + '\''
                ;
    }
}
