package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Technical_description implements ValueObject {
    private String technical_description;

    public Technical_description(String technical_description) {
        if (isValidTechnicalDescription(technical_description) == true) {
            this.technical_description = technical_description;
        }
    }

    public Technical_description(){

    }
    public boolean isValidTechnicalDescription(String technical_description){
        if(technical_description.length()>0 && technical_description.length()<31){
            return true;
        }return false;
    }

    @Override
    public String toString() {
        return
                 technical_description + '\''
                ;
    }
}
