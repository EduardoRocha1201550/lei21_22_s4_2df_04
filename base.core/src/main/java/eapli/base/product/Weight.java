package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Weight implements ValueObject {
    private double weight;

    public Weight(double weight) {
        if (isValidWeight(weight) == true) {
            this.weight = weight;
        }
    }

    public Weight(){

    }
    public boolean isValidWeight(double weight){
        if(weight>0){
            return true;
        }return false;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return                 "weight=" + weight
                ;
    }
}
