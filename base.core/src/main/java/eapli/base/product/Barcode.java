package eapli.base.product;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;

import javax.persistence.Embeddable;

@Embeddable
public class Barcode implements ValueObject {
    private long barcode;

    protected Barcode() {
    }

    public Barcode(Long barcode, Product.TYPE type) {
        if (type.equals(Product.TYPE.EAN13)) {
            Preconditions.ensure(barcode.toString().length() == 13);
            this.barcode = barcode;
        } else if (type.equals(Product.TYPE.UPC)) {
            Preconditions.ensure(barcode.toString().length() == 12);
            this.barcode = barcode;
        }

    }

    @Override
    public String toString() {
        return
                "barcode=" + barcode
                ;
    }
}
