package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class PriceWithoutTx implements ValueObject {
    private double priceWithoutTx;

    public PriceWithoutTx(double priceWithoutTx) {
        if (isValidPriceWithoutTx(priceWithoutTx) == true) {
            this.priceWithoutTx = priceWithoutTx;
        }
    }

    public PriceWithoutTx(){

    }
    public boolean isValidPriceWithoutTx(double priceWithoutTx){
        if(priceWithoutTx>0.0){
            return true;
        }return false;
    }
}
