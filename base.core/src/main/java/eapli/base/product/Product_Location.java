package eapli.base.product;

import eapli.base.product.Internal_code;
import eapli.base.product.Product;
import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Embeddable
public class Product_Location implements ValueObject {
    private int warehouse_Id;
    private int shelf_Id;
    private int aisle_Id;
    private int row_Id;
    public Product_Location(int warehouse_Id, int aisle_Id, int row_Id, int shelf_Id) {
        this.warehouse_Id = warehouse_Id;
        this.shelf_Id = shelf_Id;
        this.aisle_Id = aisle_Id;
        this.row_Id = row_Id;
    }

    public Product_Location() {

    }

    @Override
    public String toString() {
        return
                "warehouse_Id=" + warehouse_Id +
                ", shelf_Id=" + shelf_Id +
                ", aisle_Id=" + aisle_Id +
                ", row_Id=" + row_Id
                ;
    }
}
