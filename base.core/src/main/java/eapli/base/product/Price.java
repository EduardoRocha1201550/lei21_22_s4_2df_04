package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Price implements ValueObject {
    private double price;

    public Price(double price) {
        if (isValidPrice(price) == true) {
            this.price = price;
        }
    }

    public Price(){

    }
    public boolean isValidPrice(double price){
        if(price>0.0){
            return true;
        }return false;
    }
}
