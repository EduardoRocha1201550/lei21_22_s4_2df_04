package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Volume implements ValueObject {
    private double A;
    private double L;
    private double C;

    public Volume (double A,double L,double C) {
        if (isValidVolume(A,L,C) == true) {
            this.A = A;
            this.C= C;
            this.L= L;
        }
    }
    public Volume(){

    }

    public double getA() {
        return A;
    }

    public void setA(double a) {
        A = a;
    }

    public double getL() {
        return L;
    }

    public void setL(double l) {
        L = l;
    }

    public double getC() {
        return C;
    }

    public void setC(double c) {
        C = c;
    }

    public boolean isValidVolume(double A, double L, double C){
        if (A!=0 && L!=0 && C!=0){
            return true;
        }
        return false;

    }

    @Override
    public String toString() {
        return
                "A=" + A +
                ", L=" + L +
                ", C=" + C
               ;
    }
}
