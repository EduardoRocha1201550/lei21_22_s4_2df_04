package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Extended_description implements ValueObject {
    private String extended_description;

    public Extended_description(String extended_description) {
        if (isValidExtendedDescription(extended_description) == true) {
            this.extended_description = extended_description;
        }
    }

    public Extended_description(){

    }
    public boolean isValidExtendedDescription(String extended_description){
        if(extended_description.length()>20 && extended_description.length()<101){
            return true;
        }return false;
    }

    @Override
    public String toString() {
        return
                 extended_description + '\''
                ;
    }
}
