package eapli.base.product;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Reference implements ValueObject {
    private String reference;

    public Reference(String reference) {
        if (isValidReference(reference) == true) {
            this.reference = reference;
        }
    }

    public Reference(){

    }
    public boolean isValidReference(String reference){
        if(reference.length()>0 && reference.length()<24){
            return true;
        }return false;
    }

    @Override
    public String toString() {
        return
                "reference='" + reference + '\''
                ;
    }
}
