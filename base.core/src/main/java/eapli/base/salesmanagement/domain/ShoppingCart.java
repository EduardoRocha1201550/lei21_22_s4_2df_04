package eapli.base.salesmanagement.domain;

import eapli.base.costumer.domain.Costumer;
import eapli.framework.domain.model.AggregateRoot;
import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class ShoppingCart implements AggregateRoot<Integer> {

    @Id
    @GeneratedValue
    private Integer id;

    @OneToOne
    private Costumer costumer;


    @OneToMany(cascade = CascadeType.PERSIST)
    private List<OrderItem> list;


    public ShoppingCart(Costumer costumer,  List<OrderItem> list) {
        this.costumer=costumer;
        this.list=list;
    }

    public ShoppingCart() {

    }

    public void setList(List<OrderItem> list) {
        this.list = list;
    }

    public List<OrderItem> getList() {
        return list;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Integer identity() {
        return null;
    }

    public Costumer getCostumer() {
        return costumer;
    }
}
