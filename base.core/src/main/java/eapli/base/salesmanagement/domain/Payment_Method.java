package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.ValueObject;

import javax.persistence.*;

@Embeddable
public class Payment_Method implements ValueObject {
    @Embedded
    private OrderName orderName;
    @Embedded
    private Payment_Confirmation payment_Confirmation;

    public Payment_Method() {

    }

    public void setName(OrderName name) {
        this.orderName = name;
    }

    public Payment_Confirmation getPayment_Confirmation() {
        return payment_Confirmation;
    }

    public void setPayment_Confirmation(Payment_Confirmation payment_Confirmation) {
        this.payment_Confirmation = payment_Confirmation;
    }

    public Payment_Method(OrderName name, Payment_Confirmation payment_Confirmation) {
        this.orderName = name;
        this.payment_Confirmation = payment_Confirmation;
    }

    public OrderName getName() {
        return orderName;
    }
}
