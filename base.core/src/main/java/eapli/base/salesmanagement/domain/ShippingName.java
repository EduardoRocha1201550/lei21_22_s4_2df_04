package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class ShippingName implements ValueObject {
    private String shipingName;

    public ShippingName() {
    }

    public String getName() {
        return shipingName;
    }

    public void setName(String name) {
        this.shipingName = name;
    }

    public ShippingName(String name) {
        this.shipingName = name;
    }
}
