package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class State implements ValueObject {
    private String state;

    public State(String state) {
        if(isValid(state)) {
            this.state = state;
        }
    }

    public State() {

    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public static final String STATEDONE="Done";
    public static final String STATEDOING="Doing";
    public static final String STATETODO="To Do";
    public static final String STATESHIPPING="Shipping";
    public boolean isValid(String state){
        if(state.equals(STATEDONE)||state.equals(STATETODO)||state.equals(STATEDOING)||state.equals(STATESHIPPING)){
            return true;
        }else{
            return false;
        }
    }
}
