package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.*;

@Embeddable
public class Shipping_Method implements ValueObject {
    @Embedded
    private ShippingName shipingName;
    @Embedded
    private ShipPrice shipingPrice;

    public Shipping_Method(ShippingName shipingName, ShipPrice shipingPrice) {
        this.shipingName = shipingName;
        this.shipingPrice = shipingPrice;
    }

    public ShippingName getShipingName() {
        return shipingName;
    }

    public void setShipingName(ShippingName shipingName) {
        this.shipingName = shipingName;
    }

    public ShipPrice getShipingPrice() {
        return shipingPrice;
    }

    public void setShipingPrice(ShipPrice shipingPrice) {
        this.shipingPrice = shipingPrice;
    }

    public Shipping_Method(){

    }
}
