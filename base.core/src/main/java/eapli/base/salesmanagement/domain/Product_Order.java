package eapli.base.salesmanagement.domain;

import eapli.base.costumer.domain.Costumer;
import eapli.framework.domain.model.AggregateRoot;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
/**
 * Product Order
 */
@Entity
public class Product_Order implements  AggregateRoot<Long>{
    @Id
    @GeneratedValue
    private Long id;
    /**
     * Atributte Date
     */
    private Date dateTime;
    /**
     * Atributte Delivering_Address
     */
    @Embedded
    private DeliveringAddress delivering_address;

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Atributte Volume
     */
    @Embedded
    private Volume volume;

    public Date getDateTime() {
        return dateTime;
    }

    @Embedded
    private Weight weight;
    @ManyToOne
    private Costumer costumer;
    @Embedded
    private Shipping_Method shippingMethod;
    @Embedded
    private OrderPrice orderPrice;
    @Embedded
    private State state;
    @Embedded
    private Payment_Method paymentMethod;

    @Transient
    private List<OrderItem> sectionList;

    public Volume getVolume() {
        return volume;
    }

    public Weight getWeight() {
        return weight;
    }

    public Product_Order(Date dateTime, DeliveringAddress delivering_address, Volume volume, Weight weight, Costumer costumer, Shipping_Method shippingMethod, Payment_Method paymentMethod, State state, OrderPrice price, List<OrderItem> sectionList) {
        this.dateTime = dateTime;
        this.delivering_address = delivering_address;
        this.volume = volume;
        this.weight = weight;
        this.costumer = costumer;
        this.shippingMethod = shippingMethod;
        this.orderPrice = price;
        this.state = state;
        this.paymentMethod = paymentMethod;
        this.sectionList = sectionList;
    }

    public Product_Order() {
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean sameAs(Object other) {
        if (Object.class!=other.getClass()){
            return false;
        }

        Product_Order otherP = (Product_Order) other;
        if (this.id==otherP.getId()){
            return true;
        }

        return false;
    }

    @Override
    public Long identity() {
        return id;
    }

    @Override
    public String toString(){

        return "Order ID:"+id+"\n" +
                "Order Date:"+dateTime+"\n" +
                "Order Price:"+orderPrice.getPrice()+"\n" +
                "Total Weight:"+weight+"\n" +
                "Total Volume:"+volume+"\n";
    }
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }



}
