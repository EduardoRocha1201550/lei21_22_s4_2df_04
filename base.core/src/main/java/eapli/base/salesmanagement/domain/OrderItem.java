package eapli.base.salesmanagement.domain;

import eapli.base.product.Product;
import eapli.framework.domain.model.AggregateRoot;


import javax.persistence.*;

@Entity
public class OrderItem implements AggregateRoot<Integer> {
    @Id
    private int id;

    @Embedded
    @Column
    private Quantity quantity;
    @ManyToOne
    private Product product;

    public OrderItem(Quantity quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }


    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public OrderItem() {

    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Integer identity() {
        return null;
    }
}
