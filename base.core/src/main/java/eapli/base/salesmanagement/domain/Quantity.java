package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Quantity implements ValueObject {
    private int quantity;

    public Quantity() {

    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Quantity(int quantity) {
        this.quantity = quantity;
    }
}
