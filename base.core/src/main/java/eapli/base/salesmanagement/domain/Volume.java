package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Volume implements ValueObject {
    private double total;

    public Volume(double total) {
        if (isValidVolume(total) == true) {
            this.total = total;
        }
    }

    public Volume() {
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return ""+total;
    }

    public boolean isValidVolume(double total){
        if (total>0){
            return true;
        }
        return false;

    }
}
