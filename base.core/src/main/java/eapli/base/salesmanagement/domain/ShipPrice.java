package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class ShipPrice implements ValueObject {
    private double shipingPrice;

    public ShipPrice(double price) {
        if (isValidPrice(price) == true) {
            this.shipingPrice = price;
        }
    }

    public ShipPrice(){

    }

    public double getPrice() {
        return shipingPrice;
    }

    public void setPrice(double price) {
        this.shipingPrice = price;
    }

    public boolean isValidPrice(double price){
        if(price>0.0){
            return true;
        }return false;
    }
}
