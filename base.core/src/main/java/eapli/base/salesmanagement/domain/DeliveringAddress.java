package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.text.ParseException;

@Embeddable
public class DeliveringAddress implements ValueObject {
    private String DeliveringAddress;
    private String city;
    private String postalCode;

    public String getDeliveringAddress() {
        return DeliveringAddress;
    }

    public DeliveringAddress(String deliveringAddress, String city, String postalCode) {
        DeliveringAddress = deliveringAddress;
        this.city = city;
        this.postalCode = postalCode;
    }

    public void setDeliveringAddress(String deliveringAddress) {
        DeliveringAddress = deliveringAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public DeliveringAddress() {

    }
    public void isValid(String str) throws ParseException {
        String temp =str.replaceAll("-","");
        try {
            Integer.parseInt(temp);
        }catch (Exception e){
            throw new IllegalArgumentException("Invalid PostalCode");
        }
        if (str.length()!=8){
            throw new IllegalArgumentException("Invalid PostalCode");
        }
    }
}