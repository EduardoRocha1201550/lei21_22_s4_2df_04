package eapli.base.salesmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.text.ParseException;
@Embeddable
public class Payment_Confirmation implements ValueObject {
    private String text;

    public Payment_Confirmation(String text) {
        this.text = text;
    }

    public Payment_Confirmation() {
    }
    public void isValid(String str) throws ParseException {
        if (str.length()>512 && str.length()<1){
            throw new IllegalArgumentException("Invalid Payment Confirmation");
        }
    }
}
