package eapli.base.salesmanagement.application;

import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.costumer.application.CostumerController;
import eapli.base.costumer.domain.Costumer;
import eapli.base.generalmanagement.repositories.RulesRepository;
import eapli.base.product.Product;
import eapli.base.salesmanagement.domain.*;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.SQLException;
import java.util.*;

public class Product_OrderController {

    private final RulesRepository rulesRepository= PersistenceContext.repositories().rulesRepository();
    private final Product_OrderRepository orderRepository = PersistenceContext.repositories().orders();
    private final CostumerController costumerController = new CostumerController();

    public Product_Order registerOrder(Date dateTime, DeliveringAddress delivering_address, Volume volume, Weight weight, Costumer costumer, Shipping_Method shippingMethod, Payment_Method paymentMethod, State state,LinkedList<OrderItem> sectionList,OrderPrice price){
        AuthorizationService authorizationService = AuthzRegistry.authorizationService();
        authorizationService.ensureAuthenticatedUserHasAnyOf(BaseRoles.CLIENT_USER,BaseRoles.SALESCLERK);
        Product_Order order= new Product_Order(dateTime,delivering_address,volume,weight,costumer,shippingMethod,paymentMethod,state,price,sectionList);
        System.out.println(order);
        return orderRepository.save(order);
    }
    public Weight getWeightFromOrder(LinkedList<OrderItem> list){
        double total=0.0;
        for (OrderItem item : list){
            total=total+item.getProduct().getWeight().getWeight();
        }
        return new Weight(total);
    }
    public Volume getTotalVolume(LinkedList<OrderItem> list){
        double total=0.0;
        for (OrderItem item : list){
            total=total+this.getVolume(item.getProduct().getVolume());
        }
        return new Volume(total);
    }
    public double getVolume(eapli.base.product.Volume v){
        return v.getA()*v.getC()*v.getL();
    }
    public OrderItem containsProduct (LinkedList<OrderItem> list,Product product){
        for (OrderItem orderItem : list){
            if (orderItem.getProduct().equals(product)){
                return orderItem;
            }
        }
        return null;
    }
    public OrderPrice getPriceFromList(LinkedList<OrderItem> list){
        double total=0.0;
        for (OrderItem orderItem : list){
            total=total+orderItem.getProduct().getPrice().amount().doubleValue();
        }
        return new OrderPrice(total);
    }
    public List<Product_Order> getOrdersFromCostumer(int costumerID) throws SQLException, InterruptedException {
        return orderRepository.getOrdersFromCostumer(costumerID);
    }
    public Product_Order findOrderById(Long id){
        return orderRepository.findOrderById(id);
    }
    public List<Product_Order> getOrders(){
        return null;
    }

    public Iterable<Rules> findByContent(Content content){
        return rulesRepository.findByContent(content);
    }


    public Iterable<Rules> findAll(){
        return rulesRepository.findAll();
    }
}
