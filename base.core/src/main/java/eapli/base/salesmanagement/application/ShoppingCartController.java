package eapli.base.salesmanagement.application;


import eapli.base.costumer.domain.*;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.product.Internal_code;
import eapli.base.product.Product;
import eapli.base.salesmanagement.domain.OrderItem;
import eapli.base.salesmanagement.domain.Quantity;
import eapli.base.salesmanagement.domain.ShoppingCart;
import eapli.base.salesmanagement.repositories.OrderItemRepository;
import eapli.base.salesmanagement.repositories.ShoppingCartRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class ShoppingCartController {


    private final ShoppingCartRepository shoppingCartRepository = PersistenceContext.repositories().shoppingCart();
    private final CostumerRepository costumerRepository = PersistenceContext.repositories().costumer();
    private final ProductRepository productRepository= PersistenceContext.repositories().products();
    private final OrderItemRepository orderItemRepository= PersistenceContext.repositories().orderItem();





    public ShoppingCart registerShoppingCart(Costumer costumer, List<OrderItem> list) {
        ShoppingCart shoppingCart= new ShoppingCart(costumer,list);
        for(OrderItem item:list){
            orderItemRepository.save(item);
        }

        return shoppingCartRepository.save(shoppingCart);
    }

    public boolean shoppingCartExistance(String userEmail){
        Email email=new Email(userEmail);
        Costumer costumer= costumerRepository.findByEmail(email);

        try{
            shoppingCartRepository.findShoppingCart(costumer);
            return true;
        }catch(Exception e){
            return false;
        }

    }
    public ShoppingCart adicionarProdutos(Costumer costumer,List<OrderItem> list) {
        ShoppingCart shoppingCart= shoppingCartRepository.findShoppingCart(costumer);

        List<OrderItem> listaExistente= new ArrayList<>(shoppingCart.getList());

        for(OrderItem items: listaExistente){//ja existentes no shopping cart
            for(OrderItem newItems: list ){//novos produtos a ser adicionados

                if(Objects.equals(items.getProduct(),newItems.getProduct())){
                    Quantity um=items.getQuantity();
                    Quantity dois=newItems.getQuantity();
                    Quantity novo= new Quantity(um.getQuantity()+dois.getQuantity());
                    newItems.setQuantity(novo);
                    list.remove(newItems);
                }
            }
        }
        listaExistente.addAll(list);
        shoppingCart.setList(listaExistente);
        return shoppingCartRepository.save(shoppingCart);
    }



    public Product getProduct(Internal_code internal_code){
        return productRepository.findProductByCode(internal_code);
    }


    public Costumer getCostumerEmail(String userEmail){
        Email email=new Email(userEmail);
        return costumerRepository.findByEmail(email);
    }
    public Iterable<Product> allProducts(){
        return productRepository.findAll();
    }
}
