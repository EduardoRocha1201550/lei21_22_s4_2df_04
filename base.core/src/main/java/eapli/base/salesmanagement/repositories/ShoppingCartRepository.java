package eapli.base.salesmanagement.repositories;

import eapli.base.costumer.domain.Costumer;
import eapli.base.salesmanagement.domain.ShoppingCart;
import eapli.framework.domain.repositories.DomainRepository;

public interface ShoppingCartRepository extends  DomainRepository <Integer, ShoppingCart>{

    ShoppingCart findShoppingCart(Costumer costumer);

}
