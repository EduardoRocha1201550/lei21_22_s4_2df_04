package eapli.base.salesmanagement.repositories;

import eapli.base.salesmanagement.domain.OrderItem;
import eapli.framework.domain.repositories.DomainRepository;

public interface OrderItemRepository extends DomainRepository<Integer, OrderItem> {
}
