package eapli.base.salesmanagement.repositories;

import eapli.base.salesmanagement.domain.Product_Order;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.SQLException;
import java.util.List;

public interface Product_OrderRepository extends DomainRepository<Long, Product_Order> {
    List<Product_Order> findAvailable();
    List<Product_Order> findAvailableDoing();
    List<Product_Order> findAvailableDone();
    Product_Order findOrderById(Long id);
    List<Product_Order> getOrdersFromCostumer(int id) throws SQLException, InterruptedException;
}