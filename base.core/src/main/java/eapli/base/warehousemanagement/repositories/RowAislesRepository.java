package eapli.base.warehousemanagement.repositories;

import eapli.base.warehousemanagement.domain.Rows;
import eapli.framework.domain.repositories.DomainRepository;

public interface RowAislesRepository extends DomainRepository<Integer, Rows> {
}
