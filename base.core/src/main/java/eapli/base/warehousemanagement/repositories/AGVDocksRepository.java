package eapli.base.warehousemanagement.repositories;

import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.framework.domain.repositories.DomainRepository;

public interface AGVDocksRepository extends DomainRepository<String, AGVDocks> {

    Iterable<AGVDocks> findAllAGVnull();


}
