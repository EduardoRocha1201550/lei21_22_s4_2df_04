package eapli.base.warehousemanagement.repositories;

import eapli.base.warehousemanagement.domain.Aisles;
import eapli.framework.domain.repositories.DomainRepository;

public interface AislesRepository extends DomainRepository<Integer, Aisles> {
}
