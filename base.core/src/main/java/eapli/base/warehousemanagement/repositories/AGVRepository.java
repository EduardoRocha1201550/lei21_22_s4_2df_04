package eapli.base.warehousemanagement.repositories;

import eapli.base.generalmanagement.DTO.AGVDTO;
import eapli.base.generalmanagement.domain.AGV;

import eapli.framework.domain.repositories.DomainRepository;

import java.sql.SQLException;
import java.util.List;


public interface AGVRepository  extends DomainRepository<String, AGV> {

    AGV findByiid(String id);
    List<AGV> findAvailable(double volume,double weight);
    List<AGV> findAvailable1();
    List<AGVDTO> findAll1() throws SQLException;
}
