package eapli.base.warehousemanagement.repositories;

import eapli.base.warehousemanagement.application.Warehouse;
import eapli.framework.domain.repositories.DomainRepository;

public interface WarehouseRepository extends DomainRepository<Long, Warehouse> {
}
