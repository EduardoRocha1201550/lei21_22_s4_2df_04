package eapli.base.warehousemanagement.repositories;

import eapli.base.warehousemanagement.application.WarehousePlant;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.AGVAll.IDagvDocks;
import eapli.framework.domain.repositories.DomainRepository;


public interface WarehousePlantRepository extends DomainRepository<String, WarehousePlant> {


    AGVDocks findByAGVDock (IDagvDocks id);

}

