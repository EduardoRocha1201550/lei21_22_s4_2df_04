package eapli.base.warehousemanagement.repositories;


import eapli.base.warehousemanagement.domain.PositionAGV;
import eapli.framework.domain.repositories.DomainRepository;

public interface PositionAGVRepository extends DomainRepository<Integer, PositionAGV> {
}
