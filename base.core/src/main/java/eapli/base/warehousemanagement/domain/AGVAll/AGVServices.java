package eapli.base.warehousemanagement.domain.AGVAll;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.Id;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.base.warehousemanagement.repositories.WarehousePlantRepository;
import eapli.framework.application.ApplicationService;

@ApplicationService
public class AGVServices {
    private final AGVRepository repository = PersistenceContext.repositories().agv();
    private final WarehousePlantRepository repositoryWare = PersistenceContext.repositories().warehousePlantRepository();

    /**
     * Find agvid agv.
     *
     * @param id the id
     * @return the agv
     */
    public AGV findByiid(String id) {
        return repository.findByiid(id);
    }

    /**
     * Find agv dock agv docks.
     *
     * @param id the id
     * @return the agv docks
     */
    public AGVDocks findAGVDock(IDagvDocks id) {
        return repositoryWare.findByAGVDock(id);
    }

//    public AGV findWeightAGV(MaxWeight maxWeight){
//        return repository.findByWeightAGV(maxWeight);
//    }
}


