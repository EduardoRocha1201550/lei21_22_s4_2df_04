package eapli.base.warehousemanagement.domain.AGVAll;

import eapli.base.generalmanagement.domain.AGV;
import eapli.framework.domain.model.AggregateRoot;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.ArrayList;

@Entity
@Table(name="agvDocks")
public class AGVDocks implements AggregateRoot<String> {
    @EmbeddedId
    private IDagvDocks id;
    @Embedded
    private Begin begin;
    @Embedded
    private End end;
    @Embedded
    private Depth depth;
    @Embedded
    private Accessibility accessibility;
    @OneToOne
    AGV agv;



    public AGVDocks(IDagvDocks id, ArrayList<Long> beginEndDepth, Accessibility accessibility){
        this.id=id;
        this.begin=new Begin(Math.toIntExact(beginEndDepth.get(0)), Math.toIntExact(beginEndDepth.get(1)));
        this.end= new End(Math.toIntExact(beginEndDepth.get(2)), Math.toIntExact(beginEndDepth.get(3)));
        this.depth=new Depth(Math.toIntExact(beginEndDepth.get(4)), Math.toIntExact(beginEndDepth.get(5)));
        this.accessibility=accessibility;
    }
    public AGVDocks(IDagvDocks id, ArrayList<Long> beginEndDepth, Accessibility accessibility,AGV agv){
        this.id=id;
        this.begin=new Begin(Math.toIntExact(beginEndDepth.get(0)), Math.toIntExact(beginEndDepth.get(1)));
        this.end= new End(Math.toIntExact(beginEndDepth.get(2)), Math.toIntExact(beginEndDepth.get(3)));
        this.depth=new Depth(Math.toIntExact(beginEndDepth.get(4)), Math.toIntExact(beginEndDepth.get(5)));
        this.accessibility=accessibility;
    }

    public AGVDocks() {

    }

    @Nullable
    public IDagvDocks getId() {
        return id;
    }


    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public String identity() {
        return null;
    }

    public Begin getBegin() {
        return begin;
    }

    public End getEnd() {
        return end;
    }

    public Depth getDepth() {
        return depth;
    }

    @Override
    public String toString() {
        return "AGVDocks{" +
                "id=" + id +
                ", begin=" + begin +
                ", end=" + end +
                ", depth=" + depth +
                ", accessibility=" + accessibility +
                '}';
    }
}
