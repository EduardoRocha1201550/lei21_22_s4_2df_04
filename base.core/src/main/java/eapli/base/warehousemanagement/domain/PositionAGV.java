package eapli.base.warehousemanagement.domain;

import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;


@Entity
public class PositionAGV implements AggregateRoot<Integer> {
    @Id
    @GeneratedValue
    private Long id;

    int positionx;
    int positiony;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public PositionAGV(int positionx, int positiony) {
        this.positionx = positionx;
        this.positiony = positiony;

    }


    public PositionAGV() {

    }

    public int getPositionx() {
        return positionx;
    }

    public void setPositionx(int positionx) {
        this.positionx = positionx;
    }

    public void setPositiony(int positiony) {
        this.positiony = positiony;
    }

    public int getPositiony() {
        return positiony;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Integer identity() {
        return null;
    }
}
