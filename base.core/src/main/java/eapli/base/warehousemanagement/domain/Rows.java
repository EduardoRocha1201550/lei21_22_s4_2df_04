package eapli.base.warehousemanagement.domain;

import eapli.base.warehousemanagement.domain.AGVAll.Begin;
import eapli.base.warehousemanagement.domain.AGVAll.End;
import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.ArrayList;

/**
 * The type Rows.
 */
@Entity
public class Rows implements AggregateRoot<Integer> {

    @EmbeddedId
    private RowsIDs id;


    @Embedded
    private Begin begin;


    @Embedded
    private End end;


    private int shelves;


    /**
     * Instantiates a new Rows.
     */
    public Rows(){

    }

    /**
     * Instantiates a new Rows.
     *
     * @param rowsIDs  the rows i ds
     * @param beginEnd the begin end
     * @param shelves  the shelves
     */
    public Rows(RowsIDs rowsIDs, ArrayList<Long> beginEnd, int shelves){
        this.id = rowsIDs;
        this.begin = new  Begin(Math.toIntExact(beginEnd.get(0)), Math.toIntExact(beginEnd.get(1)));;
        this.end = new End(Math.toIntExact(beginEnd.get(2)), Math.toIntExact(beginEnd.get(3)));
        this.shelves = shelves;
    }

    @Override
    public String toString() {
        return "Row{" +
                "id=" + id +
                ", begin=" + begin +
                ", end=" + end +
                ", shelves=" + shelves +
                '}';
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public int compareTo(Integer other) {
        return this.id.compareTo(other);
    }

    @Override
    public Integer identity() {
        return null;
    }
}
