package eapli.base.warehousemanagement.DTO;

import eapli.base.warehousemanagement.domain.Rows;


import java.util.List;

public class AisleDTO {
    private String begin;
    private String end;
    private String depth;

    public String getBegin() {
        return begin;
    }

    public String getEnd() {
        return end;
    }

    public String getDepth() {
        return depth;
    }

    public String getAccessibility() {
        return accessibility;
    }

    public int getId1() {
        return Id1;
    }

    public List<Rows> getRowList() {
        return rowList;
    }

    private String accessibility;
    private int Id1;

    public AisleDTO(int id1,String begin, String end, String depth, String accessibility, List<Rows> rowList) {
        this.begin = begin;
        this.end = end;
        this.depth = depth;
        this.accessibility = accessibility;
        Id1 = id1;
        this.rowList = rowList;
    }

    private List<Rows> rowList;
}
