package eapli.base.warehousemanagement.DTO;

import eapli.base.generalmanagement.domain.AGV;

public class AGVDocksDTO {
    private String agvDock_id;
    private String begin;
    private String end;
    private String depth;
    private String acessibility;
    private AGV agv;

    public AGV getAgv() {
        return agv;
    }

    public AGVDocksDTO() {

    }

    public String getAgvDock_id() {
        return agvDock_id;
    }

    public String getBegin() {
        return begin;
    }

    public String getEnd() {
        return end;
    }

    public String getDepth() {
        return depth;
    }

    public String getAcessibility() {
        return acessibility;
    }

    public AGVDocksDTO(String agvDock_id, String begin, String end, String depth, String acessibility, AGV agv) {
        this.agvDock_id = agvDock_id;
        this.begin = begin;
        this.end = end;
        this.depth = depth;
        this.acessibility = acessibility;
        this.agv=agv;
    }
}
