package eapli.base.warehousemanagement.DTO;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.Aisles;

import java.util.List;

public class WarehouseDTO {
    private String name;
    private int length;
    private int width;
    private int square;

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public int getSquare() {
        return square;
    }

    public String getUnit() {
        return unit;
    }

    public Long getId() {
        return id;
    }

    public List<Aisles> getAislesList() {
        return aislesList;
    }



    public List<AGVDocks> getAgvDocksList() {
        return agvDocksList;
    }

    private String unit;
    private Long id;
    private List<Aisles> aislesList;

    public WarehouseDTO(Long id,String name, int length, int width, int square, String unit, List<Aisles> aislesList, List<AGVDocks> agvDocksList) {
        this.id = id;
        this.name = name;
        this.length = length;
        this.width = width;
        this.square = square;
        this.unit = unit;
        this.aislesList = aislesList;
        this.agvList = agvList;
        this.agvDocksList = agvDocksList;
    }

    private List<AGV>agvList;
    private List<AGVDocks>agvDocksList;
}
