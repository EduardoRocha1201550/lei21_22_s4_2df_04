package eapli.base.warehousemanagement.DTO;

import javax.persistence.Transient;

public class RowAisleDTO {
    private int Id;
    private String begin;

    public int getId() {
        return Id;
    }

    public String getBegin() {
        return begin;
    }

    public String getEnd() {
        return end;
    }

    public int getShelves() {
        return shelves;
    }

    public RowAisleDTO(int id, String begin, String end, int shelves) {
        Id = id;
        this.begin = begin;
        this.end = end;
        this.shelves = shelves;
    }

    private String end;
    private int shelves;
}
