package eapli.base.warehousemanagement.application;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.StatusAGV;
import eapli.base.generalmanagement.domain.Task;
import eapli.base.generalmanagement.repositories.TaskRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.warehousemanagement.repositories.AGVRepository;

import java.util.*;
/**
 * Controller of OrderToAGVUI
 */
public class OrderToAGVController {

    public static final String STATEOCCUPIED="occupied";
    public static final String STATEDOING="Doing";
    private AGVRepository agvRepository= PersistenceContext.repositories().agv();
    private TaskRepository repository= PersistenceContext.repositories().tasks();
    private Product_OrderRepository orderRepository=PersistenceContext.repositories().orders();
    /**
     * Changes the Status of the AGV
     */
    public void changeStatusAGV( AGV agv){

        agv.changeStatusTo(AGV.Status.OCCUPIED_DOING_TASK);
        agvRepository.save(agv);

    }
    /**
     * Changes the Status of the Order
     */
    public void changeStatusOrder(Product_Order order) {
        order.setState(new State(STATEDOING));
        orderRepository.save(order);
    }
    /**
     * Finds a list of Order which are with state "To Do"
     */
    public List<Product_Order> findavailableOrder(){
        List<Product_Order>orders=orderRepository.findAvailable();
        Collections.sort(orders, new Comparator<Product_Order>() {
            public int compare(Product_Order o1, Product_Order o2) {
                return o1.getDateTime().compareTo(o2.getDateTime());
            }
        });Collections.reverse(orders);
        return orders;
    }
    /**
     * Finds a list of AGV which are with state "free"
     */
    public List<AGV> findavailableAGV(double weight, double volume){
        List<AGV>agvList=agvRepository.findAvailable(weight,volume);
        Collections.sort(agvList, Comparator.comparingDouble(o -> o.getMaxVolume().getMaxVolume()));
        Collections.reverse(agvList);
        return agvList;
    }
    /**
     * Creates a task and saves it to the Repository
     */
    public Task createTaskandSave(String description,Product_Order order,AGV agv){
        Task task=new Task(description,order,agv);
        return repository.save(task);
    }
}
