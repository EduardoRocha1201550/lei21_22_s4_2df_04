package eapli.base.warehousemanagement.application;


import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.warehousemanagement.repositories.WarehousePlantRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;


public class JSONController{
    private final WarehousePlantRepository repository = PersistenceContext.repositories().warehousePlantRepository();

    /**
     * Create plant.
     *
     * @param filename the filename
     * @param version  the version
     */
    public void createPlant(final String filename, boolean version){
        if (!version){
            repository.delete(repository.findAll().iterator().next());
        }
        final JSONStore Importer = new JSONStore();
        ArrayList<Object> listVariablesJson = Importer.OrganizePlant(filename);
        PlantBuilder plantBuilder = new PlantBuilder();
        WarehousePlant warehousePlant=plantBuilder.buildNewPlant(listVariablesJson);
        repository.save(warehousePlant);
    }




    /**
     * Show warehouse plant string.
     *
     * @return the string
     */
    public String showWarehousePlant () {
        Iterable<WarehousePlant> warehouse = repository.findAll();

        WarehousePlant warehousePlant = warehouse.iterator().next();



        return warehousePlant.toString();



    }





}