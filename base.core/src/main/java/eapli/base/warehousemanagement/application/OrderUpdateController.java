package eapli.base.warehousemanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;

import java.util.List;

public class OrderUpdateController {
    public static final String STATEDONE="Done";
    private Product_OrderRepository orderRepository= PersistenceContext.repositories().orders();


    public void changeStatus2(Product_Order order){
        order.setState(new State(STATEDONE));
        orderRepository.save(order);
    }
    public List<Product_Order> findOrderDoing(){
        return orderRepository.findAvailableDoing();
    }
}
