package eapli.base.warehousemanagement.application;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.Aisles;
import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.List;

@Entity

public class Warehouse implements AggregateRoot<Long> {
    private String name;
    private int length;
    private int width;
    private int square;
    private String unit;
    @Id
    private Long id;

    @OneToMany
    private List<Aisles> aislesList;
    @OneToMany

    private List<AGV>agvList;
    @OneToMany
    private List<AGVDocks>agvDocksList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Warehouse(Long id,String name, int length, int width, int square, String unit, List<Aisles> aislesList, List<AGVDocks> agvList) {
        this.id=id;
        this.name = name;
        this.length = length;
        this.width = width;
        this.square = square;
        this.unit = unit;
        this.aislesList = aislesList;
        this.agvDocksList = agvList;
    }

    public Warehouse() {
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Warehouse)) {
            return false;
        }
        Warehouse house = (Warehouse) other;
        if (this == house) {
            return true;
        }
        return identity().equals(house.identity());
    }

    @Override
    public Long identity() {
        return id;
    }
}
