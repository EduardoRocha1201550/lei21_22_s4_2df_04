package eapli.base.warehousemanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.List;

public class OrderDoneController {
    private Product_OrderRepository orderRepository= PersistenceContext.repositories().orders();
    private final AuthorizationService authz= AuthzRegistry.authorizationService();



    public void changeDoneToBeingShipped(Long id) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.SALESCLERK);
        Product_Order productOrder = orderRepository.findOrderById(id);
        if (productOrder != null) {
            productOrder.setState(new State(State.STATESHIPPING));
            orderRepository.save(productOrder);
        } else System.out.println("Id Invalid");
    }
    public List<Product_Order> findOrderDone(){
        return orderRepository.findAvailableDone();
    }
}
