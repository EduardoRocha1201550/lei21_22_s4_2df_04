package eapli.base.warehousemanagement.application;



import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.StatusAGV;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.warehousemanagement.repositories.AGVRepository;

import java.util.List;
import java.util.Optional;

public class GetAgvStatusController  {
    private AGVRepository agvRepository= PersistenceContext.repositories().agv();

    public AGV.Status getAGVStatus(String id){
        AGV agv= agvRepository.findByiid(id);
        AGV.Status stat= agv.getStatus();
        return stat;

    }

    public AGV changeAGVStatuss(String id){
        AGV agv= agvRepository.findByiid(id);
        if (agv.getStatus().equals(AGV.Status.FREE)){
            agv.changeStatusTo(AGV.Status.OCCUPIED_DOING_TASK);
        }else{
            agv.changeStatusTo(AGV.Status.FREE);
        }
        return agvRepository.save(agv);
    }
    public List<AGV> getAvailableAGV(){
        return agvRepository.findAvailable1();
    }
}



