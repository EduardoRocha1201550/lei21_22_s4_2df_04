package eapli.base.costumer.application;

import eapli.base.LPROG.Rules;
import eapli.base.costumer.domain.*;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;

import eapli.base.usermanagement.application.AddUserController;
import eapli.framework.infrastructure.authz.domain.model.Role;

import java.util.List;
import java.util.Set;


public class CostumerController {
    private final CostumerRepository costumerRepository = PersistenceContext.repositories().costumer();
    private AddUserController userController=new AddUserController();


    public void becomeSystemUser(Costumer costumer, String username, String password, String firstname, String lastname, String email, Set<Role> roles ){
        costumer.becameSystemUser(userController.addUser(username,password,firstname,lastname,email,roles));
    }

    public Costumer registerCostumerOptionals(Name name, VateId vatId, PhoneNumber phoneNumber, Email email, BirthDate birthDate, Address address, Gender gender, String username, String password, List<Rules> rules,Set<Role> roless ) {
        Costumer costumer=new Costumer(name, vatId,phoneNumber,email,birthDate,address,gender,rules);
        Costumer verif= costumerRepository.save(costumer);
        becomeSystemUser(costumer,username,password,name.getName(),name.getName(), email.getEmail(),roless);
        return verif;
    }

    public Costumer registerCostumer(Name name, VateId vatId, PhoneNumber phoneNumber,Email email,String username, String password,List<Rules> rules,Set<Role> roless) {
        Costumer costumer=new Costumer( name, vatId,phoneNumber,email,rules);
        becomeSystemUser(costumer,username,password,name.getName(),name.getName(), email.getEmail(),roless);
        return costumerRepository.save(costumer);
    }

    public Iterable<Costumer> all() {
        return this.costumerRepository.findAll();
    }

    public Costumer findbyID(int id){
        return costumerRepository.findbyID(id);
    }

    public Costumer findByEmail(Email email){
        return costumerRepository.findByEmail(email);
    }


}
