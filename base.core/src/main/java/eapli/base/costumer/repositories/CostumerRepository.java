package eapli.base.costumer.repositories;


import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.domain.Email;
import eapli.framework.domain.repositories.DomainRepository;

public interface CostumerRepository extends DomainRepository<Integer, Costumer> {
    Costumer findbyID(int id);
    Costumer findByEmail(Email email);
}