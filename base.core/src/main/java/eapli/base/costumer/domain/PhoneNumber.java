package eapli.base.costumer.domain;


import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Embeddable
public class PhoneNumber implements ValueObject {
    private String phoneNumber;



    public PhoneNumber(String phoneNumber){
        isValidMobileNo(phoneNumber);
        this.phoneNumber=phoneNumber;
    }

    public PhoneNumber() {

    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void  isValidMobileNo(String str) {
        Pattern ptrn = Pattern.compile("^\\+(?:[0-9]●?){6,14}[0-9]$");
        Matcher match = ptrn.matcher(str);
        if(!(match.find() | match.group().equals(str))){
            throw new IllegalArgumentException("Invalid phone number");
        }
    }

    @Override
    public String toString() {
        return "" +
                "phoneNumber='" + phoneNumber + '\'' +
                "";
    }
}
