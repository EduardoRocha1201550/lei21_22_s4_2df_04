package eapli.base.costumer.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Gender implements ValueObject {
    private String gender;

    public Gender(String gender){
        this.gender=gender;
    }

    @Override
    public String toString() {
        return "" +
                "gender='" + gender + '\'' +
                "";
    }

    public Gender() {

    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }
}
