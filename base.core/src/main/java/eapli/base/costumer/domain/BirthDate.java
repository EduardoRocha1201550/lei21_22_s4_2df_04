package eapli.base.costumer.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Embeddable
public class BirthDate implements ValueObject {

    @Temporal(TemporalType.DATE)
    private Date birthDate;

    public BirthDate(String birthDate) throws ParseException {
        isValid(birthDate);
    }

    public BirthDate() {

    }

    @Override
    public String toString() {
        return "" +
                "birthDate=" + birthDate +
                "";
    }

    public void isValid(String birthdate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setLenient(false);
        try {
            this.birthDate = formatter.parse(birthdate);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid date. Should be like: dd-MM-yyyy");
        }
    }
}
