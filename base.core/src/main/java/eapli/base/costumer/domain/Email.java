package eapli.base.costumer.domain;


import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.util.regex.Pattern;

@Embeddable
public class Email implements ValueObject {
    private String email;

    public Email(String email){
        isValidEmail(email);
        this.email=email;
    }

    public Email() {

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "" +
                "email='" + email + '\'' +
                "";
    }

    public void isValidEmail(String email){
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);

        if(!pat.matcher(email).matches()){
            throw new IllegalArgumentException("Invalid Email");
        }
    }

}
