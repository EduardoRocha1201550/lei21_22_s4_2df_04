package eapli.base.costumer.domain;

import eapli.framework.domain.model.ValueObject;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Embeddable
public class Name implements ValueObject {
    private String name;


    public Name(String name){
        isValidUsername(name);
        this.name=name;
    }

    public Name() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "" +
                "name='" + name + '\'' +
                "";
    }

    public void isValidUsername(String name){

    }
}
