package eapli.base.costumer.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Address implements ValueObject {
    private String address;

    public Address(String address){
        this.address=address;
    }

    @Override
    public String toString() {
        return "" +
                "address='" + address + '\'' +
                "";
    }

    public Address() {

    }
}
