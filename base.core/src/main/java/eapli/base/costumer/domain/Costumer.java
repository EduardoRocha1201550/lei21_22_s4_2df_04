package eapli.base.costumer.domain;

import eapli.base.LPROG.Rules;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Costumer implements AggregateRoot<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Embedded
    private Name name;

    @Embedded
    private VateId vateId;

    @Embedded
    private PhoneNumber phoneNumber;

    @Embedded
    private Email email;

    @Temporal(TemporalType.DATE)
    @Embedded
    private BirthDate birthDate;

    @Embedded
    private Address address;

    @Embedded
    private Gender gender;


    @OneToMany(cascade=CascadeType.PERSIST)

    private List<Rules> rules=new ArrayList<Rules>();
    @OneToOne
    private SystemUser systemUser;

    public Gender getGender() {
        return gender;
    }

    public Address getAddress() {
        return address;
    }

    public BirthDate getBirthDate() {
        return birthDate;
    }

    public Email getEmail() {
        return email;
    }

    public List<Rules> getRules() {
        return rules;
    }

       public void addRule(Rules rule,List<Rules>rulesList){
        rulesList.add(rule);
        setRules(rulesList);
       }

    public void setRules(List<Rules> rules) {
        this.rules = rules;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public VateId getVateId() {
        return vateId;
    }

    public Name getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Costumer(Name name, VateId vateId, PhoneNumber phoneNumber, Email email, BirthDate birthDate, Address address, Gender gender,List<Rules> rules){
        this.name=name;
        this.vateId=vateId;
        this.phoneNumber=phoneNumber;
        this.email=email;
        this.birthDate=birthDate;
        this.address=address;
        this.gender=gender;
        this.rules=rules;
    }

    public Costumer(Name name, VateId vateId, PhoneNumber phoneNumber,Email email,List<Rules> rules){
        this.name=name;
        this.vateId=vateId;
        this.phoneNumber=phoneNumber;
        this.email=email;
        this.rules=rules;
    }

    public Costumer() {

    }

    public void becameSystemUser(SystemUser sysUser){
        this.systemUser = sysUser;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Costumer)) {
            return false;
        }

        final Costumer that = (Costumer) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity());
    }

    @Override
    public String toString() {
        return "Costumer{" +
                "id=" + id +
                ", " + name.toString() +
                ", " + vateId.toString() +
                ", " + phoneNumber.toString() +
                ", " + email.toString() +
                ", " + birthDate.toString() +
                ", " + address.toString() +
                ", " + gender.toString() +
                '}';
    }

    @Override
    public Integer identity()
    {
        return id;
    }
}