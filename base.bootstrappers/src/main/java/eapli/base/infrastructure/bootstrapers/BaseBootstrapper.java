/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.infrastructure.bootstrapers;

import eapli.base.LPROG.Content;
import eapli.base.LPROG.Rules;
import eapli.base.LPROG.TypeRule;
import eapli.base.costumer.application.CostumerController;
import eapli.base.costumer.domain.*;
import eapli.base.costumer.repositories.CostumerRepository;
import eapli.base.generalmanagement.domain.*;
import eapli.base.generalmanagement.repositories.RulesRepository;
import eapli.base.questionnaire.domain.Question;
import eapli.base.questionnaire.domain.Questionnaire;
import eapli.base.questionnaire.domain.Section;
import eapli.base.questionnaire.repositories.QuestionRepository;
import eapli.base.questionnaire.repositories.QuestionnaireRepository;
import eapli.base.questionnaire.repositories.SectionRepository;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.salesmanagement.repositories.ShoppingCartRepository;
import eapli.base.warehousemanagement.application.JSONController;
import eapli.base.warehousemanagement.domain.AGVAll.AGVDocks;
import eapli.base.warehousemanagement.domain.AGVAll.Accessibility;
import eapli.base.warehousemanagement.domain.AGVAll.IDagvDocks;
import eapli.base.warehousemanagement.domain.PositionAGV;
import eapli.base.warehousemanagement.repositories.AGVDocksRepository;
import eapli.base.warehousemanagement.repositories.AGVRepository;
import eapli.base.generalmanagement.repositories.CategoryRepository;
import eapli.base.generalmanagement.repositories.ProductRepository;
import eapli.base.product.*;
import eapli.base.product.Volume;
import eapli.base.product.Weight;
import eapli.base.salesmanagement.domain.*;
import eapli.base.usermanagement.domain.BasePasswordPolicy;
import eapli.base.warehousemanagement.repositories.PositionAGVRepository;
import eapli.framework.general.domain.model.Money;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.infrastructure.authz.domain.model.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.base.usermanagement.domain.UserBuilderHelper;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.infrastructure.authz.domain.model.SystemUserBuilder;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.strings.util.Strings;
import eapli.framework.validations.Invariants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Base Bootstrapping data app
 *
 * @author Paulo Gandra de Sousa
 */
@SuppressWarnings("squid:S106")
public class BaseBootstrapper implements Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(
            BaseBootstrapper.class);

    private static final String POWERUSER_A1 = "poweruserA1";
    private static final String POWERUSER = "poweruser";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final AuthenticationService authenticationService = AuthzRegistry.authenticationService();
    private final UserRepository userRepository = PersistenceContext.repositories().users();
    private final AGVDocksRepository agvDocksRepository=PersistenceContext.repositories().agvDocks();
    private final AGVRepository agvRepository=PersistenceContext.repositories().agv();
    private final CostumerRepository costumerRepository= PersistenceContext.repositories().costumer();
    private final CategoryRepository categoryRepository=PersistenceContext.repositories().categories();
    private final ProductRepository productRepository=PersistenceContext.repositories().products();
    private final Product_OrderRepository orderRepository=PersistenceContext.repositories().orders();
    private final ShoppingCartRepository shoppingCartRepository=PersistenceContext.repositories().shoppingCart();
    private final QuestionnaireRepository questionnaireRepository=PersistenceContext.repositories().questionnaireRepository();
    private final SectionRepository sectionRepository=PersistenceContext.repositories().sectionRepository();
    private final QuestionRepository questionRepository=PersistenceContext.repositories().questionRepository();
    private final PositionAGVRepository positionAGVRepository=PersistenceContext.repositories().positionAGV();
    private final RulesRepository rulesRepository= PersistenceContext.repositories().rulesRepository();
    public static void main(final String[] args) {

        AuthzRegistry.configure(PersistenceContext.repositories().users(),new BasePasswordPolicy(),
                new PlainTextEncoder());

        new BaseBootstrapper().execute();
    }
    @Override
    public boolean execute() {
        // declare bootstrap actions
        final Action[] actions = { new MasterUsersBootstrapper(), };
        CostumerController controller=new CostumerController();
        registerPowerUser();
        registerUser("bernafilipe","Dark1berna","Adelaide","Silva","Adelaidefilipe@gmail.com",BaseRoles.SALESCLERK);
        registerUser("berna","Dark1berna","Adelaide","Silva","Adelaidefilipe@gmail.com",BaseRoles.WAREHOUSEMANAGER);
        JSONController controlleryau = new JSONController();
        controlleryau.createPlant("warehouse1",true);
        Costumer costumer = null;

        Name name=new Name("Eduardo");
        VateId vateid=new VateId("PT999999999");
        PhoneNumber phoneNumber=new PhoneNumber("+351910807792");
        Email email=new Email("edurocha@isep.pt");
        Address address= new Address("Rua Vale de Cambra n65 3730");
        Gender gender=new Gender("Masculino");
        String username="Eduardo";
        String pass="Dark1berna";
        List<Rules> rules= new LinkedList<>();Set<Role> role=new HashSet<>();
        role.add(BaseRoles.CLIENT_USER);

        try {
            BirthDate birthDate=new BirthDate("12-10-2000");
            controller.registerCostumerOptionals(name,vateid, phoneNumber,
                    email, birthDate, address, gender,
                    username,pass,rules, role);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        registerUser("Joao","Dark1berna","Edu","Silva","nela@gmail.com",BaseRoles.SALESMANAGER);
        //AGVDocks agvDocks=new AGVDocks("10","begin","end","depth","l+",null);
        //AGVDocks agvDocks1=new AGVDocks("11","begin","end","depth","l+",null);

        ///agvDocksRepository.save(agvDocks);
        //agvDocksRepository.save(agvDocks1);

        //Category
        Category category=new Category(new Code("awdawdwa"),new Description("wadawdawdawdawdawawdawdaw"));
        categoryRepository.save(category);

        //Product
        Product product=new Product(new Internal_code("www"),new Product_code("asd321"),new Short_description("wwwwwww"),new Extended_description("wwwwwwwwwwwwwwwwwwwwwww"),new Technical_description("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"),new Reference("123123123123"), Money.euros(12.4),Money.euros(15.0),category,new Brand("aaaa"),true,new Volume(1.0,1.0,1.0),new Weight(10.0),new Barcode(123123123123L,Product.TYPE.UPC),new Product_Location(1,1,1,1),new Photo("DSC_7473.jpg"));
        Product product1=new Product(new Internal_code("wwf"),new Product_code("76sfd"),new Short_description("descricao curt"),new Extended_description("descricao extensaaaaaa"),new Technical_description("descricaooo tecnica"),new Reference("123123128123"), Money.euros(29.4),Money.euros(19.0),category,new Brand("adidas"),true,new Volume(1.0,1.0,1.0),new Weight(10.0),new Barcode(123123123923L,Product.TYPE.UPC),new Product_Location(2,1,1,1),new Photo("DSC_7473.jpg"));
        productRepository.save(product);
        productRepository.save(product1);
        //Shipping Method
        Shipping_Method shippingMethod=new Shipping_Method(new ShippingName("CTT"),new ShipPrice(3.5));
        OrderItem orderItem=new OrderItem(new Quantity(1),productRepository.findbyID(new Internal_code("www")));
        List<OrderItem> orderItemList=new ArrayList<>();
        orderItemList.add(orderItem);
        Date date=new Date();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date date1= null;
        try {
            date1 = simpleDateFormat.parse("2021-02-04");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Product product2=new Product(new Internal_code("ww1"),new Product_code("esdsdf"),new Short_description("Lapis"),new Extended_description("wwwwwwwwqwwwwwwwwwwwwwww"),new Technical_description("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"),new Reference("123123123123"), Money.euros(12.4),Money.euros(15.0),category,new Brand("aaaa"),true,new Volume(1.0,1.0,1.0),new Weight(10.0),new Barcode(123123123123L,Product.TYPE.UPC),new Product_Location(1,1,1,1),new Photo("DSC_7473.jpg"));
        Product product3=new Product(new Internal_code("ww2"),new Product_code("74sfd"),new Short_description("Lapis curto"),new Extended_description("desqcricao extensaaaaaa"),new Technical_description("descricaooo tecnica"),new Reference("123123128123"), Money.euros(29.4),Money.euros(19.0),category,new Brand("adidas"),true,new Volume(1.0,1.0,1.0),new Weight(10.0),new Barcode(123123123923L,Product.TYPE.UPC),new Product_Location(2,1,1,1),new Photo("DSC_7473.jpg"));
        productRepository.save(product2);
        productRepository.save(product3);


        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        arrayList.add(3L);
        arrayList.add(1L);
        arrayList.add(3L);
        arrayList.add(1L);
        arrayList.add(3L);

     /*   ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(7L);
        arrayList.add(3L);
        arrayList.add(1L);
        arrayList.add(3L);
        arrayList.add(3L);
        arrayList.add(3L);
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(1L);
        arrayList.add(3L);
        arrayList.add(1L);
        arrayList.add(9L);
        arrayList.add(1L);
        arrayList.add(1L);
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(9L);
        arrayList.add(3L);
        arrayList.add(1L);
        arrayList.add(3L);
        arrayList.add(1L);
        arrayList.add(8L);*/
        AGVDocks iDagvDocks1 =  new AGVDocks(new IDagvDocks("D1"),arrayList, new Accessibility("l+"));
        AGVDocks iDagvDocks2 =  new AGVDocks(new IDagvDocks("D2"),arrayList, new Accessibility("l+"));
        AGVDocks iDagvDocks3 =  new AGVDocks(new IDagvDocks("D3"),arrayList, new Accessibility("l+"));
        AGVDocks iDagvDocks4 =  new AGVDocks(new IDagvDocks("D4"),arrayList, new Accessibility("l+"));
        PositionAGV positionAGV=new PositionAGV(1,2);
        positionAGVRepository.save(positionAGV);
        AGV agv1= new AGV("A1",new MaxVolume(54.02),new MaxWeight(54.02),new Model("ze manel dos produtos é o melhor sales clerk"), new Route(1),new ShortDescription("ze manel dos produtos"),AGV.Status.FREE,new Autonomy("5 hours"),iDagvDocks1,positionAGV);
        AGV agv2= new AGV("A2",new MaxVolume(54.02),new MaxWeight(54.02),new Model("ze manel dos produtos é o melhor sales clerk"), new Route(1),new ShortDescription("ze manel dos produtos"),AGV.Status.FREE,new Autonomy("5 hours"),iDagvDocks2,positionAGV);
        AGV agv3= new AGV("A3",new MaxVolume(54.02),new MaxWeight(54.02),new Model("ze manel dos produtos é o melhor sales clerk"), new Route(1),new ShortDescription("ze manel dos produtos"),AGV.Status.FREE,new Autonomy("5 hours"),iDagvDocks3,positionAGV);
        AGV agv4= new AGV("A4",new MaxVolume(54.02),new MaxWeight(54.02),new Model("ze manel dos produtos é o melhor sales clerk"), new Route(1),new ShortDescription("ze manel dos produtos"),AGV.Status.FREE,new Autonomy("5 hours"),iDagvDocks4,positionAGV);

        agvRepository.save(agv1);
        agvRepository.save(agv2);
        agvRepository.save(agv3);
        agvRepository.save(agv4);
        controlleryau.createPlant("warehouse1",true);



        DeliveringAddress deliveringAddress=new DeliveringAddress("1","Aveiro","4590-357");
        eapli.base.salesmanagement.domain.Volume volume=new eapli.base.salesmanagement.domain.Volume(2);
        eapli.base.salesmanagement.domain.Weight weight=new eapli.base.salesmanagement.domain.Weight(2);
        OrderName orderName=new OrderName("nome");
        Payment_Confirmation method1=new Payment_Confirmation("123131231231");
        State state=new State(State.STATETODO);
        OrderPrice orderPrice=new OrderPrice(2);
        Payment_Method method=new Payment_Method(orderName,method1);
        Costumer costumer1=costumerRepository.findbyID(7);
        Product_Order order=new Product_Order(date,deliveringAddress,volume,weight,costumer1,shippingMethod,method,state,orderPrice,orderItemList);
        orderRepository.save(order);
        eapli.base.salesmanagement.domain.Volume volume1=new eapli.base.salesmanagement.domain.Volume(1);
        eapli.base.salesmanagement.domain.Weight weight1=new eapli.base.salesmanagement.domain.Weight(2);
        Product_Order order1=new Product_Order(date1,deliveringAddress,volume1,weight1,costumer1,shippingMethod,method,state,orderPrice,orderItemList);
        orderRepository.save(order1);


        authenticateForBootstrapping();

        //AGV agv=new AGV("6",new MaxVolume(3),new MaxWeight(3),new Model("model"),new Route(1),new ShortDescription("short Description"),new StatusAGV(StatusAGV.STATEFREE),new Autonomy("2h"),agvDocks,new PositionAGV(1,2));
        //agvDocks.setAgv(agv);
        TypeRule type= new TypeRule(1);
        TypeRule type2= new TypeRule(2);
        Content content1=new Content("adidas");
        Content content2= new Content("3730");
        Rules rule1=new Rules(1,type,content1,true);
        Rules rule2= new Rules(2,type2,content2,true);
        List<Rules> ruless=new LinkedList<>();
        ruless.add(0,rule2);
        rulesRepository.save(rule1);
        rulesRepository.save(rule2);






        OrderItem orderItem1=new OrderItem(new Quantity(1),productRepository.findbyID(new Internal_code("www")));
        OrderItem orderItem2=new OrderItem(new Quantity(1),productRepository.findbyID(new Internal_code("www")));
        List<String>response=new ArrayList<>();
        Question question=new Question(1,"What is your favourite colour?","obligatoriness",1,null);
        questionRepository.save(question);
        List<Question> questionList=new ArrayList<>();
        questionList.add(question);
        List<Section>sections=new ArrayList<>();
        Section section=new Section(1,"title123","sectiom","obligatoriness",1,questionList);
        sectionRepository.save(section);
        sections.add(section);
        Questionnaire questionnaire=new Questionnaire("1","title123","Welcome",sections,"bye");
        questionnaireRepository.save(questionnaire);
        // execute all bootstrapping
        boolean ret = true;
        for (final Action boot : actions) {
            System.out.println("Bootstrapping " + nameOfEntity(boot) + "...");
            ret &= boot.execute();
        }
        return ret;
    }

    /**
     * register a power user directly in the persistence layer as we need to
     * circumvent authorisations in the Application Layer
     */
    private boolean registerPowerUser() {
        final SystemUserBuilder userBuilder = UserBuilderHelper.builder();
        userBuilder.withUsername(POWERUSER).withPassword(POWERUSER_A1).withName("joe", "power")
                .withEmail("joe@email.org").withRoles(BaseRoles.POWER_USER);
        final SystemUser newUser = userBuilder.build();

        SystemUser poweruser;
        try {
            poweruser = userRepository.save(newUser);
            assert poweruser != null;
            return true;
        } catch (ConcurrencyException | IntegrityViolationException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", newUser.username());
            LOGGER.trace("Assuming existing record", e);
            return false;
        }
    }
    private boolean registerUser(String username, String password, String first_name, String last_name, String email, Role roles) {
        final SystemUserBuilder userBuilder = UserBuilderHelper.builder();
        userBuilder.withUsername(username).withPassword(password).withName(first_name, last_name)
                .withEmail(email).withRoles(roles);
        final SystemUser newUser = userBuilder.build();

        SystemUser User;
        try {
            User = userRepository.save(newUser);
            assert User != null;
            return true;
        } catch (ConcurrencyException | IntegrityViolationException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", newUser.username());
            LOGGER.trace("Assuming existing record", e);
            return false;
        }
    }

    /**
     * authenticate a super user to be able to register new users
     *
     */
    protected void authenticateForBootstrapping() {
        authenticationService.authenticate(POWERUSER, POWERUSER_A1);
        Invariants.ensure(authz.hasSession());
    }

    private String nameOfEntity(final Action boot) {
        final String name = boot.getClass().getSimpleName();
        return Strings.left(name, name.length() - "Bootstrapper".length());
    }
}