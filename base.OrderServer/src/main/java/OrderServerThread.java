import eapli.base.costumer.domain.Costumer;
import eapli.base.product.Internal_code;
import eapli.base.product.Product;
import eapli.base.salesmanagement.application.ShoppingCartController;
import eapli.base.salesmanagement.domain.Product_Order;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;

public class OrderServerThread implements Runnable {
    public static final int GETORDERSFROMCOSTUMER =3;
    public static final int GETSHOPPINGCARTEXISTANCE =4;
    public static final int GETCOSTUMERBYEMAIL=5;
    public static final int GETPRODUCTBYCODE=6;
    public static final int VIEWSURVEYS=7;


    private Socket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private CodingAndDecoding cod= new CodingAndDecoding();
    private final OrderServerController controller= new OrderServerController();
    private ShoppingCartController shoppingCartController=new ShoppingCartController();



    public OrderServerThread(Socket cli_s) { s=cli_s;}
    public void run() {

        InetAddress clientIP;
        clientIP = s.getInetAddress();
        System.out.println("Nova ligaçao de cliente com este ip: " + clientIP.getHostAddress() +
                ", Numero de porta: " + s.getPort());

        try{
            sIn= new DataInputStream(s.getInputStream());
            sOut=new DataOutputStream(s.getOutputStream());
            MessageDTO message= cod.Decoding(sIn);

            if(message.getCode()== GETORDERSFROMCOSTUMER){
                ByteArrayInputStream in = new ByteArrayInputStream(message.getData_message());
                ObjectInputStream is = new ObjectInputStream(in);
                String email= (String) is.readObject();

                Costumer costumer=shoppingCartController.getCostumerEmail(email);
                int id=costumer.getId();

                List<Product_Order> list= controller.getOrdersFromCostumer(id);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oOut= new ObjectOutputStream(bos);
                oOut.writeObject(list);
                oOut.flush();
                byte[] data = bos.toByteArray();

                DataOutputStream out =new DataOutputStream(s.getOutputStream());
                byte[] ResponseMessage = new byte[data.length+4];
                ResponseMessage[0]=1;
                ResponseMessage[1]=3;
                int size=data.length;
                int size1=size%256;
                int size2=size/256;
                ResponseMessage[2]= (byte) size1;
                ResponseMessage[3]= (byte) size2;
                for(int i=0;i<data.length;i++){
                    ResponseMessage[i+4]=data[i];
                }
                out.write(ResponseMessage);
                out.close();
                oOut.close();
            }
            if(message.getCode()== GETSHOPPINGCARTEXISTANCE){
                ByteArrayInputStream in = new ByteArrayInputStream(message.getData_message());
                ObjectInputStream is = new ObjectInputStream(in);
                String email= (String) is.readObject();

                boolean verif = shoppingCartController.shoppingCartExistance(email);
                Thread.sleep(300);
                DataOutputStream out =new DataOutputStream(s.getOutputStream());
                byte[] ResponseMessage = new byte[5];
                ResponseMessage[0]=1;
                ResponseMessage[1]=5;
                int size1=1;
                int size2=0;
                ResponseMessage[2]= (byte) size1;
                ResponseMessage[3]= (byte) size2;
                if (verif){
                    ResponseMessage[4]=1;
                }else ResponseMessage[4]=0;
                out.write(ResponseMessage);
                out.close();
            }
            if(message.getCode()== GETCOSTUMERBYEMAIL){
                ByteArrayInputStream in = new ByteArrayInputStream(message.getData_message());
                ObjectInputStream is = new ObjectInputStream(in);
                String email= (String) is.readObject();

                Costumer costumer=shoppingCartController.getCostumerEmail(email);
                Thread.sleep(300);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oOut= new ObjectOutputStream(bos);
                oOut.writeObject(costumer);
                oOut.flush();
                byte[] data = bos.toByteArray();

                DataOutputStream out =new DataOutputStream(s.getOutputStream());
                byte[] ResponseMessage = new byte[data.length+4];
                ResponseMessage[0]=1;
                ResponseMessage[1]=5;
                int size=data.length;
                int size1=size%256;
                int size2=size/256;
                ResponseMessage[2]= (byte) size1;
                ResponseMessage[3]= (byte) size2;
                for(int i=0;i<data.length;i++){
                    ResponseMessage[i+4]=data[i];
                }
                out.write(ResponseMessage);
                out.close();
                oOut.close();
            }
            if(message.getCode()== GETPRODUCTBYCODE){
                ByteArrayInputStream in = new ByteArrayInputStream(message.getData_message());
                ObjectInputStream is = new ObjectInputStream(in);
                Internal_code code= (Internal_code) is.readObject();

                Product product= shoppingCartController.getProduct(code);
                Thread.sleep(300);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oOut= new ObjectOutputStream(bos);
                oOut.writeObject(product);
                oOut.flush();
                byte[] data = bos.toByteArray();

                DataOutputStream out =new DataOutputStream(s.getOutputStream());
                byte[] ResponseMessage = new byte[data.length+4];
                ResponseMessage[0]=1;
                ResponseMessage[1]=5;
                int size=data.length;
                int size1=size%256;
                int size2=size/256;
                ResponseMessage[2]= (byte) size1;
                ResponseMessage[3]= (byte) size2;
                for(int i=0;i<data.length;i++){
                    ResponseMessage[i+4]=data[i];
                }
                out.write(ResponseMessage);
                out.close();
                oOut.close();
            }
            if(message.getCode()==VIEWSURVEYS){
                ByteArrayInputStream in = new ByteArrayInputStream(message.getData_message());
                ObjectInputStream is = new ObjectInputStream(in);
                int code= (int) is.readObject();

                //surveyController.ShowMySurveys(code);
                Thread.sleep(300);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oOut= new ObjectOutputStream(bos);
               // oOut.writeObject(product);
                //oOut.flush();
                byte[] data = bos.toByteArray();

                DataOutputStream out =new DataOutputStream(s.getOutputStream());
                byte[] ResponseMessage = new byte[data.length+4];
                ResponseMessage[0]=1;
                ResponseMessage[1]=5;
                int size=data.length;
                int size1=size%256;
                int size2=size/256;
                ResponseMessage[2]= (byte) size1;
                ResponseMessage[3]= (byte) size2;
                for(int i=0;i<data.length;i++){
                    ResponseMessage[i+4]=data[i];
                }
                out.write(ResponseMessage);
                out.close();
                oOut.close();
            }
        } catch (IOException | ClassNotFoundException | InterruptedException | SQLException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
    }
