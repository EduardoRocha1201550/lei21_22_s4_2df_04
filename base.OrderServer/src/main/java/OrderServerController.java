import eapli.base.costumer.application.CostumerController;
import eapli.base.costumer.domain.Costumer;
import eapli.base.costumer.domain.Email;
import eapli.base.generalmanagement.domain.AGV;
import eapli.base.product.Internal_code;
import eapli.base.product.Product;
import eapli.base.salesmanagement.application.Product_OrderController;
import eapli.base.salesmanagement.application.ShoppingCartController;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.warehousemanagement.application.GetAgvStatusController;

import java.sql.SQLException;
import java.util.List;

public class OrderServerController {
    private Product_OrderController controller = new Product_OrderController();
    private CostumerController costumerController = new CostumerController();
    private ShoppingCartController shoppingCartController=new ShoppingCartController();

    public List<Product_Order> getOrdersFromCostumer(int costumerID) throws SQLException, InterruptedException {
        return controller.getOrdersFromCostumer(costumerID);
    }

    public Costumer findCostumerByEmail(Email email){
        return costumerController.findByEmail(email);
    }

    public boolean shoppingCartExistance(String userEmail){
        return shoppingCartController.shoppingCartExistance(userEmail);
    }


    public Product getProduct(Internal_code internal_code){
        return shoppingCartController.getProduct(internal_code);
    }

}
