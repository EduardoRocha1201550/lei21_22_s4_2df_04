US5004 Eduardo Rocha 1201550
=======================================

# 1. Requirements
**US5003**As Project Manager, I want the input communications (of the AGV Digital Twin) made through the SPOMS2022 protocol to be secured/protected.

**Acceptance Criteria** 
- It should be adopted SSL/TLS with mutual authentication based on public key certificates.
- It complements the US5001.

# 2. Analysis

This US has the same approach as the US 5004 so to not repeat myself the documentation is the same as the US5004 (Jaime did the documentation 5004)