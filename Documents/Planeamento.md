# Projeto Integrador da LEI-ISEP 2021-22 - 4º Semestre

# 1. Constituição do Grupo de Trabalho

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	                    | Nome do Aluno			       |
|-------------------------------|------------------------|
| **[1201549](/docs/1201549/)** | Bernardo Silva         |
| **[1201550](/docs/1201550/)** | 	Eduardo Rocha					    |
| **[1201021](/docs/1201021/)** | Jaime Brito						      |
| **[1200882](/docs/1200882/)** | 	Goncalo Carrusca					 |
|                               | 						                 |
|                               | 						                 |
|                               | 						                 |


# 2. Distribuição de Funcionalidades ###

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

| Aluno Nr.	                    | Sprint B               | Sprint C                                     | Sprint D |
|-------------------------------|------------------------|----------------------------------------------|----------|
| [**1201549**](/docs/1201549/) | [US2001](/docs/US2001) |                                              |          |
| [**1201549**](/docs/1201549/) | [US2002](/docs/US2002) |                                              |          |
| [**1200882**](/docs/1200882/) |                        | [US2004](/docs/US2004)[US5001](/docs/US5001) |          |
| 	                             |                        |                                              |          |
| 	                             |                        |                                              |          |
| 	                             |                        |                                              |          |
| 	                             |                        |                                              |          |