**Aluno 1201021 Jaime Brito**
=

### Índice das Funcionalidade Desenvolvidas ###


| Sprint | Funcionalidade      |
|--------|---------------------|
| **B**  | [US1004](Sprint2/US1004/US1004.md)  |
| **B**  | [US1005](Sprint2/US1005/US1005.md)  |
| **C**  | [US1901](Sprint3/US1901/US1901.md)  |
| **C**  | [US4002](Sprint3/US4002/US4002.md)  |
| **D**  | [US1502](Sprint4/US1502/US1502.md)  |
| **D**  | [US5004](Sprint4/US5004/US5004.md)  |