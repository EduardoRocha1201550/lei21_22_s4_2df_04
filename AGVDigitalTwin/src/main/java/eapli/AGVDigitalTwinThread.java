package eapli;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.Description;
import eapli.base.generalmanagement.domain.Task;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class AGVDigitalTwinThread implements Runnable {

    public static final int FORCETASK = 6;

    static Socket sock;

    private static CodingAndDecoding cod = new CodingAndDecoding();
    private Socket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;

    private final AGVDigitalTwinController controller = new AGVDigitalTwinController();
    private Product_OrderRepository orderRepository = PersistenceContext.repositories().orders();

    //private CodingAndDecoding cod = new CodingAndDecoding();
    private int code;
    private int version;
    private int data;

    private byte[] data_message;

    public AGVDigitalTwinThread(Socket cli_s) {
        s = cli_s;
    }

    public void run() {
        InetAddress clientIP;
        clientIP = s.getInetAddress();
        System.out.println("Nova ligaçao de cliente com este ip: " + clientIP.getHostAddress() +
                ", Numero de porta: " + s.getPort());



        try {
            sIn = new DataInputStream(s.getInputStream());
            MessageDTO message = cod.Decoding(sIn);

            if (message.getCode() == FORCETASK) {
                ByteArrayInputStream in = new ByteArrayInputStream(message.getData_message());
                ObjectInputStream is = new ObjectInputStream(in);
                Task taskPerform = (Task) is.readObject();
                Product_Order order = taskPerform.getOrder();
                AGV agv = taskPerform.getAgv();
                boolean verif = controller.doTask(taskPerform);

                DataOutputStream out = new DataOutputStream(s.getOutputStream());
                byte[] ResponseMessage = new byte[5];
                ResponseMessage[0] = 1;
                ResponseMessage[1] = 5;
                ResponseMessage[2] = 1;
                ResponseMessage[3] = 0;
                if (verif) {
                    ResponseMessage[4] = 1;
                } else ResponseMessage[4] = 0;
                out.write(ResponseMessage);


            }


        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
