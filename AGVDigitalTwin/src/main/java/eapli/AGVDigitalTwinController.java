package eapli;

import eapli.base.generalmanagement.domain.AGV;
import eapli.base.generalmanagement.domain.StatusAGV;
import eapli.base.generalmanagement.domain.Task;
import eapli.base.generalmanagement.repositories.TaskRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.salesmanagement.domain.Product_Order;
import eapli.base.salesmanagement.domain.State;
import eapli.base.salesmanagement.repositories.Product_OrderRepository;
import eapli.base.warehousemanagement.application.GetAgvStatusController;
import eapli.base.warehousemanagement.repositories.AGVRepository;

import java.util.List;

public class AGVDigitalTwinController {
    private GetAgvStatusController agvController=new GetAgvStatusController();
    private Product_OrderRepository orderRepository= PersistenceContext.repositories().orders();
    private AGVRepository agvRepository= PersistenceContext.repositories().agv();
    private TaskRepository repository= PersistenceContext.repositories().tasks();

    public static final String STATEOCCUPIED="occupied";
    public static final String STATEDOING="Doing";

    public List<AGV> getAvailableAGV(){
        return agvController.getAvailableAGV();
    }

    public boolean createTaskandSave(String description, Product_Order order, AGV agv){
        Task task=new Task(description,order,agv);
        repository.save(task);
        return true;
    }

    public boolean doTask(Task task) throws InterruptedException {
        Product_Order order= task.getOrder();
        AGV agv = task.getAgv();
        changeAGVStatus(agv.getId());

        changeAGVStatus(agv.getId());
        order.setState(new State(State.STATEDONE));
        //orderRepository.save(order);
        return true;
    }

    public AGV changeAGVStatus(String id){
        AGV agv= agvRepository.findByiid(id);
        if (agv.getStatus().equals(StatusAGV.STATEFREE)){
            agv.changeStatusTo(AGV.Status.OCCUPIED_DOING_TASK);
        }else{
            agv.changeStatusTo(AGV.Status.FREE);
        }
        return agvRepository.save(agv);
    }
}
