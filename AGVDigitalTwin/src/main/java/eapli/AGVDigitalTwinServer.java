package eapli;



import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;

import java.net.Socket;

public class AGVDigitalTwinServer {
    static final String KEYSTORE_PASS="forgotten";


    public static void main(String args[]) throws Exception {
        final int SERVER_PORT=Integer.parseInt(args[0]);
        final String TRUSTED_STORE="/var/www/SSLTwin/server_J.jks";
        SSLServerSocket sock=null;
        Socket cliSock;


        // Trust these certificates provided by authorized clients
        System.setProperty("javax.net.ssl.trustStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key as server certificate
        System.setProperty("javax.net.ssl.keyStore",TRUSTED_STORE);
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        try {
            sock = (SSLServerSocket) sslF.createServerSocket(SERVER_PORT);
            sock.setNeedClientAuth(true);
            System.out.println("Sucess");
        }
        catch(IOException ex) {
            System.out.println("Server failed to open local port " + SERVER_PORT);
            System.exit(1);
        }


        while(true) {
            cliSock=sock.accept();
            new Thread(new AGVDigitalTwinThread(cliSock)).start();
        }
    }
}




