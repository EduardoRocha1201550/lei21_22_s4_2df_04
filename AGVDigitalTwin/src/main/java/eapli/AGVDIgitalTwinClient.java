package eapli;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.warehousemanagement.repositories.AGVRepository;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class AGVDIgitalTwinClient {




    static CodingAndDecoding cod = new CodingAndDecoding();
    static InetAddress serverIP;
    static Socket sock;

    public static void main(String[] args) throws Exception {
        AGVRepository agvRepository = PersistenceContext.repositories().agv();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the option that you want to select 1-Update the agv status");
        int option = sc.nextByte();
        if (option == 1) {
            byte[] message = new byte[5];
            System.out.println("Select the id of the AGV");
            System.out.println(agvRepository.findAll());
            String agv = sc.next();
            changeAGVStatus(agv, option);

        }
        if (args.length != 1) {
            System.out.println("Server IPv4/IPv6 address ou DNS é requerido");
            System.exit(1);
        }
        try {
            serverIP = InetAddress.getByName(args[0]);
        } catch (UnknownHostException ex) {
            System.out.println("Server invalido: " + args[0]);
            System.exit(1);
        }


    }


    public static void changeAGVStatus(String avgId, int messageCode) {
        try {
            try {
                serverIP = InetAddress.getByName("localhost");
            } catch (UnknownHostException ex) {
                System.out.println("Invalid server specified: " + serverIP);
                System.exit(1);
            }
            try {
                sock = new Socket(serverIP, 2223);
            } catch (IOException ex) {
                System.out.println("Failed to establish TCP connection");
                System.exit(1);
            }
            System.out.println("Connected to: " + serverIP + " through port:" + 2223);
            try {
                DataOutputStream sOutData = new DataOutputStream(sock.getOutputStream());
                DataInputStream sInData = new DataInputStream(sock.getInputStream());


                    byte[] message = new byte[5];
                    message = cod.Coding_String(1, 4, avgId);


                    DataOutputStream out = new DataOutputStream(sock.getOutputStream());


                    out.write(message);
                   /* Thread.sleep(100);
                    DataInputStream inputStream = new DataInputStream(sock.getInputStream());
                    MessageDTO messageDTO = cod.Decoding(inputStream);
                    System.out.println("Version: " + messageDTO.getVersion());
                    System.out.println("Code: " + messageDTO.getCode());
                    System.out.println("Size: " + messageDTO.getSize());
                    if (messageDTO.getData_message()[0] == 1) {
                        System.out.println("Success");

                    } else {
                        System.out.println("Not able to change AGV status");
                    }*/



            } catch (IOException e) {
                System.out.println("Failed while trying to send info from the server");
            } finally {
                try {
                    sock.close();
                } catch (IOException e) {
                    System.out.println("Failed while trying to close socket");
                }
            }

        } catch (Exception e) {
            System.out.println("Server is down");
            System.out.println(e.getMessage());
        }
    }

}
