package eapli.base.app.user.console.presentation.Catalog;

import eapli.base.app.user.console.presentation.ClientUserBaseUI;
import eapli.base.generalmanagement.application.CatalogController;
import eapli.base.generalmanagement.domain.Category;
import eapli.base.generalmanagement.domain.Code;
import eapli.base.product.Brand;
import eapli.base.product.Extended_description;
import eapli.base.product.Product;
import eapli.base.product.Short_description;
import eapli.framework.io.util.Console;

import java.util.Scanner;

public class CatalogUI extends ClientUserBaseUI {
private final CatalogController controller= new CatalogController();


    @Override
    protected boolean doShow() {
        Scanner sc= new Scanner(System.in);
        System.out.println("Select one of the options\n 1-See full catalog\n 2-Find by brand \n 3-Find by short description\n 4-Find by description\n 5-Find by category ");
        int option=sc.nextInt();
        if(option==1){

            System.out.println(controller.allProducts().toString());

        }else if(option==2){
            String brand= Console.readLine("Enter the Brand");

            System.out.println(controller.findByBrand(new Brand(brand)).toString());
            //controller.findByBrand(new Brand(brand));
        }else if(option==3) {
            String description = Console.readLine("Enter the description");
            System.out.println(controller.findByShortDescription(new Short_description(description)).toString());
            //controller.findByShortDescription(new Short_description(description));
        }else if(option==4){
            String description = Console.readLine("Enter the description");
            System.out.println(controller.findByDescripition(new Extended_description(description)).toString());
            // controller.findByDescripition(new Extended_description(description));

        }else if(option==5){
            String cod = Console.readLine("Enter the code of the category");
            Code id = new Code(cod);

            Category category= controller.findCategoryById(id);
            System.out.println(controller.findByCategory(category).toString());

        }

        return false;
    }


private Iterable<Product> seeFullCatalog(){
        final Iterable<Product> products=controller.allProducts();
return products;
    }

}
